#!/usr/bin/python

import sys,nkparser,time
import os

os.environ['SUDO_ASKPASS']='/home/msobral/netkit2/bin/netkit_askpass.py'

# cria um parser ("analisador sintatico") para intepretar um 
# arquivo de configuracao. O parser gera um objeto Network.
p=nkparser.NetkitParser(sys.argv[1])

#l = open(sys.argv[1]).readlines()
#for x in l:
#  print p.tokenize(x)
#sys.exit(0)

# tenta construir uma rede a partir do arquivo de configuracao
try:
  p.parse()
except nkparser.ParseError, e:
  print e, e.line, e.n
  sys.exit(0)

# Obtem uma referencia a rede gerada pelo parser. "rede" eh um objeto Network.
rede = p.get_network()

#sys.exit(0)

# Inicia a rede ...
rede.start()

# Apos 30 segundos, para a rede
time.sleep(3600)
rede.stopNetwork()

