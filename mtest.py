import sys,nkparser,time

def run(conf):
  p=nkparser.NetkitParser(conf)

  try:
    p.parse()
  except nkparser.ParseError, e:
    print e, e.line, e.n
    return None

  # Obtem uma referencia a rede gerada pelo parser. "rede" eh um objeto Network.
  rede = p.get_network()

  return rede
