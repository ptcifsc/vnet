#!/usr/bin/python

## Modulos
import nkparser,select,fcntl,re #locais
import sys,os,pty, shutil #sistema
from socket import * #conexao de dados
import config as cfg #arquivo de configuracao
import BaseHTTPServer #conexao de controle
import traceback
from rest import NetkitRestHandler # API REST
#
## Especializou-se o HTTPServer para que ele tenha uma variavel de instancia
## que referencie o "controller"
class ControlHTTPServer(BaseHTTPServer.HTTPServer):
	def __init__(self, addr, handler, controller):
		BaseHTTPServer.HTTPServer.__init__(self, addr, handler)
		self.controller = controller

## Classe Controladora, servira como gateway entre o cliente e o netkit
class Controller:
    BasePath = '/home/aluno/redes'
    LabPath = '/home/aluno/lab'
    
    def __init__(self, basepath=BasePath,  labpath=LabPath):
        self.server = ControlHTTPServer((cfg.HOST, cfg.CONTROL_PORT), NetkitRestHandler, self)
        # dicionario com os objetos Network em execucao. Ex:
        # {'lab': {1: Network_object}}
        self.networks = {}
        self.base = basepath
        self.path = labpath

    def __get_networks__(self):
        r = []
        for instance in self.networks.values():
            r += instance.values()
        return r

    def __get_fds__(self):
        # No minimo ha o socket do servidor http (controle)
        fds = [self.server.socket]
        # obtem os descritores (sockets e pseudo-terminais) das redes em execucao
        for network in self.__get_networks__():
            pool = network.get_pool()
            fds = fds + pool.get_fds()
        return fds

    def serve_forever(self):
        while True:
            list_fds = self.__get_fds__()
            try:	
                read, write, error = select.select(list_fds, [], [], 1)
                ## O que fazer se houver uma nova conexao de controle
                if self.server.socket in read: 
                    self.server.handle_request()
                # verifica se as redes possuem algo para transferir
                for network in self.__get_networks__():
                    try:
                        network.pool.relay(read)
                    except IOError:
                        try:
                            network.stop()
                        except:
                            pass
                        self.__del_network__(network)
            except:
                pass

    def __del_network__(self,  network):
        for name in self.networks:
            for inst in self.networks[name]:
                if self.networks[name][inst] == network:
                    del self.networks[name][inst]
                    break

    def __get_network__(self, name):
        conf = "%s/%s.conf" % (self.base,  name)
        prepareNetwork = nkparser.NetkitParser(conf) ## retorna uma variavel do tipo network
        if not prepareNetwork.parse():
            print 'Abortando ...'
            return None
        rede = prepareNetwork.get_network()
        return rede

    def __add_network__(self,  name,  network):
        try:
            nums = self.networks[name].keys()
            n = 0
            if nums:
                nums.sort()
                while n < len(nums):
                    if nums[n] != n+1: break
                    n += 1
                n += 1
            else:
                n = 1
        except KeyError:
            self.networks[name] = {}
            n = 1
        self.networks[name][n] = network
        return n

    def start_network(self, name):
        '''Inicia um objeto Network com nome dado por "name"'''
        rede = self.__get_network__(name)
        if not rede: return None
        inst_num = self.__add_network__(name, rede)
        inst_path = '%s/%s/%d' % (self.path,  name,  inst_num)
        try:
            shutil.rmtree(inst_path)
        except:
            pass
        os.makedirs(inst_path)
        rede.update_prefs({'path': inst_path})
        pool = TermPool()
        #self.pool.set_active_vm(vm[0])
        rede.start(pool)
        pool.start()
        return inst_num

    def list_networks(self):
        try:
            nets = filter(lambda path: path[-5:] == '.conf',  os.listdir(self.base))
            nets = map(lambda path: os.path.splitext(path)[0],  nets)
            return nets
        except:
            return []

    def add_network(self,  name,  conf):
        path = "%s/%s.conf" % (self.base,  name)
        try:
            os.stat(path)
            raise NameError('network already exists')
        except OSError:
            #print traceback.format_exc()
            pass
        open(path,  'w').write(conf)
        try:
            self.__get_network__(name)
        except nkparser.ParseError,  e:
            os.remove(path)
            raise
        except nkparser.SemanticError,  e:
            os.remove(path)
            raise

    def update_network_config(self,  name,  conf):
        path = "%s/%s.conf" % (self.base,  name)
        try:
            os.stat(path)
        except OSError:
            raise NameError('network does not exist')
        old = open(path).read()
        open(path,  'w').write(conf)
        try:
            self.__get_network__(name)
        except:
            open(path).write(old)
            raise

    def remove_network(self,  name):
        try:
            if self.networks[name]:
                raise RuntimeError('locked network')
            try:
                path = "%s/%s.conf" % (self.base,  name)
                os.remove(path)
            except:
                pass
            del self.networks[name]
        except KeyError:
            raise

    def get_network(self, name, inst_num=0):
        if not inst_num:
            rede = self.__get_network__(name)
        else:
            inst_num = int(inst_num)
            try:
                rede = self.networks[name][inst_num]
            except KeyError:
                return None
        return rede

    def stop_network(self, name, inst_num):
        inst_num = int(inst_num)
        rede = self.get_network(name, inst_num)
        if not rede:
            raise NameError('Unknown network')
        rede.stop()
        del self.networks[name][inst_num]

    def get_network_config(self,  name):
        try:
            path = "%s/%s.conf" % (self.base,  name)
            conf = open(path).read()
            return conf
        except OSError:
            raise NameError()
        except:
            raise
        
#
## Classe TermPool
#
class TermPool:
	
	def __init__(self):
		self.terms = {}
		self.active = None
		self.socket = None
		self.con = None
		self.addr = None

	def addVM(self, vm):
		self.terms[vm.get_name()] = NetTerm(vm)
		# a primeira vm adicionada eh a vm ativa por default
		if not self.active:
			self.active = vm.get_name()

	def get_active(self):
		return self.active

	def get_dataport(self):
		if not self.socket: return None
		addr, port = self.socket.getsockname()
		return addr,port
	
	def start(self):
		for vm in self.terms.values():
			fd_vm = vm.start()
			#fcntl.fcntl(fd_vm, fcntl.F_SETFL, os.O_NDELAY)
			self.socket = socket(AF_INET, SOCK_STREAM)
			self.socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
			self.socket.bind((cfg.HOST, 0))
			self.socket.listen(1)
		#return self.terms[self.active].fd
		return self.socket
					
	def get_fds(self):
		if self.socket: # se o pool foi iniciado ...
			#l = map(lambda term: term.get_fd(), self.terms.values()) #cria uma lista (l) com todos os fds
			# somente o terminal da vm ativa
			l = [self.terms[self.active].get_fd()] 
			if self.con: l.append(self.con) # se ha conexao de dados ...
			else: l.append(self.socket) # senao inclui o socket que espera a conexao
			return l
		else:
			return []
	
	def relay(self, read_fds):
		vm = self.terms[self.active]
		fd_vm = vm.get_fd()
		if self.socket in read_fds: ## se for uma nova conexao, aceita e armazena cliente
			self.con, addr = self.socket.accept()
		if self.con in read_fds: # se o cliente enviou alguma coisa
			fromClient = self.con.recv(256)
			vm.write(fromClient)
		if fd_vm in read_fds: # se o servidor quiser enviar alguma coisa para o cliente
			fromTerm = vm.read()
			# se cliente nao conectou ainda, entao descarta o que o 
			# terminal da vm mandou.
			# depois pensamos em mudar isso para bufferizar ...
			if self.con: self.con.send(fromTerm) 
	
	def stop(self):
		for vm in self.terms:
			self.terms[vm].stop()
		self.con.shutdown(SHUT_RDWR)
		self.socket.close()
		self.con = None
		self.socket = None
	
	def get_term(self, name):
		return self.terms[name]
	
	def get_terms(self):
		return self.terms
	
	def get_vm_names(self):
		return self.terms.keys()
	
	def get_client(self):
		return self.socket
	
	def set_active(self, name):
		# ativa a vm identificada pelo nome "name"
		# primeiro testa se o nome da vm eh valido
		if self.terms.has_key(name):
			self.active = name
	
#
## Classe NetTerm
class NetTerm:
	
	MaxBytes = 256

	def __init__(self, vm):
		self.name = vm.get_name()
		self.vm = vm
		self.fdr = None # aqui deve ser o lado mestre do pseudo-terminal
		self.fdw = None # aqui deve ser o lado mestre do pseudo-terminal
		self.rflags = 0

	def get_fd(self):
		return self.fdr

	def start(self):
		fd = self.vm.start()
		self.fdr = os.fdopen(fd, 'r')	
		self.fdw = os.fdopen(fd, 'w')	
		self.rflags = fcntl.fcntl(self.fdr, fcntl.F_GETFL) ^ os.O_NONBLOCK
		return self.fdr

	def stop(self):
		self.vm.stop()
		self.fdr = None
		self.fdw = None

	def read(self, maxbytes=MaxBytes):
		fcntl.fcntl(self.fdr, fcntl.F_SETFL, os.O_NONBLOCK)
		data = self.fdr.read(maxbytes)
		fcntl.fcntl(self.fdr, fcntl.F_SETFL, self.rflags)
		return data

	def write(self, data):
		try:
			self.fdw.write(data)
			self.fdw.flush()
		except:
			pass

	def __repr__(self):
		return 'Terminal %s: (%s)' % (self.name, self.cmd)
#
##################### Inicio do programa #################
Base = "/home/msobral/rco2/netkit-console/parser/labs"
Lab = "/home/msobral/rco2/netkit-console/parser/labs/lab"
if __name__ == '__main__':
	server = Controller(Base,  Lab)
	server.serve_forever()
