#!/usr/bin/python

import sys,os,pty,getopt,traceback,time, threading
import urlparse, httplib, urllib
import select,fcntl
from socket import *
import re,json

class RestConnection:

    JsonType = 'application/json'
    FormType = 'application/x-www-form-urlencoded'
    GET = 'GET'
    PUT = 'PUT'
    POST = 'POST'
    DELETE = 'DELETE'
    
    def __init__(self,  ip,  port):
        self.ip = gethostbyname(ip)
        self.port = port
        
    def __send_request__(self,  method,  path,  data=None,  cabecalhos={}):
        con = httplib.HTTPConnection(self.ip,  self.port)
        headers = {}
        headers.update(cabecalhos)
        if data != None:
            #if not headers.has_key('Content-type'):
            #   data = json.dumps(data)
            #   headers['Content-type'] = self.JsonType
            if not headers.has_key('Content-type'):
                data = urllib.urlencode(data)
                headers['Content-type'] = self.FormType
        print path,  data,  headers
        con.request(method,  path,  data,  headers)
        resp = con.getresponse()
        resp.data = {}
        try:
            if resp.getheader('Content-type') == self.JsonType:
                data = json.loads(resp.read())
                resp.data = data
        except:
            pass
        return resp
        
    def get(self,  path,  headers={}):
        resp = self.__send_request__(self.GET,  path,  None,  headers)
        return resp
        
    def put(self,  path,  data=None,  headers={}):
        resp = self.__send_request__(self.PUT,  path,  data,  headers)
        return resp
        
    def post(self,  path,  data=None,  headers={}):
        resp = self.__send_request__(self.POST,  path,  data,  headers)
        return resp

    def delete(self,  path,  data=None,  headers={}):
        resp = self.__send_request__(self.DELETE,  path,  data,  headers)
        return resp

class Terminal:

  def __init__(self, sock):
    self.sock = sock
    self.pid = 0
    self.fdr=-1
    self.fdw=-1

  def run(self):
    fd, self.fd = pty.openpty()
    self.pid = os.fork()
    if not self.pid:
      os.execlp('xterm','xterm','-Sptm/%d' % fd)
      raise RuntimeError('nao conseguiu executar xterm')
    
    #self.pid, self.fd = pty.fork()
    #if not self.pid:
    #  fcntl.fcntl(sys.stdout, fcntl.F_SETFL, os.O_NDELAY)
    #  fcntl.fcntl(sys.stdin, fcntl.F_SETFL, os.O_NDELAY)
    #  os.execlp('nc','nc')
    #  raise RuntimeError('nao conseguiu executar xterm')
    self.fdr = os.fdopen(self.fd, 'r')
    self.fdw = os.fdopen(self.fd, 'w')

  def __get_block_flag__(self, fd):
    flags = fcntl.fcntl(fd, fcntl.F_GETFL)
    flags = flags ^ os.O_NONBLOCK
    return flags
    
  def relay(self):
    sock_flags = self.__get_block_flag__(self.sock)
    fd_flags = self.__get_block_flag__(self.fdr)
    try:
     while True:
      rl, wl, el = select.select([self.sock, self.fdr], [], [])
      if self.sock in rl:
        fcntl.fcntl(self.sock, fcntl.F_SETFL, os.O_NONBLOCK)
        x = self.sock.recv(128)
        if x:
          print '...', len(x)
          try:
            self.fdw.write(x)
            self.fdw.flush()
          except IOError:
            pass
        else:
          print 'Fim ...'
          break
        fcntl.fcntl(self.sock, fcntl.F_SETFL, sock_flags)
      if self.fdr in rl:
        fcntl.fcntl(self.fdr, fcntl.F_SETFL, os.O_NONBLOCK)
        x = self.fdr.read(128)
        if x:
          self.sock.send(x)
        fcntl.fcntl(self.fdr, fcntl.F_SETFL, fd_flags)
    except Exception, e:
      print e
      print traceback.format_exc()
      return
  
  def stop(self):
    if self.pid: os.kill(self.pid, 15)
    try:
      self.fdr.close()
    except:
      pass
    try:
      self.fdw.close()
    except:
      pass

class NetkitClient:

  Timeout = 5.0
  URL = '/netkit/%s'

  def __init__(self, ip, port=8888):
    self.ip = ip
    self.port = port
    self.id = 0
    self.dataport = None
    self.lab = None
    self.con = None
    self.instance = None

  def start(self, lab):
    self.lab = lab
    path = self.URL % lab
    con = RestConnection(self.ip,  self.port)
    resp = con.post(path)
    if resp.status == 201:
      self.instance = resp.getheader('location')
      self.id = resp.data['id']
      self.dataport = resp.data['dataport']
      self.nodes = resp.data['nodes']
      self.active = resp.data['active']
    elif resp.status == 404:
      raise NameError('rede desconhecida')
    elif resp.status == 400:
      raise RuntimeError('erro de execucao')

  def __getaddr__(self):
    u = urlparse.urlparse(self.instance)
    addr = gethostbyname(u.hostname)
    port = u.port
    path = u.path
    return addr,  port,  path
    
  def set_active(self,  vm):
    if not self.instance: raise RuntimeError('rede nao iniciada')
    if not vm in self.nodes: raise NameError('unknown vm %s' % vm)
    addr,  port,  path = self.__getaddr__()
    data = {'active': vm}
    con = RestConnection(addr,  port)
    resp = con.put(path,  data)
    if resp.status == 200:
        self.active = resp.data['active']
        pass
    elif resp.status == 404:
      raise NameError('rede desconhecida')
    elif resp.status == 400:
      raise RuntimeError('erro de execucao')

  def stop(self):
    if not self.instance: raise RuntimeError('rede nao iniciada')
    addr,  port,  path = self.__getaddr__()
    con = RestConnection(addr,  port)
    resp = con.delete(path)
    if resp.status == 200:
        if self.con: 
          try:
            self.con.shutdown(SHUT_RDWR)
          except:
            pass
    elif resp.status == 404:
      raise NameError('rede desconhecida')
    elif resp.status == 400:
      raise RuntimeError('erro de execucao')
    self.id = 0
    self.dataport = None
    self.lab = None
    self.instance = None
    
  def get_dataport(self):
    if not self.id: raise RuntimeError('rede nao iniciada')
    if self.con: return self.con
    url = urlparse.urlparse(self.dataport)
    if url.scheme != 'tcp':
        raise ValueError('invalid protocol in url: %s' % url.scheme)
    ip = gethostbyname(url.hostname)
    port = url.port
    if not port:
        raise ValueError('unknown port in url')
    self.con = socket(AF_INET, SOCK_STREAM)
    self.con.bind(('0.0.0.0',0))
    self.con.connect((ip, port))
    return self.con
    
  def get_networks(self):
    path = self.URL % ''
    con = RestConnection(self.ip,  self.port)
    resp = con.get(path)
    if resp.status == 200:
        print resp.data
        return resp.data
    elif resp.status == 400:
      raise RuntimeError('erro de execucao')
      
  def get_terminal(self):
          return Terminal(self.get_dataport())

  def upload_network(self,  name,  arq=''):
      if not arq: arq = '%s.conf' % name
      path = self.URL % ''
      data = {'name': name,  'conf':open(arq).read()}
      con = RestConnection(self.ip,  self.port)
      resp = con.post(path,  data)
      if resp.status == 201:
          return resp.getheader('location')
      if resp.status == 400:
          raise SyntaxError(resp.data)
      if resp.status == 409:
          raise NameError('rede jah existe !')
      raise ValueError('erro desconhecido: %d' % resp.status)
      
  def delete_network(self,  name):
       path = self.URL % 'name'
       con = RestConnection(self.ip,  self.port)
       resp = con.post(path,  data)
       if resp.status == 200:
           return
       if resp.status == 404:
           raise NameError('rede nao existe !')
       if resp.status == 400:
           raise RuntimeError('erro de execucao')
       raise ValueError('erro desconhecido: %d' % resp.status)
        
