class Base:

  N = []

  def __init__(self):
    self.n = self.__get_num__()

  def __repr__(self):
    return 'base_%d' % self.n

  def __get_num__(self):
    n = 0
    while True:
      try:
        if self.N[n] != n:
          self.N.insert(n, n)
          return n
      except IndexError:
        self.N.append(n)
        return n
      #if not n in self.N: 
      #  self.N.append(n)
      #  self.N.sort()
      #  return n
      n += 1

  def __del__(self):
    self.N.remove(self.n)

