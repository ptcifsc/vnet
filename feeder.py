#!/usr/bin/python3

import sys,pyinotify,os
import subprocess

class Handler(pyinotify.ProcessEvent):

  def __init__(self, fd, pipe):
    pyinotify.ProcessEvent.__init__(self)
    self.fd = fd
    self.pipe = pipe
    
  def process_IN_CLOSE_WRITE(self, ev):
      raise BrokenPipeError()
      
  def process_IN_CLOSE_NOWRITE(self, ev):
      raise BrokenPipeError()
      
  def process_IN_MODIFY(self, ev):
    data = self.fd.read()
    #print('...', ev, data)
    self.pipe.write(data)
    self.pipe.flush()

if __name__ == '__main__':
    try:
        capname = sys.argv[1]
        caparq = open(capname, 'rb')
    except IndexError:
        print('Uso: %s nome_arq_captura' % sys.argv[0])
        sys.exit(0)
    except FileNotFoundError:
        print('Arquivo de captura %s não encontrado' % sys.argv[1])
        sys.exit(0)
    except PermissionError:
        print('Arquivo de captura %s não pode ser aberto (sem permissão !)' % sys.argv[1])
        sys.exit(0)
    except Exception as e:
        print('Erro: não pode iniciar programa: ', str(e))
        sys.exit(0)
        
    # cria um named pipe     
    pipename = '%s.fifo' % capname
    try:
        os.mkfifo(pipename)
    except FileExistsError:
        pass
    except Exception as e:
        print('Não conseguiu criar o pipe:', e)
        sys.exit(0)
        
    # inicia wireshark para capturar do fifo
    pid = subprocess.Popen(['wireshark', '-k', '-i', pipename]).pid
    print('wireshark:', pid)

    # abre fifo para escrita
    pipe = open(pipename, 'wb')        
    
    # lê tudo que porventura haja no arquivo de captura, 
    # e repassa para o fifo
    try:
        data = caparq.read()
        pipe.write(data)
        pipe.flush()
    except Exception as e:
        print(e)
    
    # inicia o notificador de modificações
    wm = pyinotify.WatchManager()
    wm.add_watch(capname, pyinotify.IN_MODIFY)
    wm.add_watch(pipename, pyinotify.IN_CLOSE_NOWRITE|pyinotify.IN_CLOSE_WRITE)
    h = Handler(caparq, pipe)
    nfier = pyinotify.Notifier(wm, h)
    try:
        nfier.loop()
    except BrokenPipeError:
        print('pipe...')
        pass
    
    # termina wireshark
    try:
        os.waitpid(pid, 0)
        print('matou wireshark ??')
        os.kill(pid, 9)
    except:
        pass

    # remove fifo    
    try:
        os.remove(pipename)
    except:
        pass
    
    sys.exit(0)
        

