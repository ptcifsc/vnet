#!/usr/bin/python

import BaseHTTPServer,re
import urlparse, socket
import json
import traceback
from nkparser import ParseError,  SemanticError

class NetkitRestHandler(BaseHTTPServer.BaseHTTPRequestHandler):

  Base = re.compile(r'/netkit/?(?P<qs>\?.*)?$')
  Lab = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/?(?P<qs>\?.*)?$')
  Instance = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/(?P<id>[0-9]+)/?(?P<qs>\?.*)?$')
  Info = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/(?P<info>[a-zA-Z]+)/?(?P<qs>\?.*)?$')
  Config = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/config/?(?P<qs>\?.*)?$')
  Prefs = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/prefs/?(?P<qs>\?.*)?$')
  Dot = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/dot/?(?P<qs>\?.*)?$')
  Image = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/image/?(?P<qs>\?.*)?$')
  Hostlab = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/(?P<id>[0-9]+)/hostlab/?$')
  HostlabPath = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/(?P<id>[0-9]+)/hostlab/(?P<hpath>.*)$')
  
  Handlers = [(Lab, 'lab'), (Base, 'base'),  (Instance, 'instance'), (Config, 'config'), 
              (Prefs, 'prefs'), (Dot, 'dot'), (Image, 'image'),  (Hostlab,  'hostlab'),  (HostlabPath,  'hostlabpath')]
  ErrorPage = '''<html><head><title>Error</title></head>
<body><h1>Operacao invalida</h1>
<br><br>Recurso desconhecido OU operacao nao suportada para esse recurso
</body></html>'''

  # Handlers de entrada, selecionados de acordo com o metodo HTTP
  def do_GET(self):
    self.__handler__()

  def do_POST(self):
    self.__handler__()

  def do_PUT(self):
    self.__handler__()

  def do_DELETE(self):
    self.__handler__()

  # gera uma resposta, e eh generico o suficiente para ser usado por todos os handlers
  def __gen_response__(self, status, data=None, media_type=None, headers={}):
    self.send_response(status)
    if data:
      self.send_header('Content-type', media_type)
      self.send_header('Content-length',repr(len(data)))
    for header, value in headers.items():
      self.send_header(header, value)
    self.end_headers()
    self.wfile.write(data)

  def __read_data__(self):
      length = int(self.headers.getheader('Content-length'))
      data = self.rfile.read(length)
      return data      

  # extrai os parametros da requisicao. Eles estao no path da URI (ex: parametro "lab") ou
  # na query string, caso exista.
  def __parse_qs__(self, m):
    d = m.groupdict()
    data = ''
    # query string passada via corpo da mensagem - tipico em metodos POST, PUT e DELETE
    if self.headers.gettype() == 'application/x-www-form-urlencoded':
      data = self.__read_data__()
    elif d:
      # query string passada via path da URI - tipico em GET.
      try:
          data = d['qs'][1:]
          del d['qs']
      except:
          pass
    qs = urlparse.parse_qs(data)
    for k in qs:
      if len(qs[k]) == 1:
        qs[k] = qs[k][0]
    qs.update(d)
    return qs
    
  # identifica o handler a ser usado, de acordo com a API REST
  def __handler__(self):
    for expr,handler in self.Handlers:
      m = expr.match(self.path)
      if m:
        handler = 'do_%s_%s' % (self.command, handler)
        try:
          handler = getattr(self, handler)
        except AttributeError:
          break
        self.params = self.__parse_qs__(m)
        handler()
        return
    # Erro: nenhum handler encontrado
    self.__gen_response__(400, self.ErrorPage, 'text/html')

  # Os handlers especificos
  # Cada um deles corresponde a exatamente uma operacao visivel da interface
  # Para atender cada requisicao, um handler tem a disposicao estes atributos:
  # params: dicionario com os parametros que descrevem a requisicao, obtidos do path da 
  # URI e da query string. O nome da rede, caso exista, estah no parametro "lab":
  #   rede = self.params['lab']

  def __get_url__(self,  path):
     server_addr = socket.gethostbyname(self.server.server_name)
     return 'http://%s:%d/netkit/%s' % (server_addr,  self.server.server_port,  path)
      
  # Obtem uma listagem de redes existentes no catalogo.
  # Parametros de entrada: nao ha
  # Resultado: 200 = sucesso, e um dicionario no formato {nome_da_rede: URI_da_rede}
  #                     400 = erro de execucao
  def do_GET_base(self):
    try:
        nets = self.server.controller.list_networks()
        data = {}
        for net in nets:
            data[net] = self.__get_url__(net)
        self.__gen_response__(200, json.dumps(data),'application/json')
    except:
        self.__gen_response__(400)

  # Adiciona uma rede ao catalogo.
  # Resultado: 201 = sucesso, e cabecalho Location contem URI da nova rede 
  #                     400 = erro de sintaxe na configuracao, e retorna a causa do erro (linha, coluna, token)
  #                     409 = erro porque jah existe essa rede
  def do_POST_base(self):
    try:
        name = self.params['name']
        conf = self.params['conf']
        self.server.controller.add_network(name,  conf)        
        self.__gen_response__(201, None,None, {'Location': self.__get_url__(name)})
    except ParseError,  e:
        data = {'line': e.n,  'column':e.column,  'value': e.value}
        self.__gen_response__(400,  json.dumps(data),  'application/json')
    except SemanticError,  e:
        data = {'line': e.n,  'column':1, 'value': e.value}
        self.__gen_response__(400,  json.dumps(data),  'application/json')
    except NameError,  e:
        self.__gen_response__(409)

  def do_GET_lab(self):
    self.__gen_response__(200, 'path=%s, qs=%s\n'%(self.path, self.params),'text/plain')
    pass

  # Inicia a execucao de uma rede, criando uma instancia.
  # Parametros: nao ha
  # Resultado: 201 = sucesso, com URI da instancia no cabecalho location, e seus dados em json (id, dataport, nodos e vm ativa)
  #                     400 = erro generico, 404 = rede nao existe
  def do_POST_lab(self):
    try:
        inst = self.server.controller.start_network(self.params['lab'])
        rede = self.server.controller.get_network(self.params['lab'],  inst)
    except Exception,  e:
        print traceback.format_exc()
        self.__gen_response__(404)
        return
    try:
        pool = rede.get_pool()
        addr, port = pool.get_dataport()
        inst = self.__get_url__('%s/%d' % (self.params['lab'],  inst))
        data = {'id': inst,  'dataport': 'tcp://%s:%d/' % (addr,  port),  'nodes': rede.get_nodes(),  'active': pool.get_active()}
        data = json.dumps(data)
        headers = {'Location':  inst}
        print data
        self.__gen_response__(201,  data,'application/json',  headers)
    except Exception,  e:
        print traceback.format_exc()
        self.__gen_response__(400)

  # Remove uma rede do catalogo.
  # Parametros: nao ha.
  # Resultado: 200 = sucesso, 404 = rede nao existe, 423 = ha instancias em execucao, 400 = erro generico
  def do_DELETE_lab(self):
    name = self.params['lab']
    try:
        self.server.controller.remove_network(name)
        self.__gen_response__(200)
    except RuntimeError:
        self.__gen_response__(423)
    except NameError:
        self.__gen_response__(404)
    except:
        self.__gen_response__(400)

  # Modifica atributos da rede (??? melhor nos metodos PUT ???)
  def do_PUT_lab(self):
    self.__gen_response__(200, 'path=%s, qs=%s\n'%(self.path, self.params),'text/plain')
    pass

  # Obtem os atributos de uma instancia: id, dataport, lista de nodos, vm ativa
  # Parametros: nenhum
  # Resultado: 200 == sucesso, e dados em json, 400 = erro generico, 404 = rede ou instancia nao existe
  def do_GET_instance(self):
    print self.params
    try:
        rede = self.server.controller.get_network(self.params['lab'],  self.params['id'])
    except:
        self.__gen_response__(404)
    try:
        pool = rede.get_pool()
        addr, port = pool.get_dataport()
        server_addr = socket.gethostbyname(self.server.server_name)        
        inst = 'http://%s:%s/netkit/%s/%s' % (server_addr,  self.server.server_port,  self.params['lab'],  self.params['id'])
        data = {'id': inst,  'dataport': 'tcp://%s:%d/' % (addr,  port),  'nodes': rede.get_nodes(),  'active': pool.get_active()}
        data = json.dumps(data)
        self.__gen_response__(200,  data,'application/json')
    except Exception, e:
        print traceback.format_exc()
        self.__gen_response__(400)
    pass

  # muda a vm ativa.
  # Parametros: active = nodo_ativo (json)
  # Resultado: 200 = sucesso e nome da nova vm ativa (json), 400 = erro generico, 404 = rede ou instancia nao existe
  def do_PUT_instance(self):
    try:
        rede = self.server.controller.get_network(self.params['lab'],  self.params['id'])
    except:
        self.__gen_response__(404)
    try:
        pool = rede.get_pool()
        pool.set_active(self.params['active'])
        data = json.dumps({'active': pool.get_active()})
        self.__gen_response__(200,  data,  'application/json')
    except:
        self.__gen_response__(400)

  # Encerra um experimento em execucao
  # Parametros de entrada: lab=nome da rede, id=numero da instancia
  # Resultado: 200 = sucesso, 404 = instancia inexistente, 400 = erro generico
  def do_DELETE_instance(self):
    nome_rede = self.params['lab']
    instancia = self.params['id']

    try:
        self.server.controller.stop_network(nome_rede, instancia)
        # envia a resposta com sucesso
        self.__gen_response__(200, 'Instancia %s da rede %s parada com sucesso\n' % (instancia, nome_rede) ,'text/plain')
    except NameError:
        print traceback.format_exc()
        # rede desconhecida
        self.__gen_response__(404, 'Rede %s, com instancia %s, nao encontrada\n' % (nome_rede, instancia), 'text/plain')
    except:
        print traceback.format_exc()
        # rede desconhecida
        self.__gen_response__(400)

  # Obtem a configuracao de uma rede.
  # Parametros: nao ha
  # Resultado: 200 = sucesso e corpo da mensagem text/plain com a configuracao
  #                      404 = rede nao existe, 400 = erro generico
  def do_GET_config(self):
    try:
        conf = self.server.controller.get_network_config(self.params['lab'])
        self.__gen_response__(200, conf,'text/plain')
    except NameError:
        self.__gen_response__(404)
    except:
        self.__gen_response__(400)

  # Modifica a configuracao de uma rede.
  # Parametros: a nova configuracao no corpo da mensagem, em text/plain
  # Resultado: 200 = sucesso, 404 = rede nao existe, 400 = erro de sintaxe, 406 = conteudo nao eh text/plain, 500 = erro generico
  def do_PUT_config(self):
    if self.headers.gettype() != 'text/plain':
        sef.__gen_response__(406)
    conf =  self.__read_data__()
    try:
        name = self.params['name']
        self.server.controller.update_network_config(name,  conf)        
        self.__gen_response__(200)
    except ParseError,  e:
        data = {'line': e.n,  'column':e.column,  'value': e.value}
        self.__gen_response__(400,  json.dumps(data),  'application/json')
    except SemanticError,  e:
        data = {'line': e.n,  'column':1, 'value': e.value}
        self.__gen_response__(400,  json.dumps(data),  'application/json')
    except NameError,  e:
        self.__gen_response__(404)
    except:
        self.__gen_response__(500)

  def do_GET_prefs(self):
    self.__gen_response__(200, 'path=%s, qs=%s\n'%(self.path, self.params),'text/plain')
    pass

  def do_PUT_prefs(self):
    self.__gen_response__(200, 'path=%s, qs=%s\n'%(self.path, self.params),'text/plain')
    pass

  def do_GET_dot(self):
    self.__gen_response__(200, 'path=%s, qs=%s\n'%(self.path, self.params),'text/plain')
    pass

  def do_GET_image(self):
    self.__gen_response__(200, 'path=%s, qs=%s\n'%(self.path, self.params),'text/plain')
    pass

  def do_GET_hostlab(self):
      self.__gen_response__(200, 'path=%s, qs=%s\n'%(self.path, self.params),'text/plain')
      pass
      
  def do_POST_hostlabpath(self):
      self.__gen_response__(200, 'path=%s, qs=%s\n'%(self.path, self.params),'text/plain')
      pass
      
  def do_GET_hostlabpath(self):
      self.__gen_response__(200, 'path=%s, qs=%s\n'%(self.path, self.params),'text/plain')
      pass
      
if __name__ == '__main__':
    s = BaseHTTPServer.HTTPServer(('0.0.0.0',8888), RequestHandler)
    s.serve_forever()
