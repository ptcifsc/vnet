   Analisador sintático do Netkit

   O analisador sintático (parser) deve verifica a correção léxica 
e sintática do arquivo de configuração. Qualquer erro de sintaxe, atributo
desconhecido, ou inconsistência, deve interromper o processamento da
configuração e gerar um erro. A linha e coluna do erro devem ser informados,
assim como o motivo exato do que se verificou incorreto (ex: que declaração
prévia faltou, ou que declaração é incompatível para o contexto). Uma
completa reescrita do parser é necessária para se obter esse efeito.

   O módulo PLY implementa um analisador léxico e sintático baseados no
lex e yacc. Ele é feito puramente em Python, e explora a capacidade de
introspecção e reflexão da linguagem. Para usá-lo deve-se definir uma 
tabela de símbolos para o analisador léxico, e uma gramática para o 
sintático. Com isso ele pode analisar  arquivos de configuraçao do Netkit,
e informar a correção e consistência das declarações. Mais ainda, ele pode
substituir ou simplificar a forma com que esse arquivo é avaliado e os
objetos netkit são criados (hosts e interfaces).

netkit.py: API com classes do Netkit que representam hosts e interfaces.

nkparser.py: analisador sintático do Netkit

nkrules.py: tabela de símbolos e gramática do Netkit

