#!/usr/bin/python

import sys,BaseHTTPServer,re
import os,socket,random
import urllib,urlparse
import json

class RequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):

  Base = re.compile(r'/netkit/?(?P<qs>\?.*)?$')
  Lab = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/?$')
  Instance = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/(?P<id>[0-9]+)/?$')
  Info = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/(?P<info>[a-zA-Z]+)/?$')
  Config = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/config/?$')
  Prefs = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/prefs/?$')
  Dot = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/dot/?$')
  Image = re.compile('/netkit/(?P<lab>[-a-zA-Z0-9_]+)/image/?$')

  Handlers = [(Base, 'base'), (Lab, 'lab'), (Instance, 'instance'), (Config, 'config'), 
              (Prefs, 'prefs'), (Dot, 'dot'), (Image, 'image')]
  ErrorPage = '''<html><head><title>Error</title></head>
<body><h1>Operacao invalida</h1>
<br><br>Recurso desconhecido OU operacao nao suportada para esse recurso
</body></html>'''

  # Handlers de entrada, selecionados de acordo com o metodo HTTP
  def do_GET(self):
    self.__handler__()

  def do_POST(self):
    self.__handler__()

  def do_PUT(self):
    self.__handler__()

  def do_DELETE(self):
    self.__handler__()

  # gera uma resposta, e eh generico o suficiente para ser usado por todos os handlers
  def __gen_response__(self, status, data=None, media_type=None, headers={}):
    self.send_response(status)
    if data:
      self.send_header('Content-type', media_type)
      self.send_header('Content-length',repr(len(data)))
    for header, value in headers:
      self.send_header(header, value)
    self.end_headers()
    self.wfile.write(data)

  def __parse_qs__(self, m):
    if self.headers.gettype() == 'application/x-www-form-urlencoded':
      length = int(self.headers.getheader('content-length'))
      data = self.rfile.read(length)
    else:
      d = m.groupdict()
      data = d['qs'][1:]
    qs = urlparse.parse_qs(data)
    print 'data=%s, qs=%s' % (data, qs)
    for k in qs:
      if len(qs[k]) == 1:
        qs[k] = qs[k][0]
    return qs
    
  # identifica o handler a ser usado, de acordo com a API REST
  def __handler__(self):
    for expr,handler in self.Handlers:
      m = expr.match(self.path)
      if m:
        handler = 'do_%s_%s' % (self.command, handler)
        try:
          handler = getattr(self, handler)
        except AttributeError:
          break
        self.qs = self.__parse_qs__(m)
        handler()
        return
    # Erro: nenhum handler encontrado
    self.__gen_response__(503, self.ErrorPage, 'text/html')

  # Os handlers especificos
  # Cada um deles corresponde a exatamente uma operacao visivel da interface

  def do_GET_base(self):
    self.__gen_response__(200, 'path=%s, qs=%s\n'%(self.path, self.qs),'text/plain')
    pass

  def do_POST_base(self):
    self.__gen_response__(200, 'path=%s, qs=%s\n'%(self.path, self.qs),'text/plain')
    pass

  def do_GET_lab(self):
    self.__gen_response__(200)
    pass

  def do_POST_lab(self):
    self.__gen_response__(200)
    pass

  def do_PUT_lab(self):
    self.__gen_response__(200)
    pass

  def do_DELETE_lab(self):
    self.__gen_response__(200)
    pass

  def do_GET_instance(self):
    self.__gen_response__(200)
    pass

  def do_PUT_instance(self):
    self.__gen_response__(200)
    pass

  def do_DELETE_instance(self):
    self.__gen_response__(200)
    pass

  def do_GET_config(self):
    self.__gen_response__(200)
    pass

  def do_PUT_config(self):
    self.__gen_response__(200)
    pass

  def do_GET_prefs(self):
    self.__gen_response__(200)
    pass

  def do_PUT_prefs(self):
    self.__gen_response__(200)
    pass

  def do_GET_dot(self):
    self.__gen_response__(200)
    pass

  def do_GET_image(self):
    self.__gen_response__(200)
    pass


s = BaseHTTPServer.HTTPServer(('0.0.0.0',8888), RequestHandler)
s.serve_forever()
