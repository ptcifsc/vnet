#!/usr/bin/python

import gtk,gobject
import sys,os,stat,urllib2,hashlib,time,tarfile,pwd,threading

class NetDialog(gtk.MessageDialog):

    File = 'netkit2.tar.bz2'
    CheckFile = 'netkit2.md5'
    BaseUrl = 'http://tele.sj.ifsc.edu.br/~msobral/netkit2/install'
    #BaseUrl = 'http://tele.sj.ifsc.edu.br/~msobral/netkit'
    UrlCheck = '%s/%s' % (BaseUrl, CheckFile)
    Url = '%s/%s' % (BaseUrl, File)
    Timeout = 60 # 1 minuto
    Chunk = 65536

    def __init__(self):
        super(NetDialog, self).__init__(None,
                gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                gtk.MESSAGE_INFO,
                gtk.BUTTONS_OK_CANCEL,
                None)
        self.set_markup('<b>Downloading installation file ...</b>')
        self.bar = gtk.ProgressBar()
        self.bar.show()
        self.prog = gtk.Label('0.00 %')
        self.prog.show()
        h = gtk.HBox()
        h.pack_start(self.bar)
        h.pack_start(self.prog)
        self.vbox.pack_end(h)
        hbox = self.get_action_area()
        self.cancel = None
        for w in hbox.get_children():
            if w.get_label() == 'gtk-cancel':
                self.cancel = w
            elif w.get_label() == 'gtk-ok':
                self.ok = w
        self.ok.set_label('Next')
        self.ok.set_sensitive(False)
        self.cancel.set_label('Cancel')

    def chunk_download(self):
      try:
        s = self.u.read(self.Chunk)
        if s:
          self.arq.write(s)
          self.m.update(s)
          self.bytes += len(s)
          x = self.bytes/self.total 
          self.bar.set_fraction(x)
          self.prog.set_label('%.2f %%' % (x*100))
          #print self.bytes/self.total
          if self.running: 
            gobject.idle_add(self.chunk_download)
          #else:
          #  os.remove(self.File)
        else:
          self.u.close()
          self.arq.close()
          if self.md5_sum == self.m.hexdigest():
            self.set_markup('<b>Download completed</b>')
            self.ok.set_sensitive(True)
            #self.response(gtk.RESPONSE_OK)
          else:
            self.set_markup('<b>Downloaded file is corrupt</b>')
            os.remove(self.File)
      except socket.error:
        self.arq.close()
        gobject.idle_add(self.download)
      return False
        
    def get_total_size(self):
      u = urllib2.urlopen(self.Url)     
      self.total = float(u.headers.getheader('content-length'))
      u.close()

    def get_hash(self):
        u = urllib2.urlopen(self.UrlCheck, None, self.Timeout)     
        m = u.read()
        u.close()
        return m

    def download(self):
        self.get_total_size()
        req = urllib2.Request(self.Url)
        self.m = hashlib.md5()
        self.md5_sum = self.get_hash()
        try:
          status = os.stat(self.File)
          self.bytes = status[stat.ST_SIZE]
          req.headers['Range'] = 'bytes=%d-' % self.bytes
          self.arq = open(self.File, 'r+')
          while 1:
            s = self.arq.read(4096)
            if not s:
              break
            self.m.update(s)
        except:
          self.arq = open(self.File, 'w')
          self.bytes = 0
        if self.md5_sum == self.m.hexdigest():
          self.set_markup('<b>Download completed</b>')
          self.bar.set_fraction(1.0)
          self.prog.set_label('100.00 %')
          self.ok.set_sensitive(True)
          return False          
        try:
          self.u = urllib2.urlopen(req, None, self.Timeout)     
          mb = int(self.total) >> 20
          self.set_markup('<b>Downloading installation file.\nPlease be patient ... it has %s MB</b>' % mb)
          gobject.idle_add(self.chunk_download)
        except Exception, e:
          self.set_markup('Download failed !')
          self.response(gtk.RESPONSE_CANCEL)
          print e
        return False

    def run(self):
        self.show_all()
        self.running = True
        #self.ok = True
        gobject.idle_add(self.download)
        r = super(NetDialog,self).run()
        self.running = False
        if r == gtk.RESPONSE_OK:
            return self.File
        return None

class InstallDialog(gtk.MessageDialog):

    File = 'netkit2.tar.bz2'
    Soft = {'graphviz':'neato', 'bridge-utils':'brctl', 'uml-utilities':'tunctl'}
    BaseUrl = 'http://tele.sj.ifsc.edu.br/~msobral/netkit2/install'
    MenuEntry='''[Desktop Entry]
Encoding=UTF-8
Name=Netkit2
Comment=Gnome interface to Netkit
Type=Application
Exec=%s/bin/netkit2
Icon=%s/contrib/ifsc.png
Terminal=false
Categories=GNOME;Application;Education;
StartupNotify=true'''

    def __init__(self, path, instarq=File):
        super(InstallDialog, self).__init__(None,
                gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                gtk.MESSAGE_INFO,
                gtk.BUTTONS_OK_CANCEL,
                None)
        self.set_title('Installing Netkit')
        self.path = path
        self.inst_path = '%s/netkit2' % path
        self.file = instarq
        #self.set_markup('<b>Choose installation directory</b>')
        self.bar = gtk.ProgressBar()
        self.bar.show()
        self.vbox.pack_end(self.bar)
        hbox = self.get_action_area()
        self.cancel = None
        for w in hbox.get_children():
            if w.get_label() == 'gtk-cancel':
                self.cancel = w
            elif w.get_label() == 'gtk-ok':
                self.ok = w
        self.ok.set_label('Finish')
        self.ok.set_sensitive(False)
        self.cancel.set_label('Cancel')
        
    def get_sparse_size(self, obj):
      n=0
      for x in obj.sparse:
        try:
          k=x.realpos
          n += x.size
        except:
          pass
      return n

    # descompacta o arquivo de instalacao.
    # faz uns rolos meio feios para evitar trancar a interface grafica durante a 
    # descompactacao ... (isso ocorria com o arquivo de imagem do sistema de arquivos)
    # essa decompress2 roda como uma thread ... mas talvez seja mais simples voltar 
    # a usar idle_add !
    def decompress2(self):
      while True:
        if not self.running: return
        obj = self.tar.next()
        if not obj: break
        if obj.type != tarfile.GNUTYPE_SPARSE:
          bytes = obj.size
        else:
          bytes = self.get_sparse_size(obj)
        gtk.gdk.threads_enter()
        text = '%s (%d MB)' % (os.path.split(obj.name)[1], bytes>>20)
        self.set_markup('<b>Decompressing %-50s</b>' % text)
        gtk.gdk.threads_leave()
        #print obj.type != tarfile.GNUTYPE_SPARSE,  obj.name
        if obj.type != tarfile.GNUTYPE_SPARSE:
          try:
            self.tar.extract(obj, self.inst_path)
          except:
            gtk.gdk.threads_enter()
            self.set_markup('<b>%-50s</b>' % 'Decompression failed !')
            self.response(gtk.RESPONSE_CANCEL)
            gtk.gdk.threads_leave()
            return
        else:
          tarpid = os.fork()
          if not tarpid:
            os.execlp('tar','tar','xjSCf',self.inst_path, self.file, obj.name)
            print 'ERROR !!! failed to execute tar !!!'
            sys.exit(1)
          progress = 0
	  dbytes = bytes/120
          while self.running:
            r, s = os.waitpid(tarpid, os.WNOHANG)
            if not r: 
                time.sleep(1)
		if progress < bytes:
                  progress += dbytes
                #print progress,dbytes,self.total
                gtk.gdk.threads_enter()
                self.bar.set_fraction((self.completed+progress)/float(self.total))
		self.get_root_window().process_updates(True)
                gtk.gdk.threads_leave()
                
            else:
              break
          if not r:
            os.kill(tarpid,9)
            print 'Canceled installation'
        self.completed += bytes
        gtk.gdk.threads_enter()
        self.bar.set_fraction(self.completed/float(self.total))
        gtk.gdk.threads_leave()
      #print 'decompress2 finishing ...', self.completed, self.total
      self.tar.close()
      gtk.gdk.threads_enter()
      if self.completed == self.total:
        self.bar.set_fraction(1.0)
        gobject.idle_add(self.set_environment)
      else:
        self.set_markup('<b>%-50s</b>' % 'Decompression failed !')
        self.response(gtk.RESPONSE_CANCEL)
      gtk.gdk.threads_leave()
      #print 'decompress2 finished !'

    # antiga tarefa de descompactacao ... a que congela euqnato descompacta
    # a imagem do sistema de arquivos. Alem disso, a imagem resultante
    # NAO eh um arquivo esparso (e assim ocupa 10 GB de disco !)
    def decompress(self):
      obj = self.tar.next()
      if obj:
        self.completed += 1
        self.bar.set_fraction(self.completed/float(self.total))        
        try:
          self.tar.extract(obj, self.path)
        except:
          self.set_markup('<b>Decompression failed !</b>')
          self.response(gtk.RESPONSE_CANCEL)
          return False
        #print 'extracted', obj.name
        gobject.idle_add(self.decompress)
      else:
        self.tar.close()
        if self.completed == self.total:
          self.bar.set_fraction(1.0)
          gobject.idle_add(self.set_environment)
        else:
          self.set_markup('<b>Decompression failed !</b>')
          self.response(gtk.RESPONSE_CANCEL)
      return False

    def exists(self, prog):
      p = os.popen('which %s' % prog)
      r = p.readline()
      p.close()
      return r

    def check_software(self):
      #print 'check_software ...'
      r = '<b>Checking dependences:</b>\n'
      for soft in self.Soft:
         r += '%-20s ... ' % soft
         if self.exists(self.Soft[soft]):
           r += '<b>OK</b>\n'
         else:
           r += '<b>FAIL (install it!)</b>\n'
      self.format_secondary_markup(r)

    def install_menu_entry(self):
      entry = self.MenuEntry % (self.inst_path, self.inst_path)
      try:
        pw = pwd.getpwuid(os.getuid())
        f = open('%s/.local/share/applications/netkit2.desktop' % pw[5],'w')
        f.write(entry)
        f.close()
      except Exception, e:
        print 'WARNING: Failed to install menu entry for netkit2:', e

    def gen_profile(self):
      env = {}   
      env['NETKIT2_HOME']=self.inst_path
      env['PATH']='${PATH}:${NETKIT2_HOME}/bin'
      try:
        pw = pwd.getpwuid(os.getuid())
        base,shell = os.path.split(pw[6])
      except Exception, e:
        self.set_markup('<b>Failed to set environment...\nwrite the following lines to your shell profile:</b>\n%s' % profile)
        return

      if shell == 'bash':
        profile_line = 'export %s=%s\n'
        profile_arq = '%s/.profile' % pw[5]
      elif shell in ['csh', 'tcsh']:
        profile_line = 'setenv %s %s\n'
        profile_arq = '%s/.login' % pw[5]
      else:
        raise ValueError('Unsupported shell')

      try:
        profile = ''
        for k in ['NETKIT2_HOME','PATH']:
          profile += profile_line % (k, env[k])
        f = open(profile_arq, 'a')
        f.write(profile)
        f.close()
        self.set_markup('<b>Configuring environment ... complete !</b>')
      except Exception, e:
        self.set_markup('<b>Failed to set environment...\nwrite the following lines to your shell profile:</b>\n%s' % profile)

    def set_environment(self):
      #print 'set_environment...'
      self.set_markup('<b>Configuring environment ...</b>')
      self.check_software()
      self.gen_profile()
      self.install_menu_entry()
      self.ok.set_sensitive(True)
      self.cancel.set_sensitive(False)

    def get_total_files(self):
      try:
        u = urllib2.urlopen('%s/netkit2.size' % self.BaseUrl)
        self.total = int(u.read())
        u.close()
      except Exception, e:
        print 'Ops: obtaining number of compressed files by the hard way ...(%s)' % e
        f = tarfile.open(self.file, 'r:bz2',format=tarfile.GNU_FORMAT)
        self.total = 0
        while 1:
          o = f.next()
          if not o: break
	  if o.type != tarfile.GNUTYPE_SPARSE:
            self.total += o.size
          else:
	    self.total += self.get_sparse_size(obj)
      
    def run(self):
        self.show_all()
        self.set_markup('<b>Decompressing installation file ...\nPLease wait: it can take many minutes.</b>')
        self.get_total_files()
        self.tar = tarfile.open(self.file, 'r:bz2', format=tarfile.GNU_FORMAT)
        self.completed = 0
        #gobject.idle_add(self.decompress)
        try:
            os.mkdir(self.inst_path)
        except:
            pass
        task = threading.Thread(None, self.decompress2)
        self.running = 1
        task.start()
        r = super(InstallDialog,self).run()
        if r == gtk.RESPONSE_OK:
            return self.inst_path
        self.running = 0
        return False

class PrepareDialog(gtk.MessageDialog):

    File = 'netkit2.tar.bz2'

    def __init__(self):
        super(PrepareDialog, self).__init__(None,
                gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                gtk.MESSAGE_INFO,
                gtk.BUTTONS_OK_CANCEL,
                None)
        self.set_title('Choose installation directory')
        self.set_markup('Select a directory where Netkit must be installed')
        h = gtk.HBox()
        h.show()
        self.entry = gtk.Entry()
        self.entry.set_width_chars(48)
        self.entry.connect('focus-out-event', self.check_path)
        self.entry.show()
        h.pack_start(self.entry)
        self.choose = gtk.Button('Directory')
        self.choose.show()
        self.choose.connect('clicked', self.choose_dir)
        h.pack_end(self.choose)
        self.vbox.pack_start(h)
        hbox = self.get_action_area()
        self.cancel = None
        for w in hbox.get_children():
            if w.get_label() == 'gtk-cancel':
                self.cancel = w
            elif w.get_label() == 'gtk-ok':
                self.ok = w
        self.ok.set_label('Next')
        self.ok.set_sensitive(False)
        self.cancel.set_label('Cancel')
        
    def check_path(self, w, event):      
      path = self.entry.get_text()
      ok = os.path.isdir(path)
      self.ok.set_sensitive(ok)
      if not ok and path:
        self.set_markup('<b>Invalid path !</b>')

    def choose_dir(self, w):
      chooser = gtk.FileChooserDialog('Choose Installation Directory', None, gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,                                (gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
      chooser.set_default_response(gtk.RESPONSE_OK)

      r = chooser.run()
      if r == gtk.RESPONSE_OK:
        self.arq = chooser.get_filename()
        self.entry.set_text(self.arq)
        self.ok.set_sensitive(True)
      else:
        self.arq = None
        self.ok.set_sensitive(False)
      chooser.destroy()

    def run(self):
        self.show_all()
        r = super(PrepareDialog,self).run()
        if r == gtk.RESPONSE_OK:
            return self.arq
        return None

class MainWindow(gtk.Window):

  Img='netkit-demo.png'

  def __init__(self):
    super(MainWindow, self).__init__()
    self.set_title('Netkit Installer')
    self.home = ''

  def __gen_img__(self):
    try:
      r = os.stat(self.Img)
      if r[stat.ST_SIZE] != len(DemoImg): raise ValueError
    except:
      f = open(self.Img,'w')
      for c in DemoImg:
        f.write(chr(c))
      f.close()

  # Faz a instalacao !!!
  def prepare_install(self):
    inst = PrepareDialog()
    path = inst.run()
    inst.destroy()
    if path:
      self.do_install(path)

  def do_install(self, path):
    inst = InstallDialog(path, self.file)
    r = inst.run()
    inst.destroy()
    if r:
      self.table.remove(self.bfile)
      self.table.remove(self.bnet)
      b = gtk.Button('Start netkit2')
      b.connect('clicked', self.quit)
      b.show()
      self.table.attach(b, 1,2,1,2, gtk.EXPAND | gtk.FILL, 0)
      x = gtk.Label()
      x.set_markup('<b>Installation Complete !</b>\nStart netkit2 and\nselect <i>General->Update</i>\nto obtain the latest version')
      x.show()
      self.table.attach(x, 0,1,1,2)
      self.home = r
    
  def net_install(self, widget):
    net = NetDialog()
    self.file = net.run()
    net.destroy()
    if self.file:
      self.prepare_install()

  def file_install(self, widget):
    chooser = gtk.FileChooserDialog('Choose Netkit Install File', None, gtk.FILE_CHOOSER_ACTION_OPEN,
                                  (gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
    chooser.set_default_response(gtk.RESPONSE_OK)

    filtro = gtk.FileFilter()
    filtro.set_name('Netkit Install File')
    filtro.add_pattern('netkit2.tar.bz2')
    chooser.add_filter(filtro)
      
    filtro = gtk.FileFilter()
    filtro.set_name('All files')
    filtro.add_pattern('*')
    chooser.add_filter(filtro)

    r = chooser.run()
    self.file = None
    if r == gtk.RESPONSE_OK:
      self.file = chooser.get_filename()
    chooser.destroy()
    if self.file:
      self.prepare_install()

  def __render__(self):
    self.table = gtk.Table(2, 2, False)
    self.table.show()
    self.add(self.table)
    frame = gtk.Frame()
    #frame.set_size_request(640,480)
    frame.show()
    self.__gen_img__()
    img = gtk.Image()
    img.set_from_file(self.Img)
    img.show()
    frame.add(img)
    self.table.attach(frame, 0,2, 0, 1)
    self.bnet = gtk.Button('Install from Network')
    self.bnet.connect('clicked', self.net_install)
    self.table.attach(self.bnet, 0,1,1,2)
    self.bnet.show()
    self.bfile = gtk.Button('Install from file')
    self.bfile.connect('clicked', self.file_install)
    self.table.attach(self.bfile, 1,2,1,2)
    self.bfile.show()

  def quit(self, w):
    gtk.main_quit()
    if self.home:
      pid = os.fork()
      if not pid:
        os.environ['NETKIT2_HOME']=self.home
        os.environ['PATH'] += ':%s/bin' % self.home
        os.execlp('netkit2', 'netkit2')

  def show(self):
    self.__render__()
    super(MainWindow, self).show()
    self.connect('delete-event', lambda window, event: gtk.main_quit())
    gtk.gdk.threads_init()
    gtk.main()

DemoImg=[137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 
73, 72, 68, 82, 0, 0, 1, 140, 0, 0, 0, 201, 
8, 2, 0, 0, 0, 191, 185, 146, 118, 0, 0, 0, 
3, 115, 66, 73, 84, 8, 8, 8, 219, 225, 79, 224, 
0, 0, 0, 25, 116, 69, 88, 116, 83, 111, 102, 116, 
119, 97, 114, 101, 0, 103, 110, 111, 109, 101, 45, 115, 
99, 114, 101, 101, 110, 115, 104, 111, 116, 239, 3, 191, 
62, 0, 0, 32, 0, 73, 68, 65, 84, 120, 156, 237, 
157, 119, 152, 20, 69, 250, 199, 191, 213, 61, 61, 113, 
119, 54, 39, 118, 151, 28, 36, 11, 8, 130, 34, 34, 
65, 204, 158, 138, 249, 68, 61, 211, 121, 230, 120, 38, 
60, 3, 134, 51, 223, 239, 204, 158, 24, 79, 57, 65, 
12, 8, 146, 4, 81, 36, 72, 148, 156, 150, 101, 23, 
150, 205, 113, 102, 118, 39, 116, 119, 253, 254, 152, 176, 
179, 147, 123, 118, 118, 119, 134, 173, 207, 195, 195, 211, 
211, 211, 93, 251, 118, 79, 215, 183, 223, 122, 235, 173, 
42, 66, 41, 5, 131, 193, 96, 196, 43, 92, 87, 27, 
192, 96, 48, 24, 161, 96, 34, 197, 96, 48, 226, 26, 
38, 82, 12, 6, 35, 174, 97, 34, 197, 96, 48, 226, 
26, 38, 82, 12, 6, 35, 174, 97, 34, 197, 96, 48, 
226, 26, 38, 82, 12, 6, 35, 174, 97, 34, 197, 96, 
48, 226, 26, 85, 87, 27, 192, 136, 18, 147, 201, 20, 
112, 191, 36, 73, 158, 109, 103, 166, 174, 243, 127, 142, 
227, 8, 33, 161, 143, 111, 15, 33, 178, 130, 9, 33, 
148, 82, 207, 1, 132, 16, 231, 30, 127, 35, 61, 7, 
4, 44, 223, 251, 152, 236, 236, 236, 152, 152, 205, 136, 
127, 152, 72, 37, 42, 1, 107, 50, 0, 158, 231, 3, 
238, 151, 36, 73, 209, 232, 2, 167, 148, 192, 75, 62, 
194, 158, 238, 115, 188, 135, 72, 246, 68, 104, 82, 20, 
103, 49, 18, 29, 38, 82, 137, 74, 36, 53, 86, 181, 
99, 151, 184, 100, 153, 227, 215, 117, 114, 69, 37, 36, 
137, 24, 147, 249, 81, 35, 213, 231, 205, 112, 140, 31, 
43, 27, 12, 33, 202, 161, 170, 150, 77, 85, 223, 238, 
171, 94, 238, 144, 77, 132, 4, 86, 189, 128, 80, 42, 
233, 5, 227, 144, 140, 11, 135, 165, 92, 236, 189, 223, 
44, 85, 238, 110, 252, 110, 95, 205, 234, 200, 139, 106, 
189, 10, 78, 51, 169, 231, 173, 5, 194, 132, 40, 206, 
101, 156, 0, 16, 54, 118, 47, 65, 49, 155, 205, 33, 
190, 229, 215, 109, 112, 124, 187, 72, 220, 188, 149, 214, 
53, 192, 187, 65, 71, 8, 244, 58, 213, 73, 3, 133, 
115, 103, 136, 83, 39, 211, 180, 84, 255, 115, 37, 190, 
121, 99, 249, 167, 7, 106, 87, 57, 100, 27, 16, 184, 
145, 24, 12, 74, 41, 32, 235, 133, 212, 225, 89, 23, 
12, 79, 191, 88, 114, 112, 0, 76, 82, 249, 174, 250, 
175, 247, 215, 174, 162, 160, 132, 8, 145, 151, 230, 65, 
69, 52, 227, 11, 174, 235, 167, 157, 238, 217, 147, 153, 
153, 25, 69, 57, 140, 68, 132, 137, 84, 162, 18, 76, 
164, 116, 69, 69, 150, 55, 222, 178, 47, 253, 9, 118, 
27, 12, 6, 18, 176, 245, 231, 176, 81, 171, 157, 27, 
208, 95, 123, 235, 141, 242, 244, 41, 178, 209, 232, 253, 
229, 246, 218, 5, 91, 43, 191, 178, 137, 102, 158, 83, 
71, 209, 194, 162, 148, 74, 178, 221, 32, 164, 143, 206, 
185, 234, 36, 227, 57, 14, 152, 246, 53, 253, 184, 174, 
244, 99, 65, 165, 71, 180, 77, 54, 74, 41, 7, 114, 
205, 192, 127, 171, 224, 10, 69, 49, 145, 234, 62, 176, 
222, 189, 19, 10, 126, 237, 250, 198, 155, 239, 178, 255, 
176, 20, 130, 10, 201, 201, 224, 130, 252, 190, 42, 53, 
146, 146, 228, 162, 195, 205, 179, 159, 22, 222, 122, 87, 
85, 81, 65, 220, 80, 222, 90, 103, 59, 34, 81, 71, 
116, 10, 5, 128, 16, 194, 17, 222, 46, 155, 235, 154, 
75, 8, 33, 22, 177, 174, 210, 188, 159, 66, 246, 4, 
185, 162, 130, 90, 69, 83, 137, 245, 15, 143, 157, 209, 
150, 195, 72, 60, 88, 76, 42, 81, 241, 119, 129, 85, 
191, 109, 104, 190, 251, 1, 218, 212, 4, 189, 14, 36, 
100, 51, 141, 16, 2, 80, 189, 30, 128, 105, 238, 127, 
147, 29, 34, 185, 229, 70, 41, 63, 31, 128, 76, 36, 
10, 9, 237, 139, 82, 187, 207, 165, 206, 191, 35, 65, 
66, 251, 85, 133, 160, 197, 209, 8, 93, 187, 203, 97, 
36, 26, 204, 147, 58, 65, 48, 28, 62, 220, 242, 208, 
99, 104, 106, 130, 70, 75, 66, 43, 148, 27, 151, 75, 
162, 211, 154, 231, 127, 35, 47, 89, 78, 234, 27, 0, 
16, 112, 136, 129, 162, 56, 203, 247, 82, 186, 246, 250, 
62, 196, 89, 2, 243, 164, 186, 33, 76, 164, 78, 16, 
204, 175, 191, 37, 87, 86, 81, 181, 134, 16, 162, 72, 
17, 8, 33, 112, 56, 108, 159, 254, 87, 218, 189, 23, 
46, 49, 136, 149, 4, 48, 41, 97, 196, 0, 38, 82, 
39, 2, 170, 117, 27, 237, 203, 87, 67, 171, 5, 199, 
69, 227, 179, 168, 53, 242, 209, 50, 245, 242, 229, 164, 
180, 52, 182, 253, 40, 60, 207, 147, 96, 113, 49, 6, 
35, 50, 216, 3, 116, 34, 224, 248, 110, 17, 136, 132, 
168, 219, 65, 132, 64, 16, 90, 214, 253, 46, 30, 45, 
139, 161, 85, 172, 105, 198, 136, 9, 76, 164, 18, 21, 
143, 4, 8, 59, 118, 137, 91, 182, 17, 65, 211, 174, 
80, 183, 70, 35, 31, 59, 174, 57, 112, 80, 109, 18, 
65, 99, 213, 157, 66, 41, 165, 96, 57, 46, 140, 246, 
193, 68, 42, 81, 241, 136, 148, 99, 241, 50, 90, 215, 
0, 133, 161, 40, 255, 226, 104, 179, 149, 238, 46, 66, 
77, 67, 236, 98, 82, 177, 135, 115, 211, 213, 134, 48, 
58, 15, 150, 130, 144, 168, 200, 178, 236, 220, 16, 127, 
91, 15, 81, 140, 65, 137, 60, 145, 106, 170, 136, 197, 
2, 99, 248, 99, 187, 10, 214, 120, 236, 134, 48, 145, 
74, 84, 60, 34, 37, 87, 215, 196, 166, 68, 66, 104, 
115, 11, 28, 142, 216, 148, 6, 80, 138, 182, 51, 23, 
196, 0, 230, 67, 117, 67, 152, 72, 37, 42, 162, 199, 
123, 146, 99, 51, 215, 10, 224, 10, 34, 1, 241, 27, 
69, 98, 158, 84, 55, 132, 189, 151, 18, 21, 217, 13, 
140, 177, 107, 158, 105, 212, 224, 185, 248, 149, 40, 214, 
99, 216, 45, 97, 34, 149, 240, 232, 78, 29, 31, 116, 
140, 158, 34, 68, 89, 200, 78, 135, 65, 27, 187, 254, 
56, 42, 203, 50, 165, 114, 140, 74, 99, 116, 83, 152, 
72, 37, 42, 30, 159, 66, 115, 233, 121, 36, 35, 45, 
6, 37, 170, 85, 164, 95, 31, 164, 164, 176, 156, 1, 
70, 92, 193, 68, 42, 225, 49, 157, 52, 136, 235, 223, 
159, 54, 55, 83, 185, 29, 62, 139, 221, 206, 101, 103, 
89, 250, 245, 117, 164, 106, 192, 197, 42, 200, 69, 148, 
142, 209, 9, 11, 117, 19, 195, 50, 25, 113, 14, 19, 
169, 68, 197, 227, 73, 73, 106, 181, 230, 242, 75, 208, 
190, 70, 21, 181, 218, 116, 99, 70, 114, 133, 249, 224, 
99, 24, 240, 137, 125, 50, 39, 19, 169, 110, 8, 19, 
169, 68, 133, 120, 97, 59, 245, 20, 126, 220, 104, 180, 
180, 68, 231, 76, 209, 230, 102, 146, 155, 109, 157, 62, 
85, 42, 44, 140, 161, 133, 148, 82, 73, 146, 228, 246, 
248, 119, 129, 202, 100, 34, 213, 221, 96, 34, 149, 168, 
120, 139, 20, 213, 233, 244, 143, 60, 64, 210, 211, 97, 
183, 41, 246, 92, 236, 54, 128, 234, 47, 255, 19, 55, 
102, 52, 52, 26, 10, 153, 182, 211, 43, 115, 211, 17, 
121, 82, 76, 164, 186, 33, 76, 164, 18, 21, 210, 22, 
219, 224, 65, 186, 199, 30, 36, 58, 29, 108, 86, 68, 
222, 161, 102, 183, 81, 155, 77, 115, 217, 37, 226, 249, 
231, 201, 233, 105, 132, 16, 136, 106, 72, 10, 86, 94, 
8, 106, 30, 56, 231, 10, 14, 4, 28, 15, 21, 228, 
118, 202, 10, 133, 76, 147, 212, 25, 237, 55, 140, 145, 
112, 48, 145, 58, 65, 144, 85, 42, 113, 218, 89, 186, 
217, 143, 34, 45, 141, 90, 44, 145, 180, 251, 104, 115, 
51, 5, 81, 95, 125, 133, 124, 221, 213, 142, 158, 5, 
50, 199, 81, 74, 169, 68, 50, 116, 125, 120, 162, 118, 
72, 214, 168, 141, 17, 101, 187, 192, 235, 179, 12, 125, 
1, 36, 107, 178, 242, 141, 195, 121, 142, 23, 37, 91, 
116, 165, 81, 42, 219, 36, 139, 86, 72, 233, 153, 52, 
154, 121, 82, 221, 16, 150, 113, 158, 168, 168, 84, 173, 
191, 157, 171, 222, 38, 37, 201, 231, 76, 55, 228, 102, 
219, 62, 252, 212, 177, 110, 3, 181, 219, 161, 215, 7, 
92, 136, 129, 54, 55, 67, 146, 184, 222, 61, 117, 151, 
95, 34, 207, 152, 46, 22, 228, 115, 42, 149, 167, 230, 
247, 55, 78, 110, 17, 27, 247, 215, 175, 52, 219, 107, 
16, 77, 211, 143, 51, 106, 178, 7, 167, 159, 211, 211, 
112, 42, 33, 68, 5, 93, 190, 110, 204, 176, 236, 243, 
247, 213, 252, 100, 19, 3, 47, 104, 26, 14, 98, 84, 
103, 77, 40, 184, 94, 37, 167, 202, 44, 235, 170, 251, 
193, 86, 139, 73, 84, 44, 22, 11, 2, 173, 220, 201, 
57, 28, 242, 209, 50, 238, 247, 77, 226, 210, 21, 226, 
129, 67, 180, 170, 26, 62, 107, 20, 235, 116, 124, 223, 
222, 194, 105, 227, 165, 51, 39, 146, 65, 3, 168, 209, 
8, 149, 10, 109, 39, 77, 183, 136, 117, 245, 142, 67, 
38, 177, 82, 162, 118, 165, 134, 169, 137, 46, 69, 85, 
144, 44, 20, 234, 248, 84, 103, 177, 50, 21, 91, 164, 
250, 58, 251, 17, 147, 124, 44, 138, 43, 21, 136, 54, 
133, 244, 77, 211, 21, 10, 68, 239, 217, 153, 158, 158, 
30, 69, 81, 140, 68, 132, 137, 84, 162, 210, 212, 212, 
4, 47, 101, 241, 36, 14, 56, 55, 56, 139, 69, 174, 
172, 82, 213, 153, 232, 129, 253, 252, 209, 35, 48, 139, 
160, 132, 168, 169, 148, 155, 141, 254, 39, 137, 57, 25, 
36, 43, 67, 78, 75, 37, 130, 32, 73, 82, 192, 164, 
3, 25, 14, 137, 218, 157, 65, 116, 159, 53, 208, 9, 
225, 188, 246, 248, 169, 36, 120, 158, 104, 8, 120, 207, 
126, 247, 128, 64, 73, 68, 107, 19, 146, 82, 26, 124, 
144, 160, 199, 30, 10, 0, 148, 83, 65, 135, 182, 3, 
247, 50, 50, 88, 124, 170, 187, 192, 68, 42, 81, 169, 
169, 113, 77, 126, 224, 25, 203, 102, 33, 101, 0, 245, 
158, 12, 138, 147, 1, 42, 17, 171, 21, 162, 12, 16, 
112, 148, 106, 181, 48, 36, 203, 14, 187, 39, 184, 78, 
41, 53, 112, 57, 222, 78, 138, 179, 76, 137, 54, 183, 
72, 245, 14, 4, 8, 36, 17, 66, 60, 242, 66, 253, 
132, 70, 197, 105, 53, 36, 133, 147, 52, 161, 237, 143, 
228, 193, 115, 169, 155, 159, 16, 131, 137, 84, 119, 130, 
197, 164, 18, 21, 135, 195, 1, 119, 189, 181, 11, 149, 
155, 203, 231, 215, 219, 143, 16, 255, 5, 135, 121, 62, 
192, 28, 118, 146, 228, 173, 45, 6, 85, 214, 176, 236, 
115, 242, 116, 35, 84, 196, 181, 98, 212, 17, 211, 134, 
131, 245, 171, 155, 236, 229, 81, 164, 35, 16, 194, 165, 
106, 10, 6, 166, 76, 205, 55, 140, 130, 151, 39, 229, 
119, 88, 208, 156, 81, 159, 227, 9, 97, 175, 210, 110, 
13, 19, 169, 68, 69, 146, 92, 171, 227, 57, 52, 213, 
191, 151, 127, 90, 218, 184, 85, 134, 76, 16, 205, 48, 
148, 42, 90, 100, 17, 107, 38, 230, 223, 156, 173, 29, 
10, 202, 29, 111, 217, 190, 173, 106, 126, 165, 101, 127, 
212, 35, 90, 106, 154, 143, 52, 59, 26, 52, 130, 33, 
71, 55, 216, 153, 210, 169, 232, 116, 167, 126, 81, 74, 
189, 55, 152, 78, 117, 91, 152, 72, 37, 42, 30, 79, 
100, 115, 229, 188, 50, 211, 31, 50, 40, 71, 84, 209, 
13, 105, 161, 160, 149, 230, 3, 71, 77, 219, 245, 36, 
87, 203, 165, 30, 105, 216, 84, 211, 82, 68, 33, 243, 
68, 29, 157, 109, 162, 100, 175, 178, 28, 56, 106, 218, 
150, 171, 31, 2, 128, 15, 216, 195, 24, 60, 147, 192, 
41, 73, 254, 82, 21, 157, 49, 140, 68, 135, 137, 84, 
162, 194, 113, 174, 232, 117, 153, 233, 15, 137, 74, 28, 
137, 126, 208, 29, 79, 84, 34, 90, 76, 142, 170, 22, 
135, 89, 208, 233, 172, 114, 131, 76, 101, 142, 8, 81, 
219, 198, 115, 42, 153, 74, 22, 91, 157, 51, 42, 31, 
76, 164, 130, 121, 88, 174, 52, 122, 166, 74, 12, 0, 
76, 164, 18, 26, 167, 42, 201, 84, 130, 127, 40, 74, 
121, 89, 20, 146, 44, 73, 132, 163, 224, 64, 72, 59, 
179, 124, 41, 8, 149, 101, 201, 225, 112, 16, 66, 60, 
98, 228, 137, 241, 123, 91, 235, 217, 227, 217, 25, 44, 
134, 229, 84, 46, 38, 94, 221, 13, 38, 82, 9, 15, 
33, 92, 12, 215, 119, 137, 221, 10, 198, 212, 219, 87, 
242, 86, 34, 159, 169, 53, 253, 119, 122, 154, 120, 8, 
36, 103, 140, 238, 6, 19, 41, 70, 71, 225, 211, 160, 
243, 118, 163, 188, 125, 34, 127, 145, 226, 56, 206, 71, 
164, 188, 195, 82, 76, 170, 186, 27, 76, 164, 24, 94, 
16, 32, 150, 139, 238, 121, 70, 218, 81, 111, 173, 241, 
111, 184, 249, 124, 235, 89, 18, 198, 121, 164, 207, 10, 
49, 62, 142, 24, 227, 132, 135, 137, 20, 163, 179, 241, 
164, 104, 250, 140, 233, 241, 72, 143, 40, 138, 222, 130, 
229, 35, 73, 76, 164, 186, 27, 76, 164, 24, 173, 144, 
88, 197, 163, 218, 162, 104, 177, 60, 103, 35, 209, 35, 
82, 178, 44, 123, 119, 246, 249, 199, 221, 25, 39, 60, 
76, 164, 24, 29, 139, 191, 227, 227, 29, 141, 10, 118, 
138, 255, 193, 222, 95, 49, 145, 234, 86, 48, 145, 98, 
116, 20, 206, 153, 57, 209, 54, 236, 237, 77, 232, 253, 
33, 75, 102, 89, 8, 221, 8, 38, 82, 140, 14, 131, 
40, 115, 121, 2, 14, 36, 246, 254, 150, 57, 80, 221, 
19, 38, 82, 137, 138, 162, 64, 79, 132, 196, 118, 253, 
41, 103, 216, 59, 180, 244, 4, 59, 49, 118, 86, 48, 
18, 30, 38, 82, 137, 74, 135, 212, 228, 152, 166, 32, 
16, 144, 72, 134, 221, 177, 17, 48, 140, 208, 48, 145, 
74, 84, 188, 68, 42, 222, 253, 142, 16, 122, 234, 31, 
20, 15, 125, 60, 163, 27, 194, 68, 42, 81, 241, 159, 
27, 51, 17, 33, 109, 39, 60, 232, 106, 115, 24, 241, 
8, 19, 41, 70, 43, 49, 207, 147, 138, 164, 159, 206, 
39, 7, 42, 146, 179, 24, 221, 10, 182, 164, 85, 162, 
226, 63, 82, 55, 17, 97, 121, 79, 140, 176, 48, 79, 
42, 81, 233, 136, 152, 20, 1, 161, 52, 102, 43, 24, 
147, 144, 41, 8, 222, 189, 126, 172, 197, 199, 8, 1, 
19, 169, 68, 37, 198, 222, 135, 76, 189, 250, 246, 98, 
83, 178, 39, 153, 51, 248, 1, 161, 210, 56, 125, 190, 
101, 121, 82, 221, 22, 214, 220, 75, 120, 10, 146, 71, 
0, 114, 59, 125, 16, 66, 248, 100, 85, 150, 192, 235, 
169, 168, 210, 114, 169, 132, 112, 146, 44, 70, 93, 154, 
44, 75, 28, 17, 116, 66, 106, 123, 76, 242, 192, 38, 
186, 235, 230, 48, 79, 42, 81, 241, 184, 21, 35, 51, 
46, 21, 101, 235, 145, 134, 205, 148, 208, 168, 61, 141, 
108, 67, 255, 28, 237, 16, 13, 151, 68, 37, 174, 80, 
127, 114, 181, 101, 95, 185, 101, 175, 93, 114, 68, 87, 
32, 71, 248, 44, 195, 128, 158, 169, 163, 67, 31, 22, 
204, 51, 242, 196, 209, 125, 54, 24, 221, 19, 38, 82, 
137, 10, 207, 243, 206, 170, 155, 169, 25, 56, 42, 243, 
74, 157, 144, 82, 103, 61, 42, 83, 135, 210, 114, 8, 
120, 163, 38, 187, 79, 210, 233, 233, 234, 254, 28, 4, 
16, 228, 232, 6, 159, 156, 117, 169, 81, 187, 174, 193, 
90, 38, 65, 113, 129, 42, 162, 73, 81, 23, 246, 73, 
62, 61, 93, 232, 71, 163, 242, 198, 252, 229, 137, 197, 
215, 187, 51, 236, 29, 149, 168, 120, 175, 96, 76, 41, 
181, 208, 202, 6, 235, 113, 89, 118, 4, 251, 65, 131, 
47, 22, 204, 37, 107, 178, 13, 92, 54, 143, 214, 181, 
97, 8, 71, 77, 98, 185, 201, 94, 45, 83, 71, 235, 
137, 174, 133, 136, 195, 192, 19, 117, 146, 42, 59, 137, 
207, 9, 125, 88, 216, 70, 156, 255, 10, 242, 222, 34, 
149, 155, 155, 27, 206, 16, 198, 9, 2, 19, 169, 68, 
165, 177, 177, 209, 103, 143, 243, 167, 148, 229, 192, 125, 
115, 81, 252, 208, 62, 61, 110, 62, 235, 9, 183, 19, 
165, 10, 133, 182, 34, 149, 147, 19, 70, 4, 25, 39, 
12, 172, 185, 151, 168, 136, 162, 171, 41, 69, 8, 225, 
85, 180, 196, 180, 165, 209, 90, 238, 116, 80, 2, 30, 
31, 66, 20, 146, 53, 89, 57, 186, 161, 58, 190, 53, 
206, 221, 44, 213, 84, 181, 20, 91, 236, 85, 238, 246, 
163, 183, 59, 21, 70, 164, 8, 132, 100, 109, 94, 142, 
174, 159, 26, 70, 183, 133, 176, 210, 186, 50, 243, 110, 
171, 216, 72, 169, 228, 46, 39, 220, 21, 182, 254, 81, 
46, 93, 215, 43, 71, 61, 44, 236, 9, 140, 19, 18, 
38, 82, 137, 138, 71, 164, 4, 13, 41, 50, 173, 219, 
90, 49, 223, 108, 175, 138, 110, 136, 140, 86, 72, 237, 
159, 114, 214, 160, 148, 233, 73, 66, 22, 0, 139, 84, 
189, 191, 113, 121, 81, 195, 134, 102, 71, 45, 148, 199, 
164, 0, 33, 73, 147, 51, 48, 109, 98, 191, 228, 41, 
26, 24, 1, 152, 196, 242, 125, 77, 75, 138, 27, 55, 
58, 36, 139, 71, 164, 148, 64, 140, 218, 30, 67, 83, 
46, 238, 147, 52, 81, 249, 185, 140, 132, 135, 137, 84, 
162, 226, 89, 136, 229, 184, 101, 227, 150, 138, 175, 106, 
155, 75, 120, 78, 136, 46, 178, 92, 223, 114, 108, 151, 
227, 123, 163, 38, 187, 143, 48, 81, 5, 109, 153, 121, 
219, 254, 186, 159, 154, 108, 21, 60, 39, 68, 17, 171, 
166, 212, 94, 109, 217, 39, 202, 205, 122, 62, 183, 183, 
126, 188, 131, 54, 31, 179, 108, 221, 85, 189, 68, 162, 
162, 138, 139, 230, 121, 163, 148, 86, 152, 247, 218, 165, 
230, 220, 164, 147, 116, 200, 140, 162, 4, 70, 66, 195, 
68, 42, 81, 241, 196, 158, 182, 84, 46, 104, 180, 149, 
171, 120, 109, 212, 157, 95, 106, 149, 170, 217, 81, 95, 
99, 59, 216, 67, 63, 66, 37, 8, 85, 214, 131, 45, 
98, 35, 207, 105, 56, 46, 192, 202, 195, 97, 33, 4, 
60, 136, 197, 222, 80, 105, 57, 208, 199, 48, 193, 42, 
55, 212, 216, 247, 59, 164, 22, 141, 144, 28, 157, 121, 
132, 64, 77, 248, 250, 150, 210, 122, 233, 160, 65, 200, 
142, 174, 16, 70, 226, 194, 146, 57, 19, 30, 147, 189, 
42, 6, 43, 24, 115, 196, 46, 89, 28, 162, 141, 114, 
162, 4, 27, 8, 137, 78, 161, 220, 133, 113, 132, 200, 
148, 90, 57, 142, 147, 168, 195, 42, 153, 193, 181, 51, 
123, 128, 80, 2, 147, 173, 186, 125, 133, 48, 18, 18, 
38, 82, 137, 138, 215, 114, 6, 124, 172, 18, 136, 36, 
89, 162, 52, 38, 3, 247, 40, 32, 195, 57, 10, 58, 
118, 67, 11, 165, 232, 210, 174, 24, 9, 14, 107, 238, 
49, 90, 145, 229, 246, 14, 175, 233, 104, 88, 62, 103, 
55, 132, 137, 84, 162, 210, 17, 213, 53, 214, 99, 228, 
40, 27, 118, 199, 104, 63, 76, 164, 18, 21, 239, 53, 
14, 226, 19, 231, 50, 159, 49, 106, 63, 186, 96, 158, 
84, 55, 132, 137, 84, 162, 226, 189, 174, 111, 124, 66, 
41, 100, 89, 14, 150, 1, 207, 96, 68, 8, 11, 156, 
51, 90, 137, 237, 146, 86, 78, 79, 74, 98, 34, 197, 
104, 31, 204, 147, 74, 84, 226, 217, 135, 114, 226, 106, 
238, 201, 177, 180, 147, 53, 247, 186, 33, 76, 164, 18, 
149, 68, 168, 174, 206, 176, 121, 44, 69, 42, 254, 165, 
153, 17, 115, 152, 72, 37, 42, 241, 191, 56, 168, 107, 
250, 224, 152, 202, 10, 19, 169, 110, 8, 19, 169, 68, 
165, 67, 150, 89, 143, 245, 146, 86, 177, 134, 122, 70, 
44, 50, 186, 15, 76, 164, 24, 173, 196, 54, 112, 238, 
90, 45, 38, 166, 30, 159, 103, 238, 7, 70, 247, 129, 
137, 84, 162, 210, 33, 13, 159, 152, 54, 247, 92, 105, 
18, 49, 245, 205, 88, 66, 67, 55, 132, 137, 84, 162, 
18, 255, 209, 25, 66, 192, 113, 92, 59, 86, 135, 96, 
48, 0, 38, 82, 12, 31, 98, 218, 25, 23, 123, 79, 
42, 17, 250, 52, 25, 49, 134, 137, 84, 162, 210, 17, 
129, 243, 248, 247, 206, 24, 221, 16, 150, 113, 158, 240, 
80, 26, 159, 83, 23, 196, 222, 229, 33, 236, 113, 237, 
150, 176, 95, 61, 225, 209, 240, 250, 24, 204, 95, 64, 
41, 79, 212, 28, 81, 81, 9, 160, 60, 40, 109, 207, 
192, 96, 10, 128, 242, 132, 8, 132, 16, 158, 240, 42, 
162, 105, 119, 182, 20, 37, 20, 122, 33, 149, 184, 105, 
95, 105, 140, 68, 130, 53, 247, 18, 158, 254, 169, 83, 
15, 54, 44, 55, 217, 235, 84, 156, 16, 157, 255, 34, 
81, 81, 224, 116, 169, 234, 66, 13, 159, 36, 217, 184, 
100, 62, 87, 224, 180, 45, 98, 19, 207, 169, 162, 40, 
144, 130, 138, 178, 77, 43, 164, 166, 104, 123, 2, 208, 
240, 41, 233, 234, 62, 60, 54, 138, 178, 157, 39, 209, 
61, 111, 84, 148, 29, 58, 85, 106, 186, 186, 47, 145, 
152, 60, 117, 59, 152, 72, 37, 60, 39, 167, 94, 73, 
8, 118, 215, 46, 146, 169, 24, 93, 148, 154, 35, 154, 
194, 164, 49, 217, 170, 225, 2, 12, 20, 180, 103, 242, 
88, 147, 88, 86, 218, 180, 205, 33, 183, 68, 49, 235, 
175, 12, 154, 164, 206, 232, 155, 50, 174, 111, 202, 24, 
34, 19, 45, 73, 233, 161, 57, 165, 32, 249, 112, 69, 
243, 86, 46, 42, 15, 136, 130, 234, 4, 227, 224, 212, 
115, 13, 114, 97, 124, 231, 154, 50, 58, 132, 184, 158, 
235, 131, 17, 130, 170, 170, 42, 239, 143, 7, 45, 203, 
42, 45, 7, 40, 228, 80, 189, 115, 65, 106, 120, 186, 
182, 87, 111, 195, 105, 122, 46, 203, 249, 145, 82, 106, 
229, 106, 142, 152, 182, 212, 54, 23, 251, 47, 105, 21, 
188, 251, 207, 213, 8, 227, 57, 77, 142, 161, 127, 161, 
126, 140, 32, 183, 46, 228, 103, 145, 42, 15, 91, 126, 
105, 180, 150, 135, 188, 38, 103, 249, 190, 112, 132, 203, 
79, 26, 222, 75, 59, 137, 45, 14, 218, 61, 97, 34, 
149, 168, 248, 136, 148, 135, 16, 63, 104, 216, 80, 142, 
207, 186, 193, 1, 143, 15, 150, 78, 73, 8, 9, 216, 
225, 24, 195, 201, 57, 153, 72, 117, 79, 88, 115, 47, 
81, 241, 169, 249, 34, 105, 182, 138, 77, 50, 149, 148, 
138, 20, 33, 68, 163, 74, 18, 96, 32, 148, 247, 20, 
75, 120, 216, 37, 147, 77, 180, 200, 126, 107, 121, 6, 
45, 159, 128, 35, 28, 0, 142, 240, 26, 46, 89, 32, 
134, 48, 199, 51, 24, 145, 193, 68, 42, 81, 241, 30, 
106, 219, 34, 215, 84, 72, 91, 202, 204, 187, 236, 82, 
179, 210, 114, 120, 78, 200, 210, 247, 233, 161, 25, 99, 
228, 122, 241, 80, 3, 160, 68, 108, 166, 21, 165, 45, 
191, 87, 89, 14, 137, 178, 85, 105, 129, 106, 94, 159, 
173, 29, 156, 175, 61, 37, 89, 200, 166, 18, 129, 194, 
148, 174, 16, 158, 23, 165, 148, 245, 235, 117, 67, 152, 
72, 37, 42, 158, 154, 108, 165, 117, 187, 205, 11, 119, 
87, 45, 149, 137, 20, 93, 224, 188, 184, 126, 67, 161, 
113, 223, 232, 140, 107, 211, 85, 253, 64, 57, 11, 57, 
190, 169, 226, 179, 35, 141, 155, 41, 162, 44, 240, 8, 
183, 169, 95, 90, 209, 41, 217, 215, 168, 37, 182, 224, 
48, 163, 189, 48, 145, 74, 84, 156, 177, 33, 66, 200, 
62, 203, 247, 7, 235, 215, 8, 42, 125, 123, 188, 140, 
163, 77, 219, 10, 141, 35, 147, 85, 121, 106, 206, 80, 
220, 176, 161, 178, 249, 0, 207, 9, 42, 206, 16, 93, 
105, 162, 100, 59, 214, 180, 61, 93, 232, 115, 82, 210, 
133, 81, 156, 30, 240, 66, 88, 179, 177, 219, 194, 146, 
57, 19, 21, 81, 20, 69, 81, 116, 56, 28, 7, 234, 
126, 141, 193, 10, 198, 20, 13, 182, 114, 155, 100, 230, 
181, 212, 66, 171, 101, 42, 169, 56, 117, 212, 133, 241, 
156, 74, 162, 182, 38, 91, 101, 187, 76, 98, 48, 0, 
48, 79, 42, 113, 137, 241, 204, 74, 28, 1, 100, 2, 
0, 114, 160, 52, 128, 206, 35, 132, 218, 50, 103, 170, 
123, 194, 68, 42, 81, 145, 164, 80, 29, 121, 209, 65, 
41, 37, 224, 216, 16, 57, 70, 92, 193, 68, 138, 209, 
138, 40, 137, 241, 48, 171, 92, 48, 103, 138, 121, 82, 
221, 19, 38, 82, 137, 77, 108, 235, 173, 195, 225, 232, 
104, 145, 138, 218, 96, 166, 80, 221, 22, 38, 82, 137, 
10, 117, 19, 195, 50, 69, 49, 150, 158, 20, 65, 224, 
28, 116, 15, 161, 141, 247, 255, 150, 37, 73, 117, 79, 
152, 72, 49, 218, 16, 67, 205, 11, 166, 161, 78, 173, 
9, 155, 25, 239, 35, 73, 222, 165, 49, 181, 234, 86, 
48, 145, 74, 108, 226, 185, 186, 82, 74, 157, 126, 153, 
103, 36, 96, 132, 214, 6, 212, 47, 214, 220, 235, 182, 
48, 145, 74, 84, 120, 158, 143, 243, 122, 43, 203, 178, 
195, 225, 240, 8, 19, 105, 139, 143, 241, 222, 250, 229, 
28, 241, 19, 208, 159, 10, 184, 135, 113, 98, 195, 68, 
42, 81, 81, 169, 84, 49, 143, 73, 33, 166, 147, 254, 
202, 178, 108, 183, 219, 1, 112, 28, 231, 81, 28, 239, 
109, 111, 253, 242, 30, 151, 231, 221, 172, 243, 31, 175, 
199, 70, 240, 117, 55, 152, 72, 37, 42, 97, 35, 59, 
209, 150, 25, 179, 250, 239, 20, 41, 66, 8, 207, 243, 
240, 242, 140, 60, 18, 19, 112, 127, 176, 169, 26, 226, 
220, 109, 100, 116, 28, 76, 164, 18, 21, 89, 150, 99, 
238, 73, 197, 124, 246, 112, 239, 152, 148, 207, 31, 130, 
123, 118, 4, 239, 54, 32, 218, 170, 149, 207, 87, 62, 
167, 51, 186, 9, 76, 164, 18, 149, 96, 245, 191, 61, 
196, 214, 147, 242, 224, 52, 210, 103, 70, 61, 180, 245, 
161, 124, 98, 85, 1, 21, 74, 165, 82, 49, 121, 234, 
134, 48, 145, 74, 84, 58, 98, 221, 189, 216, 122, 82, 
132, 16, 143, 172, 248, 100, 117, 57, 197, 200, 123, 74, 
172, 128, 49, 41, 31, 120, 158, 15, 232, 88, 49, 78, 
108, 152, 72, 49, 58, 10, 226, 158, 80, 56, 96, 71, 
158, 231, 91, 111, 197, 9, 22, 65, 119, 226, 73, 52, 
101, 34, 213, 173, 96, 34, 149, 168, 36, 64, 69, 37, 
190, 209, 125, 31, 61, 114, 6, 206, 157, 248, 31, 227, 
84, 43, 150, 51, 197, 96, 34, 149, 168, 180, 86, 120, 
74, 41, 98, 211, 43, 79, 8, 161, 161, 214, 131, 137, 
190, 88, 231, 134, 143, 18, 121, 82, 189, 252, 93, 45, 
159, 61, 206, 143, 29, 209, 194, 101, 196, 63, 76, 164, 
18, 21, 47, 85, 18, 157, 181, 184, 93, 58, 229, 14, 
87, 83, 42, 129, 210, 118, 23, 200, 129, 242, 254, 123, 
125, 60, 41, 4, 241, 7, 253, 157, 169, 176, 225, 42, 
198, 9, 12, 123, 53, 37, 42, 158, 16, 114, 178, 166, 
7, 32, 181, 39, 29, 129, 82, 10, 10, 53, 73, 82, 
17, 181, 181, 9, 196, 174, 38, 0, 245, 91, 39, 38, 
114, 100, 89, 164, 144, 120, 162, 241, 49, 213, 219, 120, 
180, 141, 166, 251, 116, 231, 113, 28, 231, 73, 251, 100, 
116, 115, 152, 72, 37, 42, 158, 250, 124, 118, 191, 7, 
147, 133, 92, 155, 108, 166, 84, 166, 81, 97, 151, 45, 
6, 117, 90, 58, 134, 168, 105, 26, 128, 220, 228, 161, 
90, 85, 170, 72, 29, 209, 149, 70, 41, 149, 32, 38, 
169, 179, 122, 26, 79, 118, 106, 141, 191, 229, 206, 13, 
79, 170, 151, 143, 78, 57, 229, 201, 63, 5, 193, 35, 
94, 172, 221, 215, 173, 96, 137, 188, 137, 74, 105, 105, 
169, 103, 91, 52, 28, 95, 85, 242, 102, 125, 75, 41, 
69, 20, 19, 173, 16, 189, 42, 125, 124, 193, 181, 153, 
244, 100, 217, 174, 2, 64, 41, 45, 167, 27, 247, 55, 
45, 174, 105, 41, 166, 212, 181, 154, 158, 130, 226, 192, 
101, 233, 251, 156, 100, 188, 48, 11, 163, 60, 169, 6, 
30, 37, 242, 198, 163, 77, 240, 203, 56, 167, 238, 88, 
149, 119, 206, 170, 32, 8, 158, 3, 6, 12, 24, 160, 
252, 74, 25, 9, 9, 19, 169, 68, 197, 91, 164, 92, 
36, 55, 136, 212, 162, 180, 28, 21, 167, 129, 197, 72, 
69, 193, 219, 21, 2, 160, 77, 33, 146, 96, 18, 149, 
47, 228, 167, 230, 147, 84, 162, 177, 185, 209, 213, 90, 
244, 46, 19, 109, 131, 74, 193, 90, 115, 1, 15, 246, 
62, 158, 16, 194, 68, 170, 251, 192, 2, 231, 39, 16, 
166, 84, 21, 82, 131, 189, 117, 124, 106, 190, 95, 156, 
168, 141, 118, 80, 74, 109, 77, 32, 196, 72, 96, 84, 
106, 133, 3, 112, 64, 246, 22, 32, 234, 149, 76, 16, 
44, 171, 192, 27, 143, 51, 229, 147, 193, 224, 127, 45, 
140, 238, 0, 19, 169, 68, 37, 172, 27, 18, 147, 210, 
98, 130, 167, 29, 23, 204, 51, 98, 48, 66, 192, 154, 
123, 12, 6, 35, 174, 97, 189, 36, 12, 6, 35, 174, 
97, 34, 197, 96, 48, 226, 26, 38, 82, 12, 6, 35, 
174, 97, 34, 197, 96, 48, 226, 26, 38, 82, 12, 6, 
35, 174, 97, 34, 197, 96, 48, 226, 26, 38, 82, 12, 
6, 35, 174, 97, 34, 197, 96, 48, 226, 26, 38, 82, 
12, 6, 35, 174, 97, 34, 197, 96, 48, 226, 26, 38, 
82, 12, 6, 35, 174, 97, 34, 197, 96, 48, 226, 26, 
38, 82, 12, 6, 35, 174, 97, 34, 197, 96, 48, 226, 
26, 38, 82, 12, 6, 35, 174, 97, 34, 197, 96, 48, 
226, 26, 38, 82, 12, 6, 35, 174, 97, 34, 197, 96, 
116, 29, 180, 5, 63, 188, 133, 239, 139, 187, 218, 142, 
104, 233, 20, 251, 153, 72, 49, 24, 93, 199, 206, 231, 
113, 225, 157, 248, 203, 179, 80, 188, 202, 79, 124, 208, 
41, 246, 51, 145, 98, 48, 186, 10, 9, 235, 150, 0, 
64, 237, 126, 88, 18, 113, 169, 129, 78, 178, 159, 137, 
20, 131, 209, 85, 80, 88, 29, 0, 128, 26, 212, 56, 
186, 216, 150, 104, 232, 36, 251, 153, 72, 49, 24, 93, 
5, 129, 78, 0, 0, 212, 161, 46, 17, 69, 170, 147, 
236, 103, 34, 197, 96, 116, 21, 60, 50, 146, 1, 0, 
13, 168, 79, 68, 145, 234, 36, 251, 153, 72, 49, 24, 
93, 71, 70, 15, 0, 128, 136, 178, 166, 174, 52, 99, 
231, 27, 200, 209, 97, 250, 108, 84, 138, 202, 78, 236, 
20, 251, 153, 72, 49, 24, 93, 71, 86, 127, 215, 70, 
105, 125, 151, 217, 32, 29, 199, 29, 15, 160, 202, 138, 
149, 115, 112, 237, 60, 72, 74, 206, 237, 20, 251, 153, 
72, 49, 226, 24, 185, 14, 115, 46, 68, 118, 42, 6, 
77, 197, 71, 59, 145, 136, 61, 96, 161, 201, 26, 224, 
218, 56, 214, 16, 217, 9, 50, 126, 152, 5, 206, 128, 
39, 54, 196, 238, 110, 80, 200, 238, 205, 67, 59, 161, 
168, 221, 166, 216, 254, 104, 136, 7, 145, 106, 217, 127, 
228, 134, 49, 75, 212, 228, 27, 66, 190, 209, 102, 175, 
152, 60, 107, 223, 215, 123, 68, 57, 252, 121, 140, 19, 
158, 125, 111, 98, 246, 15, 168, 110, 196, 129, 85, 248, 
203, 25, 248, 180, 164, 171, 13, 138, 53, 134, 124, 56, 
163, 58, 199, 35, 172, 228, 54, 44, 93, 9, 218, 140, 
151, 159, 66, 93, 140, 84, 138, 207, 199, 220, 143, 112, 
74, 58, 144, 141, 123, 175, 135, 86, 201, 185, 138, 237, 
143, 134, 174, 22, 41, 249, 216, 138, 253, 151, 76, 220, 
246, 201, 86, 155, 83, 192, 109, 213, 230, 53, 159, 237, 
157, 57, 116, 229, 245, 243, 90, 20, 54, 143, 25, 39, 
30, 199, 119, 120, 125, 104, 196, 95, 46, 195, 142, 150, 
46, 51, 166, 35, 80, 165, 34, 13, 0, 80, 119, 60, 
178, 118, 22, 133, 76, 1, 192, 190, 11, 135, 173, 49, 
51, 99, 224, 44, 108, 170, 5, 173, 196, 189, 67, 148, 
157, 168, 216, 254, 104, 232, 50, 145, 162, 114, 217, 175, 
197, 119, 156, 185, 180, 240, 236, 61, 203, 106, 252, 191, 
110, 249, 252, 222, 253, 155, 154, 59, 223, 44, 70, 92, 
81, 95, 213, 230, 163, 188, 5, 151, 205, 70, 83, 251, 
61, 8, 9, 155, 191, 194, 27, 239, 97, 119, 151, 134, 
171, 1, 240, 6, 232, 1, 0, 13, 199, 16, 209, 75, 
153, 64, 224, 1, 0, 229, 40, 141, 131, 250, 161, 216, 
254, 104, 80, 117, 84, 193, 33, 144, 171, 126, 59, 242, 
196, 131, 123, 63, 216, 96, 111, 221, 151, 155, 54, 243, 
138, 188, 105, 19, 140, 57, 117, 199, 239, 185, 163, 180, 
20, 64, 182, 33, 83, 232, 2, 227, 24, 241, 131, 132, 
26, 179, 239, 190, 67, 175, 226, 161, 115, 240, 206, 180, 
118, 189, 94, 255, 120, 22, 99, 159, 6, 128, 204, 141, 
40, 158, 139, 164, 118, 20, 213, 94, 120, 87, 21, 108, 
56, 10, 17, 208, 132, 61, 158, 131, 222, 89, 45, 100, 
28, 168, 1, 50, 58, 212, 184, 8, 80, 106, 127, 52, 
116, 182, 39, 101, 179, 124, 125, 207, 154, 222, 19, 255, 
240, 40, 20, 233, 147, 55, 251, 171, 169, 245, 199, 38, 
207, 255, 215, 160, 219, 174, 202, 251, 211, 95, 135, 62, 
119, 22, 1, 248, 243, 30, 45, 236, 203, 68, 170, 123, 
35, 161, 46, 144, 179, 240, 254, 53, 88, 82, 221, 142, 
98, 205, 120, 237, 223, 174, 205, 154, 223, 81, 222, 165, 
9, 74, 114, 11, 156, 151, 88, 95, 140, 250, 90, 172, 
91, 136, 39, 111, 197, 233, 39, 65, 79, 64, 8, 212, 
185, 184, 243, 187, 182, 205, 40, 14, 70, 119, 208, 232, 
96, 123, 110, 66, 140, 80, 108, 127, 52, 116, 166, 39, 
37, 213, 212, 60, 125, 209, 186, 103, 215, 123, 140, 214, 
93, 56, 103, 244, 219, 15, 101, 23, 168, 189, 14, 226, 
180, 127, 122, 227, 228, 199, 151, 235, 239, 186, 66, 203, 
119, 162, 109, 140, 56, 68, 70, 67, 192, 8, 84, 53, 
174, 191, 11, 123, 254, 139, 156, 168, 158, 16, 219, 97, 
252, 90, 231, 249, 0, 91, 151, 118, 209, 84, 174, 199, 
17, 231, 214, 90, 20, 102, 250, 126, 235, 168, 196, 225, 
42, 200, 64, 235, 133, 114, 72, 215, 187, 54, 75, 226, 
64, 164, 20, 219, 31, 13, 157, 38, 82, 82, 117, 229, 
189, 19, 215, 189, 121, 192, 253, 185, 111, 193, 251, 223, 
140, 186, 121, 132, 138, 248, 29, 153, 52, 162, 247, 156, 
17, 157, 101, 22, 35, 142, 145, 209, 24, 36, 54, 92, 
247, 63, 220, 121, 57, 230, 93, 22, 205, 211, 223, 82, 
130, 99, 158, 15, 90, 104, 59, 61, 44, 43, 153, 177, 
107, 35, 214, 174, 197, 234, 101, 248, 110, 61, 2, 139, 
164, 26, 147, 174, 198, 93, 15, 224, 146, 225, 109, 175, 
145, 71, 134, 187, 117, 90, 93, 2, 177, 43, 2, 54, 
237, 178, 63, 26, 58, 231, 18, 229, 186, 234, 71, 166, 
180, 42, 84, 218, 57, 195, 87, 254, 175, 255, 104, 99, 
167, 252, 109, 70, 194, 34, 195, 228, 45, 82, 249, 152, 
119, 63, 102, 61, 0, 103, 160, 96, 193, 173, 88, 50, 
9, 23, 102, 41, 46, 181, 102, 191, 87, 42, 80, 10, 
82, 58, 167, 10, 80, 84, 110, 195, 252, 175, 240, 221, 
98, 172, 218, 21, 164, 98, 3, 57, 195, 49, 125, 6, 
206, 57, 23, 231, 158, 129, 244, 32, 209, 142, 20, 183, 
195, 82, 123, 24, 142, 78, 19, 169, 216, 217, 175, 156, 
206, 184, 68, 171, 233, 95, 23, 175, 123, 101, 151, 235, 
83, 230, 37, 163, 215, 125, 209, 107, 128, 162, 108, 12, 
70, 183, 68, 134, 201, 214, 250, 73, 24, 134, 179, 239, 
196, 63, 191, 194, 125, 27, 1, 0, 117, 184, 235, 69, 
76, 123, 21, 58, 133, 165, 86, 31, 106, 221, 78, 206, 
135, 206, 223, 151, 143, 57, 86, 252, 223, 165, 184, 231, 
199, 144, 199, 232, 241, 117, 49, 46, 205, 14, 95, 152, 
49, 215, 181, 81, 91, 12, 7, 20, 95, 126, 52, 196, 
212, 126, 229, 116, 184, 72, 81, 113, 195, 211, 27, 239, 
95, 235, 146, 94, 221, 228, 225, 171, 63, 143, 153, 66, 
57, 202, 170, 62, 89, 169, 250, 211, 159, 211, 51, 59, 
55, 124, 229, 104, 180, 150, 30, 179, 86, 215, 139, 45, 
14, 168, 117, 170, 180, 60, 125, 159, 66, 181, 174, 171, 
83, 206, 78, 60, 100, 88, 188, 58, 128, 243, 134, 67, 
167, 198, 237, 115, 241, 217, 48, 108, 165, 0, 80, 242, 
49, 214, 60, 142, 115, 210, 149, 149, 90, 235, 149, 17, 
154, 217, 15, 157, 208, 59, 67, 205, 88, 186, 186, 245, 
163, 177, 16, 35, 79, 198, 200, 17, 24, 54, 4, 61, 
129, 191, 93, 139, 35, 0, 36, 232, 34, 171, 22, 41, 
249, 174, 13, 91, 37, 76, 50, 140, 177, 120, 238, 172, 
165, 88, 81, 132, 169, 147, 161, 15, 36, 217, 177, 181, 
95, 57, 29, 44, 82, 180, 102, 217, 142, 203, 94, 52, 
185, 62, 245, 233, 251, 221, 130, 126, 195, 244, 33, 207, 
80, 128, 180, 245, 249, 45, 183, 188, 205, 31, 25, 61, 
109, 206, 240, 224, 191, 20, 149, 138, 22, 238, 189, 237, 
190, 67, 63, 85, 233, 46, 124, 122, 236, 39, 15, 165, 
167, 69, 247, 171, 218, 172, 27, 23, 150, 124, 60, 175, 
124, 217, 207, 245, 197, 1, 178, 107, 212, 67, 102, 228, 
223, 116, 207, 128, 219, 207, 49, 68, 240, 106, 150, 247, 
189, 188, 238, 30, 221, 168, 31, 238, 52, 176, 14, 204, 
224, 80, 27, 108, 94, 41, 81, 217, 3, 33, 0, 252, 
16, 124, 244, 52, 70, 62, 9, 0, 168, 195, 223, 230, 
224, 143, 87, 145, 28, 185, 55, 36, 161, 204, 43, 247, 
42, 163, 111, 103, 136, 20, 201, 196, 252, 125, 216, 84, 
132, 228, 124, 12, 232, 11, 163, 247, 159, 52, 225, 100, 
61, 142, 52, 3, 246, 72, 67, 248, 105, 133, 238, 173, 
58, 212, 57, 144, 223, 238, 110, 127, 199, 33, 76, 31, 
130, 181, 14, 60, 186, 13, 207, 159, 28, 224, 128, 216, 
218, 175, 156, 14, 125, 253, 203, 181, 85, 15, 94, 95, 
114, 220, 249, 129, 79, 123, 241, 251, 97, 211, 51, 98, 
232, 92, 83, 83, 165, 3, 176, 252, 186, 71, 12, 154, 
221, 39, 59, 54, 62, 247, 219, 240, 153, 7, 127, 58, 
74, 97, 107, 94, 244, 200, 218, 43, 254, 99, 81, 220, 
229, 44, 139, 59, 63, 219, 49, 185, 215, 143, 227, 175, 
217, 243, 238, 247, 1, 21, 10, 128, 125, 207, 178, 226, 
7, 206, 91, 62, 248, 186, 210, 34, 91, 192, 3, 188, 
13, 119, 108, 95, 84, 189, 252, 63, 21, 71, 189, 211, 
223, 168, 248, 199, 191, 54, 14, 76, 254, 134, 144, 111, 
243, 207, 216, 254, 209, 206, 224, 23, 5, 128, 74, 69, 
223, 236, 185, 112, 240, 15, 28, 249, 126, 208, 229, 69, 
155, 27, 149, 166, 55, 202, 21, 191, 149, 60, 50, 243, 
231, 222, 169, 223, 18, 242, 13, 33, 223, 25, 179, 151, 
14, 26, 181, 102, 198, 204, 173, 15, 189, 124, 108, 99, 
69, 156, 12, 73, 146, 237, 240, 114, 164, 144, 91, 224, 
10, 193, 142, 120, 0, 179, 7, 187, 118, 22, 191, 142, 
151, 182, 43, 41, 84, 196, 81, 175, 1, 28, 61, 10, 
35, 170, 1, 246, 10, 204, 125, 4, 227, 122, 131, 39, 
32, 4, 26, 35, 122, 14, 194, 233, 51, 112, 211, 67, 
248, 114, 99, 27, 35, 131, 97, 232, 133, 201, 83, 48, 
102, 80, 219, 26, 14, 64, 133, 194, 84, 0, 0, 133, 
57, 236, 131, 3, 0, 208, 231, 194, 224, 220, 170, 69, 
93, 200, 4, 74, 106, 194, 251, 215, 35, 197, 136, 199, 
215, 135, 26, 232, 87, 242, 45, 214, 58, 0, 96, 222, 
124, 4, 51, 33, 134, 246, 43, 167, 3, 69, 138, 58, 
214, 60, 190, 245, 19, 215, 107, 139, 155, 242, 246, 216, 
251, 135, 197, 184, 85, 166, 78, 226, 1, 148, 110, 111, 
9, 242, 152, 72, 187, 255, 181, 225, 172, 217, 181, 94, 
221, 216, 210, 202, 251, 254, 152, 87, 166, 160, 74, 139, 
85, 213, 115, 206, 91, 62, 98, 86, 209, 154, 202, 136, 
142, 47, 249, 239, 150, 115, 159, 172, 15, 51, 225, 179, 
44, 213, 153, 129, 61, 21, 219, 27, 91, 247, 53, 111, 
221, 127, 249, 189, 199, 15, 154, 1, 208, 227, 107, 139, 
255, 50, 114, 205, 67, 63, 217, 3, 171, 133, 236, 88, 
247, 236, 218, 225, 151, 238, 255, 97, 159, 131, 66, 58, 
176, 96, 199, 148, 235, 142, 29, 139, 56, 221, 151, 154, 
155, 62, 190, 121, 117, 207, 137, 91, 255, 249, 117, 125, 
137, 75, 221, 100, 83, 117, 203, 129, 237, 117, 203, 191, 
46, 121, 229, 225, 77, 227, 243, 150, 93, 240, 66, 77, 
85, 215, 15, 74, 162, 246, 54, 117, 166, 32, 213, 189, 
165, 199, 195, 31, 195, 61, 252, 30, 207, 221, 138, 253, 
145, 232, 132, 19, 17, 101, 94, 34, 213, 55, 108, 50, 
36, 197, 174, 143, 49, 166, 39, 110, 250, 39, 54, 149, 
184, 2, 198, 118, 19, 142, 30, 192, 186, 229, 152, 251, 
10, 174, 25, 143, 62, 23, 224, 231, 170, 48, 197, 4, 
133, 71, 78, 178, 107, 211, 20, 89, 37, 23, 178, 225, 
234, 45, 104, 68, 117, 240, 145, 49, 142, 50, 220, 121, 
42, 110, 251, 20, 77, 38, 124, 252, 37, 66, 12, 161, 
57, 190, 211, 181, 113, 228, 23, 40, 126, 221, 41, 183, 
95, 57, 29, 39, 82, 205, 91, 14, 220, 242, 158, 235, 
214, 104, 206, 26, 249, 225, 95, 98, 222, 180, 33, 73, 
89, 60, 128, 138, 93, 214, 64, 119, 135, 86, 255, 184, 
227, 156, 251, 107, 90, 0, 64, 53, 110, 86, 191, 235, 
78, 230, 0, 160, 185, 242, 177, 151, 195, 137, 136, 187, 
132, 250, 245, 7, 46, 28, 186, 118, 246, 178, 214, 226, 
83, 6, 102, 92, 120, 93, 223, 135, 159, 25, 254, 238, 
103, 227, 87, 110, 156, 86, 90, 127, 145, 205, 124, 222, 
161, 117, 227, 254, 121, 67, 170, 167, 175, 242, 224, 235, 
187, 191, 45, 15, 253, 91, 19, 142, 0, 142, 166, 95, 
14, 187, 83, 198, 196, 230, 207, 238, 61, 120, 176, 205, 
31, 111, 122, 245, 220, 245, 175, 237, 246, 79, 132, 147, 
15, 190, 255, 251, 140, 127, 212, 121, 39, 16, 153, 22, 
237, 120, 116, 149, 35, 146, 199, 75, 170, 174, 250, 251, 
164, 85, 55, 126, 216, 228, 0, 0, 237, 25, 215, 13, 
120, 233, 189, 83, 190, 254, 225, 180, 21, 63, 77, 92, 
186, 112, 244, 243, 55, 165, 167, 1, 128, 117, 241, 99, 
191, 14, 159, 89, 188, 175, 139, 135, 201, 201, 246, 54, 
35, 242, 243, 82, 90, 183, 147, 198, 98, 238, 29, 174, 
109, 186, 25, 119, 125, 17, 241, 152, 12, 17, 199, 189, 
92, 225, 126, 126, 169, 61, 109, 144, 176, 242, 239, 24, 
117, 35, 118, 57, 0, 32, 239, 12, 60, 252, 18, 62, 
255, 26, 75, 87, 96, 229, 82, 204, 125, 30, 19, 210, 
0, 224, 248, 98, 156, 53, 28, 239, 237, 139, 208, 130, 
182, 240, 200, 118, 87, 242, 8, 61, 17, 117, 14, 242, 
156, 91, 20, 71, 130, 76, 144, 98, 61, 136, 107, 78, 
193, 219, 123, 1, 192, 112, 38, 190, 122, 42, 84, 124, 
221, 234, 190, 33, 244, 40, 202, 148, 10, 141, 114, 251, 
163, 128, 118, 12, 118, 243, 27, 227, 22, 2, 11, 129, 
133, 32, 171, 94, 222, 39, 117, 192, 223, 144, 14, 189, 
186, 18, 88, 136, 145, 135, 138, 29, 190, 223, 57, 74, 
142, 156, 159, 236, 52, 224, 251, 115, 223, 55, 89, 41, 
149, 235, 171, 110, 207, 95, 8, 44, 68, 202, 239, 203, 
27, 194, 23, 94, 60, 111, 75, 63, 184, 47, 1, 11, 
51, 167, 238, 254, 122, 143, 35, 248, 101, 200, 53, 191, 
237, 63, 59, 213, 121, 240, 55, 147, 62, 111, 9, 117, 
193, 178, 237, 203, 211, 23, 2, 11, 79, 251, 212, 117, 
88, 211, 154, 109, 185, 88, 8, 44, 76, 189, 168, 120, 
251, 193, 202, 199, 78, 249, 198, 245, 119, 135, 237, 221, 
209, 210, 230, 84, 203, 182, 189, 195, 156, 95, 105, 126, 
158, 189, 220, 188, 127, 193, 102, 231, 137, 234, 105, 71, 
43, 194, 221, 99, 177, 178, 252, 246, 254, 174, 203, 225, 
71, 238, 248, 174, 88, 148, 253, 44, 171, 253, 109, 255, 
212, 20, 215, 49, 89, 87, 29, 43, 19, 195, 148, 217, 
145, 88, 183, 211, 62, 160, 112, 255, 123, 255, 104, 155, 
111, 165, 26, 122, 109, 134, 251, 219, 116, 186, 168, 58, 
162, 50, 165, 50, 58, 214, 83, 166, 138, 126, 31, 226, 
65, 16, 233, 226, 219, 221, 71, 242, 244, 190, 239, 104, 
139, 223, 237, 18, 107, 233, 243, 83, 221, 199, 100, 209, 
175, 202, 148, 93, 160, 147, 111, 166, 184, 74, 120, 114, 
103, 100, 39, 88, 232, 45, 217, 174, 83, 254, 182, 33, 
192, 247, 230, 29, 244, 156, 20, 215, 1, 170, 83, 232, 
166, 166, 48, 229, 45, 189, 200, 125, 9, 122, 186, 44, 
220, 193, 254, 40, 182, 95, 49, 29, 228, 73, 213, 173, 
216, 243, 212, 239, 174, 237, 158, 247, 140, 188, 125, 80, 
155, 191, 67, 45, 166, 207, 254, 242, 83, 18, 249, 134, 
144, 111, 136, 230, 135, 252, 147, 126, 158, 126, 197, 246, 
39, 222, 56, 186, 98, 167, 205, 166, 192, 221, 228, 82, 
10, 4, 0, 168, 52, 87, 250, 184, 251, 14, 203, 135, 
55, 108, 95, 108, 2, 128, 130, 219, 78, 253, 252, 166, 
36, 13, 64, 82, 51, 31, 253, 71, 58, 0, 52, 86, 
127, 189, 39, 100, 170, 62, 149, 118, 253, 123, 195, 168, 
171, 74, 138, 92, 159, 249, 211, 158, 60, 99, 239, 178, 
33, 151, 14, 86, 5, 191, 93, 36, 227, 180, 129, 255, 
251, 95, 97, 58, 0, 193, 14, 116, 29, 0, 0, 25, 
238, 73, 68, 65, 84, 208, 237, 139, 77, 161, 6, 127, 
18, 46, 53, 141, 3, 112, 120, 67, 179, 13, 128, 221, 
252, 254, 253, 197, 21, 0, 244, 57, 111, 189, 211, 107, 
100, 255, 236, 103, 126, 56, 245, 90, 231, 43, 126, 215, 
193, 103, 86, 123, 185, 72, 214, 198, 151, 174, 221, 187, 
11, 0, 52, 179, 22, 140, 127, 106, 186, 97, 224, 197, 
131, 31, 31, 3, 0, 246, 223, 142, 109, 14, 57, 88, 
150, 154, 234, 231, 156, 187, 225, 157, 67, 0, 192, 141, 
24, 184, 106, 245, 176, 139, 122, 243, 126, 1, 66, 146, 
126, 218, 192, 197, 219, 198, 206, 234, 9, 0, 213, 243, 
54, 207, 154, 219, 220, 117, 205, 62, 217, 209, 38, 220, 
147, 97, 104, 243, 45, 151, 129, 23, 94, 118, 15, 22, 
171, 195, 223, 158, 133, 41, 130, 103, 71, 108, 66, 235, 
109, 202, 64, 86, 48, 239, 158, 98, 211, 28, 92, 244, 
142, 243, 47, 225, 145, 85, 120, 229, 34, 104, 253, 110, 
23, 159, 142, 71, 23, 227, 139, 89, 0, 128, 106, 204, 
154, 133, 35, 202, 111, 87, 170, 59, 165, 32, 210, 230, 
146, 6, 163, 10, 92, 155, 69, 126, 73, 231, 77, 155, 
113, 222, 120, 44, 117, 6, 18, 210, 241, 223, 239, 112, 
74, 178, 239, 49, 62, 240, 158, 155, 208, 140, 74, 229, 
131, 150, 21, 219, 175, 152, 14, 17, 41, 135, 229, 243, 
39, 221, 147, 96, 233, 115, 95, 250, 123, 154, 231, 241, 
178, 29, 171, 254, 215, 173, 191, 246, 73, 93, 57, 235, 
163, 38, 87, 155, 203, 238, 56, 190, 191, 126, 229, 252, 
226, 231, 238, 219, 124, 246, 136, 37, 218, 188, 53, 55, 
188, 86, 83, 21, 217, 112, 31, 109, 166, 206, 8, 160, 
198, 92, 209, 70, 164, 104, 201, 167, 219, 238, 93, 45, 
3, 64, 207, 190, 159, 255, 51, 59, 221, 117, 145, 36, 
119, 82, 222, 73, 0, 96, 219, 186, 39, 72, 184, 7, 
0, 228, 67, 255, 249, 125, 226, 221, 149, 238, 200, 133, 
112, 254, 219, 103, 173, 120, 42, 51, 146, 44, 135, 148, 
49, 121, 67, 1, 0, 77, 251, 205, 245, 33, 47, 33, 
57, 71, 5, 160, 98, 139, 169, 94, 166, 197, 115, 183, 
62, 186, 5, 0, 122, 223, 57, 228, 146, 30, 4, 0, 
159, 147, 251, 218, 251, 249, 41, 0, 32, 254, 244, 101, 
163, 187, 113, 74, 75, 63, 223, 241, 194, 30, 0, 72, 
189, 124, 212, 43, 231, 105, 56, 0, 42, 237, 244, 203, 
146, 1, 160, 165, 105, 125, 105, 240, 107, 146, 108, 223, 
255, 109, 253, 83, 206, 142, 251, 204, 130, 47, 151, 12, 
153, 148, 22, 180, 7, 67, 211, 167, 224, 195, 245, 167, 
222, 88, 8, 64, 254, 233, 193, 157, 223, 87, 117, 213, 
84, 115, 212, 225, 213, 220, 211, 33, 201, 175, 39, 186, 
240, 42, 220, 223, 207, 181, 125, 244, 255, 240, 238, 1, 
223, 3, 252, 145, 154, 96, 242, 124, 200, 8, 154, 112, 
88, 249, 61, 46, 120, 202, 53, 232, 236, 202, 47, 241, 
236, 164, 224, 21, 69, 131, 171, 63, 196, 194, 27, 1, 
192, 250, 19, 238, 255, 62, 104, 162, 99, 48, 82, 220, 
138, 19, 105, 115, 137, 71, 159, 30, 174, 205, 227, 135, 
218, 180, 115, 27, 214, 225, 236, 211, 241, 139, 91, 104, 
238, 249, 26, 51, 123, 32, 44, 42, 175, 212, 129, 50, 
229, 211, 66, 41, 182, 95, 49, 29, 33, 82, 246, 67, 
199, 222, 222, 226, 218, 238, 123, 247, 224, 139, 114, 221, 
245, 193, 82, 119, 207, 216, 181, 247, 126, 80, 83, 18, 
226, 117, 83, 89, 247, 201, 3, 107, 207, 125, 177, 41, 
146, 185, 114, 132, 12, 125, 6, 0, 209, 90, 84, 219, 
90, 145, 228, 202, 227, 247, 221, 239, 12, 40, 106, 110, 
250, 112, 200, 36, 175, 72, 134, 144, 151, 50, 80, 7, 
0, 181, 197, 246, 96, 125, 124, 141, 191, 238, 62, 239, 
214, 10, 119, 68, 91, 53, 227, 173, 51, 231, 223, 158, 
28, 48, 123, 196, 31, 162, 226, 4, 231, 13, 109, 178, 
54, 134, 122, 165, 114, 198, 92, 1, 0, 14, 84, 31, 
172, 107, 124, 235, 133, 90, 7, 0, 62, 227, 177, 59, 
82, 220, 145, 3, 146, 125, 193, 208, 127, 140, 6, 128, 
250, 13, 53, 37, 78, 9, 54, 215, 61, 63, 187, 198, 
14, 128, 75, 123, 246, 197, 156, 44, 215, 47, 199, 229, 
143, 79, 73, 1, 0, 219, 129, 234, 96, 213, 131, 150, 
47, 216, 54, 235, 115, 231, 35, 148, 252, 232, 162, 81, 
151, 231, 135, 185, 30, 85, 143, 30, 111, 125, 127, 210, 
0, 0, 77, 199, 159, 254, 76, 121, 127, 104, 108, 104, 
35, 82, 1, 199, 175, 232, 240, 183, 199, 90, 31, 225, 
167, 31, 64, 121, 184, 183, 155, 100, 106, 43, 82, 129, 
82, 112, 164, 114, 252, 117, 22, 156, 161, 240, 33, 143, 
226, 131, 203, 195, 37, 234, 168, 112, 201, 91, 152, 61, 
0, 0, 190, 121, 26, 69, 10, 111, 87, 170, 187, 146, 
71, 244, 204, 3, 0, 210, 122, 186, 54, 42, 246, 180, 
58, 155, 117, 107, 48, 245, 76, 108, 116, 127, 254, 211, 
71, 120, 105, 114, 68, 245, 91, 237, 229, 162, 150, 43, 
159, 187, 38, 10, 251, 21, 210, 1, 34, 37, 151, 46, 
57, 182, 223, 185, 169, 202, 124, 162, 181, 226, 1, 162, 
189, 188, 49, 200, 73, 109, 160, 91, 95, 216, 179, 164, 
58, 252, 11, 92, 149, 97, 200, 0, 0, 219, 193, 214, 
250, 41, 109, 126, 113, 231, 55, 77, 0, 96, 188, 244, 
228, 231, 166, 8, 109, 170, 163, 90, 59, 56, 7, 0, 
154, 42, 196, 128, 79, 51, 109, 168, 126, 228, 234, 67, 
158, 0, 246, 200, 217, 167, 207, 191, 61, 57, 242, 148, 
94, 234, 144, 92, 185, 34, 14, 41, 100, 28, 155, 24, 
243, 84, 0, 80, 95, 187, 224, 147, 3, 31, 149, 2, 
64, 202, 204, 65, 51, 11, 189, 140, 21, 12, 179, 158, 
204, 51, 0, 56, 92, 179, 215, 4, 128, 86, 253, 184, 
255, 211, 10, 0, 200, 188, 126, 232, 172, 62, 173, 191, 
155, 38, 63, 165, 7, 0, 136, 85, 141, 129, 69, 138, 
214, 87, 63, 122, 167, 243, 225, 35, 227, 95, 61, 245, 
31, 227, 3, 140, 151, 244, 71, 72, 22, 156, 23, 190, 
99, 65, 93, 77, 215, 228, 36, 80, 135, 151, 155, 160, 
129, 58, 144, 217, 121, 103, 99, 156, 123, 219, 178, 24, 
79, 175, 13, 51, 169, 174, 163, 9, 158, 217, 95, 82, 
10, 3, 180, 224, 64, 241, 211, 163, 248, 182, 9, 0, 
84, 227, 241, 245, 63, 34, 75, 194, 18, 144, 230, 186, 
93, 88, 23, 96, 122, 180, 80, 164, 244, 128, 243, 47, 
68, 94, 201, 141, 110, 255, 168, 122, 183, 171, 145, 91, 
189, 2, 147, 167, 96, 171, 251, 126, 157, 255, 14, 190, 
184, 1, 234, 192, 103, 251, 162, 241, 154, 170, 166, 218, 
111, 110, 156, 176, 68, 97, 191, 66, 98, 47, 82, 178, 
227, 247, 239, 93, 47, 171, 228, 139, 6, 92, 236, 253, 
210, 78, 201, 126, 253, 131, 222, 35, 218, 134, 22, 192, 
39, 93, 112, 115, 175, 25, 189, 218, 238, 180, 84, 188, 
243, 179, 61, 172, 74, 241, 70, 93, 142, 6, 128, 253, 
144, 59, 181, 135, 214, 84, 206, 249, 160, 5, 0, 184, 
180, 103, 254, 153, 155, 227, 115, 121, 130, 218, 217, 231, 
220, 92, 39, 5, 18, 41, 105, 231, 27, 219, 223, 45, 
115, 125, 48, 156, 51, 250, 219, 217, 233, 10, 210, 4, 
1, 169, 209, 221, 81, 162, 86, 169, 67, 221, 89, 146, 
148, 237, 124, 57, 55, 191, 249, 96, 89, 29, 0, 232, 
110, 121, 40, 211, 167, 5, 150, 113, 102, 159, 243, 141, 
128, 100, 94, 127, 68, 130, 100, 253, 254, 245, 202, 22, 
0, 72, 190, 239, 225, 12, 163, 183, 154, 101, 37, 245, 
224, 1, 160, 185, 65, 14, 36, 38, 180, 248, 179, 221, 
159, 56, 235, 205, 168, 33, 31, 221, 145, 28, 62, 247, 
79, 116, 236, 93, 114, 240, 218, 179, 119, 186, 102, 197, 
36, 232, 132, 129, 35, 129, 144, 69, 47, 79, 74, 64, 
192, 59, 202, 231, 225, 150, 137, 173, 31, 223, 187, 27, 
251, 66, 166, 35, 88, 170, 91, 85, 44, 163, 119, 128, 
76, 78, 177, 24, 143, 127, 226, 218, 126, 238, 35, 156, 
20, 65, 170, 100, 253, 94, 188, 124, 45, 238, 119, 79, 
34, 74, 20, 222, 46, 117, 6, 156, 61, 195, 230, 136, 
227, 65, 201, 158, 17, 139, 199, 81, 97, 67, 229, 18, 
156, 113, 54, 118, 186, 127, 253, 171, 62, 193, 130, 191, 
42, 24, 46, 163, 245, 10, 90, 249, 79, 224, 21, 150, 
40, 236, 87, 72, 236, 51, 206, 173, 230, 213, 123, 157, 
207, 1, 63, 253, 230, 140, 244, 54, 191, 24, 215, 247, 
218, 81, 127, 92, 57, 236, 208, 134, 234, 53, 91, 204, 
199, 106, 37, 33, 199, 56, 109, 102, 222, 184, 28, 142, 
54, 20, 222, 49, 116, 237, 59, 199, 61, 71, 210, 245, 
95, 54, 52, 93, 158, 147, 130, 144, 8, 66, 239, 116, 
160, 156, 30, 63, 96, 115, 64, 16, 64, 203, 151, 20, 
253, 104, 1, 128, 130, 59, 71, 220, 212, 223, 239, 169, 
230, 85, 61, 50, 8, 64, 173, 77, 146, 127, 125, 166, 
117, 213, 207, 188, 230, 254, 141, 116, 217, 111, 190, 223, 
179, 183, 194, 164, 9, 71, 141, 197, 21, 200, 52, 106, 
141, 33, 99, 88, 234, 84, 141, 30, 104, 253, 81, 71, 
245, 255, 235, 8, 191, 19, 82, 210, 174, 156, 192, 125, 
181, 172, 101, 211, 1, 209, 145, 93, 249, 225, 122, 0, 
224, 78, 235, 127, 221, 128, 182, 215, 165, 86, 231, 27, 
129, 122, 216, 205, 146, 236, 255, 214, 113, 88, 230, 191, 
231, 140, 51, 8, 87, 60, 223, 103, 80, 224, 58, 71, 
27, 75, 155, 182, 109, 170, 223, 176, 161, 110, 221, 111, 
181, 107, 215, 155, 189, 58, 182, 249, 25, 127, 205, 202, 
236, 154, 17, 63, 84, 244, 242, 164, 248, 32, 239, 83, 
30, 51, 231, 224, 241, 201, 168, 112, 126, 220, 129, 199, 
23, 99, 193, 37, 65, 95, 190, 38, 175, 72, 115, 122, 
159, 0, 143, 127, 209, 124, 108, 6, 0, 164, 92, 129, 
91, 7, 5, 46, 68, 108, 196, 238, 109, 248, 125, 3, 
214, 175, 195, 218, 181, 56, 232, 117, 187, 244, 51, 48, 
37, 116, 90, 131, 31, 170, 52, 164, 1, 141, 64, 115, 
29, 164, 200, 102, 53, 209, 123, 210, 187, 202, 177, 233, 
127, 120, 241, 6, 184, 250, 120, 4, 60, 184, 8, 47, 
204, 80, 86, 173, 181, 94, 35, 253, 77, 13, 8, 240, 
16, 133, 36, 10, 251, 21, 18, 115, 145, 146, 234, 76, 
219, 92, 15, 66, 210, 212, 33, 129, 138, 87, 9, 253, 
39, 246, 232, 63, 177, 205, 62, 146, 154, 54, 243, 12, 
213, 59, 255, 107, 125, 40, 45, 219, 107, 75, 108, 57, 
35, 66, 191, 200, 84, 154, 222, 233, 64, 57, 42, 246, 
88, 237, 72, 18, 36, 235, 178, 247, 107, 69, 0, 66, 
230, 211, 15, 166, 5, 154, 111, 145, 36, 101, 170, 0, 
135, 220, 34, 217, 41, 124, 60, 132, 154, 159, 138, 22, 
185, 227, 21, 253, 238, 27, 118, 85, 161, 98, 7, 194, 
84, 212, 232, 172, 43, 250, 158, 250, 212, 144, 63, 22, 
111, 80, 27, 90, 69, 138, 59, 251, 225, 194, 62, 254, 
130, 72, 132, 81, 231, 39, 99, 89, 227, 225, 13, 150, 
82, 71, 233, 239, 0, 192, 77, 189, 59, 47, 223, 167, 
100, 94, 149, 107, 4, 234, 225, 176, 82, 127, 223, 147, 
54, 52, 44, 217, 235, 42, 78, 62, 102, 218, 179, 79, 
160, 54, 177, 177, 214, 86, 113, 220, 90, 90, 108, 62, 
116, 192, 188, 103, 79, 211, 174, 157, 150, 192, 139, 100, 
167, 165, 221, 244, 226, 201, 175, 253, 89, 215, 21, 211, 
183, 2, 160, 162, 215, 124, 105, 28, 184, 32, 191, 135, 
113, 34, 94, 63, 31, 87, 47, 118, 125, 252, 230, 97, 
236, 56, 23, 39, 7, 25, 71, 102, 241, 18, 169, 172, 
30, 126, 53, 138, 98, 219, 18, 215, 166, 74, 198, 222, 
61, 72, 165, 48, 55, 162, 186, 2, 199, 74, 81, 116, 
8, 251, 247, 96, 215, 46, 20, 5, 105, 208, 141, 189, 
9, 239, 189, 134, 2, 133, 183, 75, 72, 67, 26, 112, 
4, 176, 212, 68, 90, 201, 85, 73, 208, 2, 86, 0, 
54, 220, 114, 131, 123, 111, 1, 62, 88, 134, 155, 134, 
40, 118, 124, 189, 69, 170, 185, 78, 177, 72, 69, 97, 
191, 66, 98, 254, 252, 217, 142, 53, 184, 26, 76, 106, 
93, 159, 224, 93, 72, 190, 72, 142, 226, 226, 182, 161, 
230, 35, 181, 123, 154, 48, 34, 244, 76, 28, 130, 208, 
59, 19, 0, 234, 14, 182, 88, 40, 116, 149, 213, 31, 
173, 167, 0, 114, 111, 26, 114, 69, 96, 137, 33, 134, 
44, 30, 112, 192, 46, 218, 125, 167, 226, 114, 108, 153, 
87, 235, 110, 41, 164, 220, 115, 179, 81, 249, 104, 73, 
233, 208, 207, 46, 145, 235, 117, 170, 62, 244, 233, 156, 
86, 104, 117, 199, 245, 57, 119, 156, 173, 9, 244, 92, 
144, 172, 225, 198, 84, 52, 150, 109, 170, 91, 182, 175, 
94, 6, 160, 206, 188, 121, 138, 223, 145, 188, 144, 149, 
4, 0, 146, 35, 128, 72, 217, 43, 76, 238, 4, 123, 
251, 130, 91, 214, 44, 136, 224, 50, 244, 189, 210, 166, 
206, 200, 185, 240, 146, 252, 203, 166, 25, 3, 6, 150, 
59, 11, 217, 225, 21, 96, 146, 32, 5, 107, 254, 243, 
184, 244, 101, 140, 88, 12, 87, 123, 235, 16, 254, 190, 
0, 75, 254, 28, 184, 182, 152, 189, 68, 170, 53, 133, 
221, 131, 29, 123, 220, 205, 253, 218, 5, 56, 45, 162, 
219, 133, 177, 83, 113, 222, 133, 184, 252, 50, 12, 85, 
56, 212, 217, 9, 111, 68, 38, 15, 72, 48, 215, 68, 
218, 51, 200, 235, 160, 67, 155, 36, 242, 180, 233, 88, 
52, 15, 167, 71, 101, 128, 206, 235, 62, 180, 52, 40, 
238, 157, 140, 194, 126, 133, 196, 94, 164, 202, 205, 174, 
33, 2, 73, 106, 67, 196, 170, 42, 87, 215, 124, 177, 
173, 237, 46, 218, 252, 199, 113, 233, 170, 172, 208, 69, 
112, 89, 133, 2, 224, 64, 133, 169, 194, 78, 229, 213, 
37, 235, 101, 0, 41, 247, 221, 23, 208, 141, 2, 64, 
146, 50, 84, 0, 224, 144, 124, 35, 94, 246, 150, 141, 
59, 221, 239, 237, 65, 249, 211, 194, 245, 127, 5, 192, 
222, 252, 243, 175, 78, 149, 19, 78, 25, 175, 13, 221, 
82, 228, 116, 173, 253, 190, 252, 169, 5, 227, 211, 2, 
31, 166, 233, 97, 236, 1, 52, 252, 113, 248, 69, 155, 
12, 64, 117, 122, 207, 51, 253, 199, 113, 112, 156, 51, 
206, 68, 3, 61, 33, 114, 139, 61, 68, 164, 128, 104, 
84, 89, 57, 218, 188, 124, 125, 223, 1, 73, 131, 78, 
50, 142, 60, 37, 237, 212, 177, 198, 62, 169, 113, 50, 
159, 131, 228, 29, 93, 18, 17, 98, 48, 163, 250, 36, 
188, 126, 19, 166, 126, 232, 250, 184, 252, 49, 108, 186, 
4, 227, 13, 1, 142, 52, 123, 57, 65, 133, 254, 119, 
93, 70, 125, 168, 219, 5, 99, 22, 114, 243, 208, 179, 
47, 6, 13, 194, 208, 145, 24, 119, 42, 70, 244, 105, 
247, 16, 101, 1, 133, 105, 64, 13, 204, 85, 145, 230, 
205, 115, 154, 54, 11, 79, 13, 185, 27, 43, 95, 65, 
94, 180, 118, 168, 12, 208, 192, 53, 2, 169, 165, 65, 
249, 108, 191, 202, 237, 87, 72, 172, 69, 74, 174, 43, 
118, 15, 134, 86, 243, 1, 251, 99, 2, 65, 75, 23, 
22, 173, 246, 109, 113, 88, 119, 149, 81, 140, 12, 125, 
34, 151, 214, 83, 0, 28, 40, 111, 170, 176, 218, 247, 
124, 84, 43, 2, 154, 233, 3, 103, 249, 71, 163, 92, 
16, 125, 58, 15, 0, 14, 217, 247, 145, 183, 219, 246, 
187, 71, 231, 233, 7, 167, 230, 71, 216, 49, 226, 133, 
88, 86, 53, 223, 57, 50, 66, 72, 187, 120, 104, 152, 
251, 74, 4, 222, 243, 23, 122, 79, 52, 166, 4, 185, 
81, 66, 102, 82, 14, 135, 61, 205, 150, 163, 0, 128, 
81, 87, 102, 6, 8, 15, 113, 92, 146, 83, 164, 2, 
85, 98, 194, 115, 78, 153, 215, 95, 114, 122, 197, 194, 
236, 112, 121, 125, 113, 133, 228, 253, 200, 55, 163, 57, 
196, 107, 154, 96, 210, 108, 76, 250, 20, 191, 56, 31, 
162, 163, 248, 251, 151, 248, 233, 102, 191, 167, 91, 70, 
141, 87, 30, 80, 161, 191, 39, 229, 89, 139, 69, 143, 
229, 21, 152, 222, 57, 183, 75, 64, 175, 52, 160, 6, 
77, 229, 17, 139, 148, 182, 85, 164, 116, 103, 99, 217, 
171, 200, 107, 71, 69, 230, 244, 208, 121, 68, 170, 81, 
185, 55, 164, 220, 126, 133, 196, 250, 165, 73, 235, 143, 
186, 197, 134, 35, 193, 162, 8, 190, 52, 55, 188, 249, 
82, 189, 171, 138, 169, 117, 238, 152, 139, 124, 236, 176, 
61, 220, 69, 19, 163, 243, 253, 209, 210, 188, 127, 95, 
213, 135, 191, 80, 64, 152, 249, 112, 142, 111, 167, 158, 
23, 42, 13, 1, 0, 209, 79, 164, 100, 217, 51, 85, 
173, 177, 135, 160, 252, 39, 151, 75, 190, 43, 113, 14, 
199, 231, 78, 45, 156, 16, 206, 237, 230, 4, 222, 243, 
39, 210, 123, 9, 65, 223, 129, 26, 77, 175, 214, 154, 
148, 116, 209, 36, 77, 0, 199, 146, 112, 122, 53, 224, 
27, 97, 115, 255, 33, 189, 58, 102, 83, 227, 116, 54, 
178, 247, 59, 189, 9, 33, 51, 207, 160, 234, 133, 87, 
238, 106, 253, 248, 203, 108, 172, 55, 249, 29, 36, 121, 
229, 1, 105, 144, 227, 239, 106, 121, 205, 32, 222, 121, 
168, 208, 51, 29, 0, 228, 6, 52, 69, 230, 198, 120, 
139, 84, 143, 83, 145, 213, 62, 87, 131, 215, 193, 115, 
209, 214, 38, 229, 34, 165, 220, 126, 133, 196, 92, 164, 
90, 26, 61, 131, 102, 229, 160, 81, 132, 182, 167, 148, 
45, 216, 243, 150, 107, 34, 50, 110, 234, 43, 195, 102, 
184, 91, 52, 245, 165, 17, 136, 148, 51, 43, 18, 45, 
43, 62, 58, 180, 218, 1, 228, 247, 188, 251, 52, 33, 
132, 54, 170, 156, 33, 29, 73, 242, 21, 41, 66, 212, 
110, 1, 8, 216, 110, 10, 131, 205, 252, 241, 91, 206, 
28, 48, 50, 241, 150, 236, 16, 42, 233, 182, 131, 115, 
63, 88, 36, 45, 43, 248, 104, 27, 149, 80, 224, 105, 
147, 100, 103, 78, 233, 21, 164, 135, 203, 105, 121, 160, 
203, 22, 178, 12, 57, 0, 0, 187, 37, 64, 135, 102, 
124, 211, 230, 119, 144, 194, 47, 228, 61, 250, 1, 156, 
237, 169, 187, 21, 120, 96, 174, 223, 138, 225, 34, 142, 
123, 18, 245, 82, 144, 234, 95, 183, 5, 12, 112, 221, 
46, 88, 59, 237, 118, 113, 200, 117, 78, 104, 217, 16, 
70, 136, 91, 207, 208, 181, 138, 84, 209, 179, 152, 118, 
7, 86, 151, 68, 191, 234, 58, 103, 104, 21, 41, 155, 
89, 185, 72, 41, 183, 95, 241, 31, 136, 49, 173, 53, 
197, 33, 71, 50, 48, 159, 54, 212, 60, 245, 112, 149, 
211, 137, 33, 99, 134, 188, 245, 103, 189, 231, 209, 169, 
47, 117, 132, 189, 102, 117, 154, 51, 21, 205, 190, 232, 
189, 6, 9, 232, 123, 99, 239, 145, 161, 94, 133, 196, 
213, 4, 245, 23, 80, 94, 149, 229, 118, 238, 235, 143, 
216, 34, 159, 250, 195, 117, 202, 170, 253, 111, 58, 103, 
165, 213, 101, 223, 117, 78, 32, 127, 199, 199, 14, 66, 
220, 119, 62, 105, 242, 160, 224, 135, 171, 84, 121, 238, 
174, 23, 213, 240, 236, 129, 65, 114, 95, 56, 158, 0, 
32, 1, 99, 239, 73, 73, 227, 122, 2, 128, 88, 220, 
120, 76, 233, 85, 121, 16, 109, 171, 158, 91, 63, 248, 
140, 67, 135, 58, 55, 247, 188, 237, 79, 84, 82, 23, 
228, 48, 55, 124, 15, 60, 127, 119, 235, 199, 77, 79, 
97, 169, 79, 55, 156, 136, 99, 94, 34, 21, 96, 118, 
115, 130, 1, 227, 92, 71, 110, 61, 230, 247, 109, 196, 
84, 174, 194, 5, 131, 241, 250, 161, 240, 71, 58, 113, 
101, 144, 215, 161, 54, 178, 27, 204, 233, 218, 164, 65, 
173, 125, 27, 83, 122, 99, 196, 229, 248, 114, 107, 52, 
13, 46, 94, 223, 42, 82, 98, 115, 52, 37, 40, 181, 
95, 33, 177, 22, 41, 46, 37, 223, 221, 118, 49, 219, 
76, 97, 175, 151, 138, 235, 159, 221, 250, 31, 87, 48, 
200, 248, 244, 220, 190, 222, 137, 60, 150, 218, 192, 121, 
225, 222, 240, 70, 173, 247, 12, 30, 215, 95, 157, 20, 
58, 105, 129, 119, 186, 89, 146, 95, 115, 79, 173, 25, 
146, 227, 218, 180, 239, 174, 41, 82, 148, 60, 107, 107, 
122, 251, 97, 215, 123, 62, 251, 207, 3, 103, 100, 71, 
208, 202, 229, 220, 146, 146, 151, 53, 165, 103, 240, 31, 
129, 87, 229, 184, 69, 170, 255, 212, 148, 32, 161, 43, 
194, 169, 8, 130, 37, 17, 106, 13, 211, 199, 170, 0, 
224, 96, 217, 146, 146, 104, 156, 3, 185, 177, 254, 181, 
139, 86, 78, 125, 162, 66, 58, 43, 187, 176, 83, 39, 
18, 245, 81, 221, 150, 8, 42, 192, 168, 251, 113, 161, 
167, 17, 215, 128, 123, 95, 134, 119, 28, 92, 182, 160, 
194, 243, 57, 53, 144, 39, 5, 244, 155, 238, 90, 43, 
116, 193, 18, 63, 71, 44, 18, 100, 108, 126, 13, 67, 
167, 98, 177, 132, 105, 133, 225, 15, 119, 217, 82, 224, 
50, 184, 42, 178, 231, 142, 104, 252, 38, 159, 3, 118, 
45, 192, 53, 99, 80, 56, 9, 111, 44, 67, 179, 18, 
183, 138, 215, 163, 181, 225, 27, 213, 26, 95, 74, 237, 
87, 72, 204, 69, 42, 111, 184, 123, 178, 111, 135, 173, 
44, 220, 130, 216, 13, 107, 246, 92, 251, 154, 235, 177, 
25, 242, 228, 41, 15, 182, 77, 104, 148, 44, 162, 53, 
220, 205, 86, 25, 189, 178, 60, 122, 247, 184, 168, 111, 
152, 11, 114, 137, 147, 68, 125, 229, 79, 165, 61, 117, 
130, 91, 223, 142, 150, 205, 11, 61, 77, 66, 27, 104, 
201, 103, 59, 230, 56, 151, 153, 224, 210, 159, 124, 52, 
35, 146, 104, 107, 171, 160, 244, 74, 235, 21, 74, 86, 
185, 228, 52, 231, 61, 225, 135, 140, 9, 218, 99, 232, 
140, 141, 19, 62, 160, 76, 9, 99, 103, 101, 234, 1, 
160, 233, 181, 87, 235, 148, 174, 80, 110, 63, 122, 252, 
142, 241, 63, 63, 240, 163, 29, 89, 61, 255, 117, 79, 
4, 217, 234, 177, 132, 111, 219, 123, 145, 26, 65, 180, 
136, 203, 193, 243, 15, 183, 126, 60, 252, 10, 222, 247, 
154, 165, 203, 81, 3, 143, 107, 165, 206, 66, 82, 160, 
103, 197, 56, 22, 103, 233, 1, 96, 215, 107, 88, 167, 
116, 32, 155, 29, 11, 239, 192, 184, 7, 80, 11, 204, 
250, 23, 134, 70, 124, 187, 140, 238, 247, 99, 113, 56, 
111, 209, 133, 87, 236, 236, 170, 71, 240, 39, 175, 238, 
165, 138, 95, 113, 223, 57, 200, 26, 137, 231, 22, 193, 
18, 217, 175, 77, 116, 48, 122, 244, 218, 129, 136, 38, 
38, 107, 139, 98, 251, 149, 17, 243, 230, 158, 113, 104, 
150, 123, 96, 122, 243, 239, 165, 161, 170, 186, 173, 232, 
200, 117, 127, 42, 58, 2, 0, 16, 78, 27, 246, 213, 
163, 174, 81, 126, 173, 53, 173, 197, 209, 18, 78, 43, 
248, 228, 214, 145, 71, 41, 19, 178, 251, 135, 203, 110, 
18, 157, 169, 7, 146, 127, 188, 140, 31, 113, 99, 190, 
123, 72, 84, 203, 187, 79, 149, 87, 68, 38, 83, 98, 
73, 233, 173, 247, 184, 38, 72, 204, 189, 121, 232, 172, 
222, 17, 142, 69, 118, 9, 74, 82, 129, 214, 16, 242, 
55, 48, 100, 56, 69, 202, 48, 190, 79, 208, 227, 8, 
79, 224, 150, 42, 127, 210, 39, 247, 159, 153, 1, 0, 
21, 239, 109, 123, 110, 99, 200, 89, 137, 219, 98, 222, 
113, 248, 178, 209, 27, 223, 221, 7, 64, 119, 219, 231, 
195, 103, 196, 114, 234, 231, 72, 80, 27, 218, 132, 217, 
250, 71, 150, 201, 61, 244, 46, 220, 232, 89, 179, 68, 
198, 99, 119, 160, 212, 237, 208, 219, 43, 220, 137, 233, 
64, 70, 144, 212, 1, 146, 142, 123, 102, 2, 0, 42, 
112, 203, 115, 17, 77, 255, 226, 132, 154, 241, 230, 101, 
184, 236, 93, 80, 160, 240, 54, 188, 54, 67, 65, 221, 
210, 103, 184, 178, 186, 130, 165, 137, 250, 226, 53, 213, 
92, 175, 43, 241, 205, 118, 52, 238, 199, 123, 143, 96, 
172, 251, 194, 155, 119, 226, 137, 139, 208, 99, 28, 222, 
219, 24, 65, 74, 1, 135, 76, 79, 37, 10, 145, 143, 
22, 28, 197, 246, 43, 35, 230, 34, 165, 238, 147, 61, 
217, 149, 129, 105, 95, 179, 188, 57, 216, 236, 13, 230, 
157, 197, 87, 78, 216, 246, 131, 51, 66, 144, 145, 255, 
249, 252, 254, 67, 221, 250, 194, 121, 108, 138, 32, 244, 
206, 233, 213, 41, 238, 202, 217, 127, 114, 82, 184, 1, 
75, 212, 110, 145, 1, 64, 162, 178, 95, 201, 134, 177, 
3, 95, 58, 207, 85, 150, 121, 209, 246, 219, 231, 181, 
132, 111, 173, 90, 26, 254, 57, 115, 219, 114, 167, 47, 
152, 81, 240, 222, 156, 140, 72, 199, 250, 185, 99, 82, 
201, 57, 170, 144, 93, 51, 68, 231, 244, 164, 52, 250, 
161, 193, 163, 241, 206, 158, 66, 78, 21, 100, 212, 152, 
49, 227, 145, 7, 157, 109, 98, 243, 75, 23, 111, 91, 
16, 209, 236, 201, 242, 177, 69, 59, 206, 24, 243, 199, 
15, 53, 0, 200, 152, 103, 199, 191, 58, 61, 228, 96, 
196, 14, 65, 149, 212, 26, 43, 65, 30, 6, 133, 25, 
35, 229, 130, 164, 225, 185, 127, 195, 227, 206, 182, 172, 
192, 237, 238, 121, 59, 43, 183, 183, 142, 46, 14, 177, 
78, 204, 25, 143, 192, 185, 56, 237, 193, 151, 112, 219, 
130, 136, 98, 52, 246, 99, 184, 255, 12, 220, 245, 3, 
0, 240, 99, 240, 221, 171, 200, 80, 114, 187, 136, 218, 
101, 204, 225, 8, 167, 33, 86, 33, 215, 125, 133, 165, 
245, 0, 96, 28, 136, 91, 95, 192, 239, 229, 56, 176, 
28, 247, 93, 224, 10, 171, 55, 109, 198, 95, 199, 227, 
148, 219, 176, 211, 191, 163, 211, 27, 30, 89, 94, 34, 
229, 95, 51, 194, 162, 216, 126, 101, 196, 254, 201, 211, 
39, 95, 118, 166, 171, 222, 29, 122, 251, 192, 10, 191, 
201, 12, 168, 181, 101, 229, 203, 191, 143, 28, 177, 253, 
59, 103, 242, 175, 33, 243, 213, 85, 163, 47, 239, 225, 
174, 95, 28, 167, 243, 60, 60, 146, 28, 254, 45, 160, 
18, 178, 93, 55, 88, 24, 50, 72, 8, 27, 177, 118, 
137, 20, 13, 212, 131, 199, 235, 174, 124, 111, 212, 197, 
174, 214, 163, 227, 219, 63, 175, 189, 235, 187, 80, 113, 
16, 218, 98, 250, 224, 234, 181, 79, 108, 118, 94, 160, 
230, 186, 143, 71, 92, 144, 21, 169, 175, 65, 220, 49, 
41, 125, 122, 136, 137, 244, 0, 16, 215, 248, 154, 236, 
164, 156, 224, 169, 91, 206, 152, 148, 74, 19, 108, 104, 
43, 119, 210, 223, 70, 222, 211, 27, 0, 80, 117, 236, 
138, 51, 182, 205, 15, 25, 156, 162, 22, 211, 151, 119, 
175, 25, 120, 81, 209, 118, 17, 0, 134, 221, 127, 218, 
210, 71, 83, 13, 93, 48, 200, 88, 200, 132, 199, 121, 
210, 157, 130, 96, 221, 6, 254, 228, 93, 138, 55, 102, 
180, 126, 92, 114, 11, 254, 111, 55, 64, 177, 101, 105, 
235, 206, 30, 253, 131, 230, 8, 170, 79, 194, 91, 247, 
184, 182, 191, 188, 2, 183, 5, 95, 158, 0, 0, 40, 
246, 125, 137, 211, 7, 226, 141, 237, 0, 64, 134, 97, 
233, 82, 140, 10, 148, 71, 26, 2, 78, 112, 25, 83, 
85, 28, 89, 220, 90, 133, 124, 119, 106, 74, 89, 173, 
119, 65, 24, 48, 29, 175, 45, 66, 205, 1, 220, 59, 
220, 181, 111, 251, 251, 24, 49, 24, 255, 222, 26, 188, 
219, 78, 213, 58, 79, 57, 228, 104, 178, 198, 21, 219, 
175, 176, 248, 216, 23, 41, 140, 187, 209, 173, 27, 53, 
71, 47, 28, 183, 113, 246, 71, 149, 191, 109, 105, 216, 
188, 190, 102, 241, 23, 135, 255, 113, 235, 186, 97, 89, 
75, 167, 63, 92, 118, 216, 121, 64, 82, 230, 139, 63, 
79, 184, 207, 123, 177, 117, 142, 79, 243, 60, 140, 52, 
128, 191, 227, 75, 107, 255, 151, 118, 120, 143, 176, 87, 
67, 109, 102, 25, 240, 138, 91, 251, 20, 86, 80, 48, 
119, 97, 63, 119, 115, 213, 252, 238, 159, 86, 158, 121, 
223, 209, 29, 13, 1, 140, 104, 62, 84, 118, 255, 164, 
85, 183, 45, 114, 138, 24, 55, 241, 159, 167, 191, 115, 
126, 192, 161, 45, 65, 224, 157, 61, 114, 208, 26, 185, 
48, 34, 229, 204, 62, 205, 74, 202, 14, 30, 180, 118, 
118, 89, 170, 245, 65, 243, 210, 136, 49, 99, 206, 130, 
193, 99, 156, 18, 94, 92, 114, 197, 176, 181, 143, 45, 
178, 180, 248, 95, 150, 195, 182, 233, 227, 29, 147, 251, 
172, 188, 230, 223, 13, 45, 0, 32, 156, 53, 103, 210, 
154, 151, 179, 59, 121, 89, 67, 55, 154, 94, 173, 97, 
157, 41, 55, 194, 63, 245, 50, 40, 42, 92, 63, 23, 
151, 120, 60, 47, 59, 30, 56, 29, 247, 206, 193, 83, 
63, 181, 30, 50, 162, 32, 208, 137, 78, 8, 78, 159, 
131, 167, 199, 184, 62, 205, 189, 2, 211, 30, 67, 113, 
160, 9, 223, 171, 55, 225, 222, 201, 24, 124, 13, 54, 
183, 0, 64, 202, 89, 248, 101, 13, 166, 41, 28, 96, 
12, 128, 168, 93, 83, 140, 214, 28, 142, 44, 90, 79, 
144, 227, 14, 77, 52, 28, 11, 160, 11, 134, 1, 120, 
229, 55, 172, 251, 47, 206, 117, 46, 210, 87, 134, 187, 
199, 225, 153, 77, 65, 210, 20, 120, 120, 250, 144, 193, 
69, 51, 231, 133, 98, 251, 149, 209, 17, 99, 179, 210, 
38, 13, 184, 190, 199, 241, 183, 156, 83, 26, 28, 41, 
159, 243, 151, 242, 57, 129, 14, 35, 131, 122, 207, 251, 
113, 228, 21, 62, 161, 22, 149, 208, 203, 147, 9, 169, 
81, 133, 175, 247, 188, 208, 35, 5, 56, 10, 240, 218, 
254, 225, 71, 235, 83, 155, 211, 147, 82, 7, 43, 153, 
164, 79, 29, 182, 226, 83, 235, 132, 89, 101, 149, 0, 
32, 174, 127, 99, 243, 200, 55, 118, 141, 191, 60, 255, 
252, 201, 105, 195, 251, 105, 211, 180, 180, 241, 104, 211, 
218, 31, 142, 126, 240, 191, 6, 247, 216, 119, 97, 218, 
243, 167, 205, 127, 48, 69, 153, 175, 193, 17, 173, 10, 
112, 64, 99, 8, 35, 82, 134, 12, 21, 0, 46, 85, 
155, 28, 84, 41, 136, 74, 67, 0, 146, 148, 202, 135, 
40, 42, 105, 204, 160, 101, 43, 233, 69, 231, 239, 91, 
215, 12, 152, 107, 95, 184, 104, 249, 135, 167, 21, 222, 
118, 67, 254, 180, 209, 134, 28, 61, 173, 41, 110, 220, 
184, 170, 98, 222, 167, 101, 155, 60, 99, 219, 210, 50, 
159, 248, 114, 236, 147, 51, 194, 140, 239, 233, 72, 72, 
26, 30, 127, 16, 27, 159, 67, 211, 104, 60, 59, 67, 
89, 229, 225, 123, 224, 63, 11, 177, 123, 42, 92, 179, 
117, 54, 226, 95, 79, 122, 125, 205, 225, 148, 144, 93, 
111, 36, 9, 143, 47, 131, 120, 17, 158, 93, 7, 0, 
107, 95, 64, 223, 15, 113, 221, 109, 184, 124, 26, 250, 
230, 192, 81, 131, 157, 27, 177, 104, 30, 230, 111, 106, 
61, 229, 172, 39, 240, 249, 147, 232, 17, 213, 237, 82, 
165, 33, 21, 168, 5, 234, 34, 94, 148, 56, 199, 61, 
67, 67, 83, 5, 196, 64, 245, 152, 79, 198, 132, 107, 
176, 248, 98, 204, 159, 141, 219, 95, 71, 157, 132, 87, 
95, 198, 131, 95, 33, 192, 128, 49, 14, 57, 158, 49, 
178, 66, 224, 121, 187, 98, 110, 191, 162, 226, 99, 92, 
30, 0, 32, 41, 237, 153, 207, 250, 173, 154, 90, 180, 
55, 248, 159, 29, 127, 239, 232, 79, 158, 203, 31, 232, 
223, 97, 195, 9, 253, 71, 106, 176, 193, 6, 64, 200, 
213, 167, 133, 181, 79, 37, 184, 28, 223, 12, 67, 110, 
4, 189, 41, 174, 220, 173, 20, 173, 49, 104, 201, 92, 
159, 235, 78, 217, 146, 174, 159, 121, 213, 193, 13, 174, 
8, 134, 117, 195, 252, 162, 13, 243, 3, 29, 218, 55, 
239, 249, 185, 163, 30, 56, 83, 163, 248, 54, 18, 206, 
160, 6, 28, 16, 116, 97, 210, 242, 157, 147, 186, 8, 
33, 67, 87, 106, 3, 7, 8, 133, 233, 161, 75, 34, 
25, 147, 7, 255, 180, 35, 249, 190, 153, 91, 223, 221, 
46, 1, 168, 90, 119, 244, 217, 117, 71, 159, 13, 112, 
164, 48, 238, 175, 195, 222, 126, 190, 215, 152, 200, 199, 
135, 119, 12, 4, 167, 205, 65, 77, 192, 55, 92, 4, 
164, 79, 193, 138, 47, 49, 241, 106, 28, 245, 255, 110, 
32, 70, 24, 253, 247, 182, 129, 207, 192, 51, 63, 97, 
200, 125, 248, 203, 187, 104, 1, 80, 133, 207, 158, 197, 
103, 129, 110, 87, 202, 56, 188, 250, 54, 110, 24, 19, 
253, 4, 0, 154, 124, 12, 80, 163, 200, 14, 91, 21, 
204, 145, 45, 74, 156, 63, 6, 42, 64, 4, 28, 214, 
80, 105, 156, 196, 128, 43, 94, 195, 133, 247, 226, 251, 
31, 97, 152, 130, 96, 205, 208, 236, 65, 192, 15, 0, 
192, 165, 6, 152, 166, 185, 35, 236, 87, 66, 199, 68, 
67, 73, 250, 148, 225, 27, 119, 140, 185, 119, 154, 222, 
239, 119, 19, 70, 94, 57, 120, 254, 206, 115, 126, 123, 
61, 144, 66, 1, 0, 63, 244, 170, 220, 76, 0, 32, 
19, 174, 72, 13, 223, 157, 79, 184, 156, 94, 2, 0, 
228, 36, 103, 135, 31, 112, 71, 180, 70, 14, 128, 113, 
68, 106, 200, 87, 30, 151, 127, 254, 176, 181, 37, 103, 
125, 242, 80, 238, 224, 32, 35, 149, 243, 198, 23, 60, 
241, 217, 228, 170, 253, 227, 255, 30, 133, 66, 1, 224, 
249, 84, 29, 0, 168, 195, 120, 82, 16, 50, 244, 233, 
64, 106, 161, 58, 248, 95, 33, 41, 121, 2, 160, 31, 
215, 51, 124, 37, 209, 246, 43, 120, 103, 243, 185, 155, 
231, 246, 155, 222, 47, 96, 255, 123, 242, 249, 119, 143, 
92, 81, 124, 238, 134, 119, 122, 119, 185, 66, 197, 130, 
158, 87, 97, 235, 207, 152, 158, 237, 187, 127, 244, 173, 
232, 23, 201, 232, 76, 45, 174, 122, 7, 101, 155, 113, 
247, 116, 4, 236, 54, 30, 114, 62, 254, 189, 2, 229, 
27, 112, 83, 59, 20, 10, 0, 140, 184, 250, 52, 0, 
64, 11, 154, 35, 235, 85, 78, 157, 128, 105, 58, 0, 
200, 140, 96, 132, 179, 174, 39, 174, 188, 13, 23, 12, 
8, 234, 141, 14, 190, 30, 103, 166, 1, 105, 184, 247, 
17, 37, 205, 106, 15, 202, 237, 87, 2, 161, 1, 199, 
165, 198, 10, 106, 57, 82, 191, 242, 151, 198, 131, 37, 
54, 155, 94, 93, 56, 40, 237, 172, 179, 82, 11, 195, 
182, 139, 168, 120, 96, 81, 201, 119, 213, 169, 215, 95, 
159, 145, 29, 94, 0, 104, 201, 219, 63, 247, 190, 163, 
65, 125, 222, 132, 170, 197, 185, 97, 59, 128, 172, 69, 
199, 223, 156, 103, 29, 113, 67, 239, 179, 243, 35, 211, 
103, 73, 60, 178, 173, 118, 195, 54, 83, 241, 113, 187, 
69, 226, 146, 211, 53, 5, 3, 83, 198, 157, 154, 218, 
63, 35, 210, 129, 137, 129, 177, 53, 62, 49, 100, 213, 
115, 135, 185, 243, 22, 159, 191, 248, 188, 80, 23, 73, 
155, 106, 31, 190, 96, 143, 230, 149, 9, 115, 198, 5, 
61, 204, 94, 82, 254, 193, 106, 205, 149, 215, 165, 43, 
9, 30, 201, 181, 123, 106, 87, 255, 110, 42, 62, 106, 
51, 131, 207, 44, 72, 26, 54, 38, 125, 252, 112, 109, 
4, 107, 196, 39, 28, 180, 25, 75, 223, 199, 251, 95, 
96, 205, 31, 168, 183, 99, 216, 117, 248, 250, 125, 12, 
84, 56, 21, 143, 163, 22, 191, 174, 198, 222, 98, 84, 
154, 97, 200, 196, 128, 97, 152, 48, 30, 121, 177, 107, 
216, 152, 55, 97, 234, 20, 52, 221, 138, 77, 175, 6, 
106, 145, 5, 162, 97, 59, 62, 90, 129, 73, 183, 96, 
76, 52, 186, 18, 99, 162, 176, 63, 98, 58, 88, 164, 
58, 3, 169, 236, 216, 204, 97, 155, 118, 220, 49, 101, 
207, 156, 148, 206, 205, 54, 108, 7, 212, 246, 197, 164, 
37, 215, 174, 85, 223, 248, 235, 140, 185, 19, 187, 114, 
210, 38, 6, 35, 222, 57, 1, 234, 7, 159, 95, 240, 
197, 38, 245, 26, 132, 25, 16, 19, 95, 16, 97, 204, 
5, 70, 172, 229, 70, 251, 206, 179, 201, 96, 48, 218, 
114, 2, 120, 82, 9, 10, 181, 182, 108, 219, 131, 33, 
163, 116, 1, 86, 44, 97, 48, 24, 30, 152, 72, 49, 
24, 140, 184, 38, 78, 102, 138, 101, 48, 24, 140, 192, 
48, 145, 98, 48, 24, 113, 13, 19, 41, 6, 131, 17, 
215, 48, 145, 98, 48, 24, 113, 13, 19, 41, 6, 131, 
17, 215, 48, 145, 98, 48, 24, 113, 13, 19, 41, 6, 
131, 17, 215, 48, 145, 98, 48, 24, 113, 13, 19, 41, 
6, 131, 17, 215, 48, 145, 98, 48, 24, 113, 13, 19, 
41, 6, 131, 17, 215, 48, 145, 98, 48, 24, 113, 13, 
19, 41, 6, 131, 17, 215, 48, 145, 98, 48, 24, 113, 
205, 255, 3, 255, 45, 163, 219, 193, 95, 30, 179, 0, 
0, 0, 0, 73, 69, 78, 68, 174, 66, 96, 130]

if __name__ == '__main__':
  w = MainWindow()
  w.show()

