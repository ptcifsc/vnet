#!/usr/bin/python

import sys,os
import urllib

Url = 'http://tele.sj.ifsc.edu.br/~msobral/netkit2/install'

def get_version():
  u = urllib.urlopen('%s/Version.txt' % Url)
  v = map(int, u.read().split('.'))
  u.close()
  return v

def gen_version(version):
  v = '%.3f' % version
  while v.endswith('0'):
      v = v[:-1]
  if v.endswith('.'): v = '%s0' % v
  return v

def get_diffs():
  p = os.popen('rsync -anv --exclude=.svn bin contrib msobral@tele.sj.ifsc.edu.br:public_html/netkit2/netkit2')
  l = p.readlines()
  p.close()
  l = map(lambda x: x.strip(), l)
  l = filter(lambda x: x.split('/')[0] in ['bin','contrib'], l)
  r = []
  for x in l:
    if not os.path.isdir(x): r.append(x)
  return r

v = get_version()
v[1] += 1
v = '%d.%d' % tuple(v)
open('Version.txt', 'w').write(v)

Arqs='pyinotify.py netkit_askpass.py uml_layer.py tap.py nkparser.py netkit.py feeder.py'
os.system('mkdir -p bin; cp -a  %s bin' % Arqs)
os.system('cp gnome-netkit.py bin/netkit2')
arqs = get_diffs()
if not arqs:
  print 'nada a atualizar ...'
  sys.exit(0)
f = open('nklist-%s' % v, 'w')
f.writelines(map(lambda x: x + '\n', arqs))
f.close()
os.system('rsync -av --exclude=.svn nklist-%s bin contrib msobral@tele.sj.ifsc.edu.br:public_html/netkit2/netkit2/' % v)
os.system('''ssh msobral@tele.sj.ifsc.edu.br "cd public_html/netkit2/netkit2;
tar czf ../install/netkit-update-%s.tgz -T nklist-%s --ignore-failed-read; chmod 644 ../install/netkit-update-%s.tgz"''' % (v,v,v))
os.system('scp -p Version.txt msobral@tele.sj.ifsc.edu.br:public_html/netkit2/install')

sys.exit(0)

