#!/usr/bin/python

import sys,os,getopt
import socket
import fcntl
import struct

def getpath(prog):
  lpath = os.environ['PATH'].split(':')
  lpath.append('%s/bin' % os.environ['NETKIT2_HOME'])
  for path in lpath:
    if not path: continue
    path = '%s/%s' % (path, prog)
    if os.path.exists(path): return path
  return None

class Tap:

  def __init__(self, name, ip=None, debug=False):
    self.name = name
    self.ip = ip
    self.debug = debug
    self.tunctl = getpath('tunctl')

  def __exec__(self, cmd):
    #print cmd
    p = os.popen(cmd)
    r = p.read()
    p.close()
    return r

  def __create__(self):
    if not self.tunctl: raise ValueError('tunctl not found !')
    try:
      user = os.environ['SUDO_USER']
    except:
      user = os.environ['USER']
    r = self.__exec__('%s -u %s -t %s' % (self.tunctl, user, self.name))
    r = self.__exec__('chmod 666 /dev/net/tun')

  def create(self):
    self.__create__()
    print '%s: ip=%s' % (self.name, self.ip)
    if self.ip:
      r = self.__exec__('ifconfig %s %s up' % (self.name, self.ip))
    else:
      r = self.__exec__('ifconfig %s up' % self.name)
    r = self.__exec__('sysctl -w net.ipv4.ip_forward=1')
    r = self.__exec__('iptables -t nat -L POSTROUTING -n')
    if r.find('MASQUERADE') < 0:
      r = self.__exec__('iptables -t nat -A POSTROUTING -j MASQUERADE')
    r = self.__exec__('iptables -L FORWARD -n -v')
    if r.find(self.name) < 0:
      r = self.__exec__('iptables -A FORWARD -i %s -j ACCEPT' % self.name)

  def __destroy__(self):
    r = self.__exec__('ifconfig %s down' % self.name)
    r = self.__exec__('%s -d %s' % (self.tunctl,self.name))
    
  def destroy(self):
    r = self.__exec__('iptables -t nat -L POSTROUTING -n')
    if r.find('MASQUERADE') > 0:
      r = self.__exec__('iptables -t nat -D POSTROUTING -j MASQUERADE')
    r = self.__exec__('iptables -L FORWARD -n -v')
    if r.find(self.name) > 0:
      r = self.__exec__('iptables -D FORWARD -i %s -j ACCEPT' % self.name)
    self.__destroy__()

class BridgeTap(Tap):

  SIOCGIFNETMASK=0x891b
  SIOCGIFADDR=0x8915

  def __init__(self, name, hostif, debug=False):
    Tap.__init__(self, name, None, debug)
    self.bridge = 'br_%s' % hostif
    self.hostif = hostif

  def __switchif__(self, srcif, destif):
    try:
      ip, mask = self.__get_ip__(srcif)
      if self.debug: print '%s: %s/%s' % (srcif,  ip,  mask)
    except IOError:
      if self.debug: print '%s has no IP address: skipping ...'
      return

    src_routes = self.__get_routes__(srcif)
    if self.debug: print 'src routes:',  src_routes

    self.__exec__('ifconfig %s 0.0.0.0' % srcif)
    if self.debug: print 'removed IP of %s' % srcif

    self.__exec__('ifconfig %s %s netmask %s' % (destif,  ip,  mask))
    if self.debug: print 'configure IP %s/%s of %s' % (ip,  mask,  destif)

    dest_routes = self.__get_routes__(destif)
    if self.debug: print 'dest routes:',  dest_routes

    for route in src_routes:
      if not route in dest_routes:
        if self.debug: print '..restoring route to %s/%s via %s' % route
        self.__exec__('route add -net %s netmask %s gw %s' % route)

  def create(self):
    self.__create__()
    r = self.__exec__('ifconfig %s up' % self.name)
    try:
      self.__get_ip__(self.bridge)
    except IOError, e:
      if e.errno == 19: # no such device
        r = self.__exec__('brctl addbr %s' % self.bridge)
        self.__switchif__(self.hostif, self.bridge)
        r = self.__exec__('brctl addif %s %s' % (self.bridge, self.hostif))
    r = self.__exec__('brctl addif %s %s' % (self.bridge,self.name))    
    
  def destroy(self):
    ok = True
    try:
      self.__get_ip__(self.bridge)
    except IOError, e:
      ok = e.errno != 19 # no such device
    if ok:
      self.__switchif__(self.bridge, self.hostif)
      self.__exec__('ifconfig %s down' % self.bridge)
      self.__exec__('brctl delbr %s' % self.bridge)
    self.__destroy__()

  def __get_routes__(self, iface):
    p = os.popen('route -n | grep %s$' % iface)
    r = []
    while 1:
        l = p.readline()
        if not l: return r
        l = l.split()
        r.append((l[0],  l[2],  l[1])) # prefixo, mascara, gateway
    
  def __ioctl__(self, s, ifname, op):
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        op, 
        struct.pack('256s', ifname[:15])
        )[20:24])
    
  def __get_ip__(self, ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return  (self.__ioctl__(s, ifname, self.SIOCGIFADDR), 
             self.__ioctl__(s, ifname, self.SIOCGIFNETMASK))

def ajuda():
  print 'Uso: %s [-b ifname][-d][-I IP] -i tap_name | -h' % sys.argv[0] 
  print '-b:   cria em modo bridge com a interface ifname do hospedeiro'
  print '-d:   destroi a tap (e a bridge automaticamente)'
  sys.exit(0)

if __name__ == '__main__':
  opcoes,extra = getopt.getopt(sys.argv[1:], 'b:di:I:vh')
  bridge = ''
  ifname = ''
  destroi = False
  ip = None
  debug = False
  for op,valor in opcoes:
    if op == '-i':
      ifname = valor
    elif op == '-I':
      ip = valor
    elif op == '-b':
      bridge = valor
    elif op == '-d':
      destroi = 1
    elif op == '-v':
      debug = True
    else:
      ajuda()

  if not ifname: ajuda()

  #ifname = 'tap_%s' % ifname

  if bridge: 
    tap = BridgeTap(ifname, bridge, debug)
  else:
    tap = Tap(ifname, ip, debug)

  if not destroi:
    tap.create()
  else:
    tap.destroy()

  sys.exit(0)
