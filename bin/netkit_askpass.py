#!/usr/bin/env python

import gtk
import pwd,os,sys

class Askpass(gtk.MessageDialog):
    
    def __init__(self):
        super(Askpass,  self).__init__(None,
		gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
		gtk.MESSAGE_QUESTION,
		gtk.BUTTONS_OK,
		None)
        self.set_markup('Enter your password:')
        self.entry = gtk.Entry()
        self.entry.set_visibility(False)
        #allow the user to press enter to do ok
        self.entry.connect("activate", self.responseToDialog, gtk.RESPONSE_OK)
        #create a horizontal box to pack the entry and a label
        hbox = gtk.HBox()
        hbox.pack_start(gtk.Label("Password:"), False, 5, 5)
        hbox.pack_end(self.entry)
        #some secondary text
        self.format_secondary_markup("<i>This will be used only to clean remaining virtual machines</i>")
        #add it and show it
        self.vbox.pack_end(hbox, True, True, 0)
        self.show_all()
        
    def responseToDialog(self,  entry, response):
        self.response(response)

    def run(self):
        super(Askpass,  self).run()
        text = self.entry.get_text()
        return text

def get_passfile():
  pw = pwd.getpwuid(os.getuid())
  arq = '/tmp/.pass-%s' % pw[0]
  return arq

MinTimePasswd = 10

def check_password(passwd=''):
  arq = get_passfile()
  if not passwd:
    try:
      status = os.stat(arq)
      if (time.time() - status[stat.ST_MTIME]) < MinTimePasswd: 
        os.remove(arq)
        return None
      f = open(arq,'r+')
      r = f.read()
      f.close()
      return r
    except:
      return None
  else:
    f = open(arq, 'w')
    f.write(passwd)
    f.close()
    os.chmod(arq,0600)
    return passwd

if __name__ == '__main__':
    #print 'getting password ...'
    r = check_password()
    if not r:
      msg = Askpass()
      msg.show_all()
      r = msg.run()
      open(get_passfile(),'w').write('%s\n' % r)
      if r:
        check_password(r)
    sys.stdout.write('%s\n' % r)

