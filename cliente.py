#!/usr/bin/python

import sys,getopt
from clientapi import NetkitClient

#######################################################################
def ajuda():
  print 'Uso: %s [-a IP][-p port][-l lab] | -h' % sys.argv[0]
  print 'IP: default = 172.18.20.200'
  print 'port: default = 8888'
  sys.exit(0)
          
if __name__ == '__main__':
    opcoes,extra = getopt.getopt(sys.argv[1:], 'p:l:a:h')
    ip = '172.18.20.200'
    port = 8888
    lab = 'lab'
    for op,valor in opcoes:
      if op == '-p':
        port = int(valor)
      elif op == '-l':
        lab = valor
      elif op == '-a':
        ip = valor
      else:
        ajuda()
    
    rede = NetkitClient(ip, port)
    rede.start(lab)
    term = rede.get_terminal()
    term.run()
    term.relay()
    term.stop()
    rede.stop()
    
    sys.exit(0)

