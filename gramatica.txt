reserved = {'ip': 'IPATTR', 'dhcp': 'DHCP', 'generic': 'GENERIC',
              'type': 'TYPE', 'gateway': 'GATEWAY', 'route': 'ROUTE',
              'default_gateway': 'DEFAULTGW', 'uplink':'UPLINK',
              'dev': 'DEV', 'interfaces':'INTERFACES', 'services':'SERVICES',
              'rate':'RATE', 'preserve':'PRESERVE', 'ber':'BER', 'delay':'DELAY', 
              'vlan_untagged':'UNTAGGED', 'vlans_tagged':'TAGGED', 'bridge':'BRIDGE',
              'range': 'RANGE', 'default-lease':'DEFLEASE', 'max-lease':'MAXLEASE',
              'switch':'SWITCH', 'router':'ROUTER', 'pppoe':'PPPOE', 'mpls':'MPLS',
              'pbx':'PBX', 'nat':'NAT', 'bridge_prioriy':'BRPRIO', 'stp':'STP',
              'on': 'ON', 'off':'OFF', 'stp_cost':'STPCOST', 'stp_prio':'STPPRIO',
              'max_age':'BRAGE', 'hello_time':'BRHELLO', 'forward_delay':'BRDELAY',
              'users':'USERS', 'interface':'INTERFACE', 'pppoe_ac':PPPOEAC,
              'pppoe_user':'PPPOEUSER', 'pppoe_password':'PPPOEPASSWD',
              'label':'LABEL', 'fec':'FEC', 'labelspace':'LABELSPACE',
              'ilm':'ILM', 'nhlfe':'NHLFE', '}

literals += '/'
tokens += ('LPAR','RPAR','FLOATNUMBER', 'NODE')

t_FLOATNUMBER = r'[0-9]*\.[0-9]+'
t_LPAR = r'\('
t_RPAR = r'\)'
t_NODE = r'[a-zA-Z_.0-9][-a-zA-Z_. 0-9]*'

statement : vmif
statement : vmdef
statement : vmroute
statement : vmdefgw
statement : vmservices
statement : vmpreserve
statement : vmdhcp
statement : vmnat
statement : vmstp
statement : vmpppoe
statement : vmfec
statement : vmilm
statement : vmnhlfe
statement : vmlabelspace


vmdef : ID LBRACK TYPE RBRACK EQUALS types
types : GENERIC
      | GATEWAY
      | ROUTER
      | SWITCH
      | PPPOE
      | MPLS
      | PBX

vmroute : ID LBRACK ROUTE RBRACK EQUALS IPMASK gwspec
gwspec : GATEWAY IP
gwspec : DEV PPPIF

vmdefgw : ID LBRACK DEFAULTGW RBRACK EQUALS IP

vmservices : ID LBRACK SERVICES RBRACK EQUALS servlist
servlist : ID VIRG servlist
servlist : ID

vmpreserve : ID LBRACK PRESERVE RBRACK EQUALS pathlist
pathlist : pathname COLON pathlist
pathlist : pathname
pathname : / NODE pathname
pathname : / NODE

vmdhcp : ID LBRACK DHCP RBRACK EQUALS dhcpexpr
dhcpexpr : ETHIF COLON dhcpattrs
dhcpattrs : dhcppair COLON dhcpattrs
dhcpattrs : dhcppair
dhcppair : RANGE EQUALS IPMASK VIRG IPMASK
dhcppair : DEFLEASE EQUALS NUMBER
dhcppair : MAXLEASE EQUALS NUMBER
dhcppair : DEFAULTGW EQUALS IP

vmnat : ID LBRACK NAT RBRACK EQUALS ETHIF

vmstp : ID LBRACK STP RBRACK EQUALS stpdecl
stpdecl : ON COLON stpattrs
stpdecl : ON
stpdecl : OFF
stpattrs : stppair COLON stpattrs
stpattrs : stppair
stppair : BRPRIO EQUALS NUMBER
stppair : BRHELLO EQUALS NUMBER
stppair : BRAGE EQUALS NUMBER
stppair : BRDELAY EQUALS NUMBER

vmif : ID LBRACK ETHIF RBRACK EQUALS ethexpr
vmif : ID LBRACK PPPIF RBRACK EQUALS pppexpr
vmif : ID LBRACK BONDIF RBRACK EQUALS bondexpr

pppexpr : ID COLON pppattrs
pppexpr : ID
pppattrs : ppppair COLON pppattrs
pppattrs : ppppair
ppppair : IPATTR EQUALS IPMASK
ppppair : RATE EQUALS NUMBER
ppppair : BER EQUALS FLOATNUMBER
ppppair : DELAY EQUALS NUMBER

ethexpr : UPLINK COLON uplinkattrs
ethexpr : ID COLON ethattrs
ethexpr : ID
ethattrs : ethpair COLON ethattrs
ethattrs : ethpair
ethpair : IPATTR EQUALS IPMASK
        | IPATTR EQUALS DHCP
ethpair : RATE EQUALS NUMBER
ethpair : UNTAGGED EQUALS NUMBER
ethpair : TAGGED EQUALS taglist
ethpair : TAGGED EQUALS iptaglist
ethpair : STPCOST EQUALS NUMBER
ethpair : STPPRIO EQUALS NUMBER
ethpair : PPPOEAC EQUALS ID
ethpair : PPPOEUSER EQUALS ID
ethpair : PPPOEPASSWORD EQUALS ID

taglist : NUMBER VIRG taglist
taglist : NUMBER
iptaglist : tagpair VIRG iptaglist
iptaglist : tagpair
tagpair : LPAR NUMBER VIRG IPMASK RPAR
uplinkattrs : ethattrs
uplinkattrs : uplinkpair COLON ethattrs
uplinkattrs : uplinkpair
uplinkpair : BRIDGE EQUALS ETHIF

bondexpr : ID COLON bondattrs
bondexpr : ID
bondattrs : bondpair COLON bondattrs
bondattrs : bondpair
bondattrs : IPATTR EQUALS IPMASK
          | IPATTR EQUALS DHCP
bondattrs : INTERFACES EQUALS iflist
iflist : ETHIF VIRG iflist
iflist : ETHIF

vmpppoe : ID LBRACK PPPOE RBRACK EQUALS pppoexpr
pppoexpr : ID COLON pppoeattrs
pppoeattrs : pppoepair COLON pppoeattrs
pppoeattrs : pppoepair
pppoepair : RANGE EQUALS IP VIRG IP
pppoepair : IPATTR EQUALS IP
pppoepair : INTERFACE EQUALS ETHIF
pppoepair : USERS EQUALS userlist
userlist : userpair VIRG userlist
userlist : userpair
userpair : LPAR ID VIRG ID RPAR

vmfec : ID LBRACK FEC RBRACK EQUALS IPMASK COLON NHLFE EQUALS NUMBER

vmnhlfe : ID LBRACK NHLFE RBRACK EQUALS NUMBER COLON nhlfeattrs
nhlfeattrs : nhlfepair COLON nhlfeattrs
nhlfeattrs : nhlfepair
nhlfepair : INTERFACE EQUALS ETHIF
nhlfepair : LABEL EQUALS NUMBER
nhlfepair : IPATTRS EQUALS IP

vmilm : ID LBRACK ILM RBRACK EQUALS NUMBER ilmattrs
ilmattrs : ilmpair COLON ilmpair
ilmattrs : ilmpair
ilmpair : LABELSPACE EQUALS NUMBER
ilmpair : NHLFE EQUALS NUMBER

vmlabelspace : ID LBRACK LABELSPACE RBRACK EQUALS NUMBER COLON INTERFACES EQUALS iflist

# Uma possível declaração alternativa de interface vlan
# untagged:
# vm[vlanN]=link:interface=ethN:ip=IP/MASK:...
# tagged:
# vm[vlanN]=link:interfaces=ethN[,ethM,...]:ip=IP/MASK:...

vlanexpr : ID COLON vlanattrs
vlanattrs : vlanpair COLON ethattrs
vlanattrs : vlanpair COLON vlanattrs
vlanattrs : vlanpair
vlanpair : INTERFACES EQUALS iflist
vlanpair : INTERFACE EQUALS ETHIF

# Uma possível declaração alternativa de interface pppoe
# vm[pppN]=link:interface=ethN:...
pppoeifexpr: ID COLON pppoifeattrs
pppoeifattrs: pppoeifpair COLON pppoeifattrs
pppoeifattrs: pppoeifpair
pppoeifpair: PPPOEAC EQUALS ID
pppoeifpair: PPPOEUSER EQUALS ID
pppoeifpair: PPPOEPASSWORD EQUALS ID
pppoeifpair: INTERFACE EQUALS ETHIF

