#!/usr/bin/python

import sys,os, getopt, pty,traceback,time, threading
import select,fcntl
from clientapi import NetkitClient
import gtk,vte,gobject

#######################################################################
def ajuda():
  print 'Uso: %s [-a IP][-p port][-l lab] | -h' % sys.argv[0]
  print 'IP: default = 172.18.20.200'
  print 'port: default = 8888'
  sys.exit(0)

# Representa um pty + terminal VTE a ele vinculado
# Usado para a multiplexacao entre o dataport e o terminal da vm ativa
class Pty:
    
    def __init__(self,  vt):
        # Os descritores do pty
        self.master,  self.slave = pty.openpty()
        # atributo state: se estah ativado ou nao (default=nao)
        self.state = False
        # fdr e fdw sao file objects para bufferizar o IO
        self.fdr = os.fdopen(self.slave, 'r')
        self.fdw = os.fdopen(self.slave, 'w')
        # vt eh o o terminal VTE
        self.vt = vt
        # vincula o pty ao vt
        self.vt.set_pty(self.master)
        
    def activate(self):        
        self.state = True
        
    def deactivate(self):
        self.state = False
        
    def is_active(self):
        return self.state

# Multiplexa os pty dos terminais com o dataport
# Faz o intercambio de dados entre o dataport e o pty do terminal da vm ativa
class PtyRelayer(threading.Thread):
    
    def __init__(self,  con):
        super(PtyRelayer,  self).__init__()
        self.sock = con
        #self.master,  self.slave = pty.openpty()
        self.ptys = {}
        self.stop = False
        
    # cada Pty deve ser cadastrado antes que o PtyRelayer seja executado
    def add_pty(self,  ptyObj):
        self.ptys[ptyObj.slave] = ptyObj
        
    def __get_block_flag__(self, fd):
        flags = fcntl.fcntl(fd, fcntl.F_GETFL)
        flags = flags ^ os.O_NONBLOCK
        return flags
    
    # Retorna o Pty ativo, ou None caso nao exista
    def get_active(self):
        for obj in self.ptys.values():
            if obj.state: return obj
        return None
        
    # a rotina principal do PtyRelayer, executada em uma thread separada
    def run(self):
        fds = map(lambda obj: obj.fdr,  self.ptys.values())
        fds.append(self.sock)
        sock_flags = self.__get_block_flag__(self.sock)
        fd_flags = self.__get_block_flag__(fds[0])
        print fds,  self.ptys
        while not self.stop:
            try:
             while not self.stop:
              print 'esperando ...'
              rl, wl, el = select.select(fds, [], [],  1.0)
              print 'stop: ',  self.stop
              if self.sock in rl:
                fcntl.fcntl(self.sock, fcntl.F_SETFL, os.O_NONBLOCK)
                x = self.sock.recv(128)
                if x:
                  print '...', len(x)
                  # precisa fazer exclusao mutua ao acessar os objetos Pty, pois
                  # podem ser modificados via interface grafica
                  gtk.gdk.threads_enter()
                  obj = self.get_active()
                  gtk.gdk.threads_leave()
                  try:
                    obj.fdw.write(x)
                    obj.fdw.flush()
                  except IOError:
                    print '???'
                    pass
                else:
                  print 'Fim ...'
                  return
                fcntl.fcntl(self.sock, fcntl.F_SETFL, sock_flags)
              # precisa fazer exclusao mutua ao acessar os objetos Pty, pois
              # podem ser modificados via interface grafica
              gtk.gdk.threads_enter()
              # Para cada fd pronto para leitura, le seus dados e envie pelo dataport Se
              # ele pertencer a um Pty ativo
              for fdr in rl:
                  try:
                      obj = self.ptys[fdr.fileno()]
                      if obj.state:
                        fcntl.fcntl(fdr, fcntl.F_SETFL, os.O_NONBLOCK)
                        x = fdr.read(128)
                        if x:
                          self.sock.send(x)
                        fcntl.fcntl(fdr, fcntl.F_SETFL, fd_flags)
                        break
                  except:
                    pass  
              gtk.gdk.threads_leave()
            except Exception, e:
              print e
              print traceback.format_exc()
              return
        print 'fim do relayer'
        gtk.main_quit()
    
class LabDialog(gtk.MessageDialog):    

    def __init__(self,  rede):
        super(LabDialog, self).__init__(None,
                gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                gtk.MESSAGE_INFO,
                gtk.BUTTONS_OK_CANCEL,
                None)
        self.rede = rede
        self.set_title('Choose network')
        self.set_markup('Choose network to be runned ...')
        self.entry = gtk.combo_box_entry_new_text()        
        self.entry.connect('changed',  self.select_text)
        self.entry.show()
        self.vbox.pack_start(self.entry)
        self.cancel = None
        hbox = self.get_action_area()
        for w in hbox.get_children():
            if w.get_label() == 'gtk-cancel':
                self.cancel = w
            elif w.get_label() == 'gtk-ok':
                self.ok = w
        self.ok.set_label('Ok')
        self.ok.set_sensitive(False)
        self.cancel.set_label('Cancel')
      
    def select_text(self,  combo):
        self.ok.set_sensitive(True)

    def run(self):
        networks = self.rede.get_networks()
        for net in networks .keys():
            self.entry.append_text(net)
        self.show_all()
        r = super(LabDialog,self).run()
        if r == gtk.RESPONSE_OK:
            return self.entry.get_active_text()
        return None
    

class Tela(gtk.Window):

  def __init__(self,  ip,  port,  lab):
    super(Tela, self).__init__()
    #gobject.timeout_add_seconds(2, self.check)
    self.tab = gtk.Table(3, 1, False)
    self.add(self.tab)
    self.next = gtk.Button('Next')
    self.next.connect('clicked',  self.change_active)
    self.next.show()
    b = gtk.Button('Finish')
    b.connect('clicked',  self.finish)
    b.show()
    self.tab.attach(self.next,  0,  1,  1,  2)
    self.tab.attach(b,  0,  1,  2,  3)
    self.rede = NetkitClient(ip, port)
    self.lab = lab
    self.active = 0
    
  def finish(self,  w):
      self.rede.stop()
      self.relayer.stop = True
      print 'acabou ???'
      #gtk.main_quit()
      
  def change_active(self, w):
    if len(self.rede.nodes) < 2: return
    sys.stdout.flush()
    term = self.terms[self.rede.active]
    term.vt.set_visible(False)
    term.deactivate()
    self.active = (self.active + 1) % len(self.rede.nodes)
    node = self.rede.nodes[self.active]
    self.rede.set_active(node)
    self.set_active(self.rede.active)
    print 'active:', node, self.rede.active
    
  def set_active(self,  node):
      term = self.terms[node]
      #fd = self.relayer.get_pty()
      #print 'fd=%d' % fd
      #term.set_pty(fd)
      term.activate()
      term.vt.set_visible(True)
      term.vt.grab_focus()
      term.vt.show()
      
  def show(self):
    if not self.lab:
          lab = LabDialog(self.rede)
          self.lab = lab.run()
          lab.destroy()
          if not self.lab:
              raise RuntimeError
    print self.lab
    self.rede.start(self.lab)
    con = self.rede.get_dataport()
    self.relayer = PtyRelayer(con)
    self.terms = {}
    for node in self.rede.nodes:
        term = vte.Terminal()
        ptyObj = Pty(term)
        self.relayer.add_pty(ptyObj)
        self.terms[node] = ptyObj
        self.tab.attach(ptyObj.vt,  0,  1,  0,  1)
    self.set_active(self.rede.active)
    self.active = self.rede.nodes.index(self.rede.active)
    self.relayer.start()
    self.tab.show()    
    super(Tela,  self).show()

  def __show_alert__(self, e):
    m = AlertDialog('Teste: %s' % repr(e))
    m.run()
    m.destroy()

if __name__ == '__main__':
    opcoes,extra = getopt.getopt(sys.argv[1:], 'p:l:a:h')
    ip = '172.18.20.200'
    port = 8888
    lab = ''
    for op,valor in opcoes:
      if op == '-p':
        port = int(valor)
      elif op == '-l':
        lab = valor
      elif op == '-a':
        ip = valor
      else:
        ajuda()

    #gobject.threads_init()
    gtk.gdk.threads_init()

    w = Tela(ip,  port,  lab)
    w.show()
    gtk.main()
    w.destroy()
    
    sys.exit(0)

