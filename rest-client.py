#!/usr/bin/env python

import pty,sys,time,os,signal,stat,pwd,shutil,threading,string,getopt
import urllib2,tarfile,cStringIO,socket,struct,select,types,traceback
import gtk,vte,gobject
import netkit
import nkparser
import uml_layer

class MconsoleNotify:

  Format = 'IIII'
  MaxSize = 1024

  def __init__(self, path):
    self.path = '%s.sock' % path
    try:
      os.remove(self.path)
    except:
      pass
    self.sd = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    self.sd.bind(self.path)

  def wait(self):
    try:
      m = self.sd.recv(self.MaxSize)
      tam = struct.calcsize(self.Format)
      r = struct.unpack(self.Format, m[:tam])
      data = struct.unpack('%ds' % r[3], m[tam:])
      return data[0].strip()
    except Exception, e:
      print e
      return None

  def close(self):
    self.sd.close()

  def fileno(self):
    return self.sd.fileno()

class Starter:

  Timeout = 0.5

  def start_vm(self, vm):
    s = MconsoleNotify(vm.name)
    gtk.gdk.threads_enter()
    vm.start()
    gtk.gdk.threads_leave()
    return s

  def __start__(self, vms, lim):
    if lim <= 0: lim = len(vms)
    ls = []
    n = 0
    while n < len(vms):
      if len(ls) < lim:
        s = self.start_vm(vms[n])
        if not s:
          raise Exception('Error: could not launch VMs ...')
        ls.append(s)
        n += 1
      else:
        #print '... waiting vm start'
        r = select.select(ls, [], [], self.Timeout)
        for s in r[0]:
          data = s.wait()
          #print data
          if data == 'COMPLETE':
            ls.remove(s)
            s.close()

  def start(self, vms, lim):
    task = threading.Thread(None, self.__start__, None, (vms, lim))
    task.start()

class Preferences:

  Prefs = {'path': './lab','vm':0,'mem':24, 'clean':False, 'compact': True,  'rw': False}

  def __init__(self, prefs={}):
    self.prefs = {}
    self.prefs.update(self.Prefs)
    self.prefs.update(prefs)
    self.prefs['path'] = os.path.abspath(self.prefs['path'])
    self.changed = []
    for k in self.prefs:
      try:
        if self.prefs[k] != prefs[k]:
          self.changed.append(k)
      except:
        self.changed.append(k)

  def __checkitem__(self, k):
    if not self.prefs.has_key(k):
      raise KeyError

  def __setitem__(self, k, v):
    k = k.lower()
    self.__checkitem__(k)
    if k == 'path':
      v = os.path.abspath(v)
      if os.path.split(v)[-1] != 'lab':
        v += '/lab'
    if v != self.prefs[k]:
      if not k in self.changed: self.changed.append(k)
    self.prefs[k] = v

  def __getitem__(self, k):
    self.__checkitem__(k)
    return self.prefs[k]

  def __hasitem__(self, k):
    return self.prefs.has_item(k)

  def keys(self):
    return self.prefs.keys()

  def values(self):
    return self.prefs.values()

  def __repr__(self):
    return repr(self.prefs)

  def clear(self):
    self.changed = []

  def has_changed(self, k=None):
    if not k:
      return len(self.changed) > 0
    return k in self.changed

class PrefsDialog(gtk.Dialog):

  Linhas = 5
  Colunas = 2

  def __init__(self, parent, prefs=None, title=''):
    super(PrefsDialog, self).__init__(title, parent,
            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
            (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
             gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
    self.set_title('Preferences')
    if not prefs:
      self.prefs = Preferences()
    else:
      self.prefs = prefs
    self.table = gtk.Table(5,2,False)
    self.table.show()
    self.vbox.pack_start(self.table)
    self.__set_wdir__()
    self.__set_vms__()
    self.__set_mem__()
    self.__set_preserve__()
    self.__set_compact__()
    hbox = self.get_action_area()
    self.cancel = None
    for w in hbox.get_children():
        if w.get_label() == 'gtk-cancel':
            self.cancel = w
        elif w.get_label() == 'gtk-ok':
            self.ok = w
    self.ok.grab_focus()
    #self.ok.set_label('Next')
    #self.ok.set_sensitive(False)
    #self.cancel.set_label('Cancel')
    #self.connect("delete_event", self.close_application)
    #self.w.connect('expose-event',self.expose)

  def __set_wdir__(self):
    text = gtk.Label('Working directory')
    self.table.attach(text, 0, 1, 0, 1)
    text.set_alignment(0,0.5)
    text.show()
    entry = gtk.Entry()
    entry.set_width_chars(48)
    entry.set_text(os.path.abspath(self.prefs['path']))
    entry.connect('focus-in-event', self.get_dir)
    entry.show()
    self.table.attach(entry, 1, 2, 0, 1)

  def __set_vms__(self):
    text = gtk.Label('Maximum VMs to start\nin parallel')
    self.table.attach(text, 0, 1, 3, 4)
    text.set_alignment(0,0.5)
    text.show()
    entry = gtk.HScale()
    entry.set_range(0,10)
    entry.set_increments(1,2)
    entry.set_digits(0)
    entry.set_value(self.prefs['vm'])
    entry.connect('value-changed', self.value_changed, 'vm')
    entry.show()
    self.table.attach(entry, 1, 2, 3, 4)

  def __set_mem__(self):
    text = gtk.Label('VM memory')
    self.table.attach(text, 0, 1, 4, 5)
    text.set_alignment(0,0.5)
    text.show()
    entry = gtk.HScale()
    entry.set_range(16,256)
    entry.set_increments(1,2)
    entry.set_digits(0)
    entry.set_value(self.prefs['mem'])
    entry.connect('value-changed', self.value_changed, 'mem')
    entry.show()
    self.table.attach(entry, 1, 2, 4, 5)

  def __set_preserve__(self):
    b = gtk.CheckButton('Clean working directory')
    b.set_active(self.prefs['clean'])
    b.show()
    self.table.attach(b, 0, 1, 1, 2)
    b.connect('toggled', self.value_changed, 'clean')

  def __set_compact__(self):
    b = gtk.CheckButton('Compact project file')
    b.set_active(self.prefs['compact'])
    b.show()
    self.table.attach(b, 0, 1, 2, 3)
    b.connect('toggled', self.value_changed, 'compact')

  def value_changed(self, w, par):
    if par in ['clean','compact']:
      self.prefs[par] = w.get_active()
      return
    n = int(w.get_value())
    self.prefs[par] = n

  def get_dir(self, entry, event):
    chooser = gtk.FileChooserDialog('Choose working directory', None, gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,                                (gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
    chooser.set_default_response(gtk.RESPONSE_OK)

    r = chooser.run()
    if r == gtk.RESPONSE_OK:
      self.prefs['path'] = chooser.get_filename()
      entry.set_text(self.prefs['path'])
    self.ok.grab_focus()
    chooser.destroy()

  def run(self):
    r = super(PrefsDialog, self).run()
    if r == gtk.RESPONSE_ACCEPT:
      return self.prefs
    else:
      return None

class Version:

  # v: tupla com (major, minor)
  def __init__(self, v):
    if type(v) == type(''):
      major,minor = map(int, v.split('.'))
      v = (major, minor)
    elif type(v) != types.TupleType:
      raise ValueError()
    self.v = v

  def __repr__(self):
    return '%d.%d' % self.v

  def next(self):
    return Version((self.v[0], self.v[1]+1))

  def minor(self):
    return self.v[1]

  def major(self):
    return self.v[0]

  def __cmp__(self, o):
    if self.major() < o.major(): return -1
    if self.major() > o.major(): return 1
    if self.minor() < o.minor(): return -1
    if self.minor() > o.minor(): return 1
    return 0
  
class Update:

  URL='http://tele.sj.ifsc.edu.br/~msobral/netkit2/install'
  Timeout = 5
  # preciso corrigir isso ... deve haver outra forma de traduzir tele.sj.ifsc.edu.br
  # sujeito a timeout !
  Check_Host = '200.135.37.75'
  Milestone = (2,  0)

  def __init__(self, url=URL):
    try:
      self.version = Version(open('%s/netkit-version.txt' % os.environ['NETKIT2_HOME']).read())
    except:
      self.version = Version((0,0))
    self.url = url
    self.cont = False

  def __check_milestone__(self):
     if self.Milestone == self.version or self.__isNewer__(self.Milestone,  self.version):
        self.update_fs()

  def __get_version__(self, s):
    major,minor = map(int, s.split('.'))
    return major, minor
    
  def __version2str__(self, v): 
    return '%d.%d' % v

  def get_version(self):
    return repr(self.version)
 
  def __isNewer__(self,  v1,  v2):
    if v1[0] < v2[0]: return True
    if v1[0] > v2[0]: return False
    if v1[1] < v2[1]: return True
    return False
    
  def isNewer(self, v):
     return self.__isNewer__(self.version,  v)

  def check_network(self):
    try:
      s = socket.create_connection((self.Check_Host, 80), self.Timeout)
      s.shutdown(socket.SHUT_RDWR)
      s.close()
      return True
    except socket.timeout:
      return False
          
  def check(self, update=True):
    #self.__check_milestone__()
    if not self.check_network(): return False
    try:
        u = urllib2.urlopen('%s/Version.txt' % self.url, None, self.Timeout)  
        version = Version(u.read())
        sys.stdout.flush()
        u.close()
        if self.version < version:
            if update:
                return self.do_update(version)
            else:
                return True
        return False
    except urllib2.HTTPError:
	return False
    except Exception, e:
        print e
        return False

  def do_update(self, version):
    inter = self.version
    self.cont = False
    while inter < version:
      inter = inter.next()
      ok = self.__do_update__(inter)
      if ok:
        if self.update_packages():
          self.cont = True
          break
    return ok

  def __do_update__(self, version):
    print 'updating to', version
    try:
        u = urllib2.urlopen('%s/netkit-update-%s.tgz' % (self.url, version), None, self.Timeout)
        print 'ok: baixou update %s' % version
        stream = cStringIO.StringIO(u.read())
        arq = tarfile.open(None, 'r:gz', stream)
        arq.extractall(os.environ['NETKIT2_HOME'])
        u.close()
        self.version = version
        open('%s/netkit-version.txt' % os.environ['NETKIT2_HOME'], 'w').write(repr(self.version))        
        return True
    except Exception,  e:
        print e
        return False

  def __wait__(self, s):
    esperar = True
    while esperar:
        r = select.select([s], [], [], self.Timeout)
        for sd in r[0]:
          data = sd.wait()
          if data == 'DONE':
            esperar = False
    
  def update_packages(self):
    path = '%s/contrib/packages' % os.environ['NETKIT2_HOME']
    pkgs = filter(lambda x: x[-3:] == 'tgz', os.listdir(path))
    if not pkgs: return False
    f = open('%s/update.startup' % path, 'w')
    os.fchmod(f.fileno(), 0700)
    f.write('#/bin/bash\n\n')
    for pkg in pkgs:
      f.write('echo Instalando %s\n' % pkg)
      f.write('tar xCzf / /hostlab/%s\n' % pkg)
    f.write('poweroff\n')
    f.close()
    msg = gtk.MessageDialog()
    msg.set_markup('<b>Updating Netkit filesystem. Please wait:  this can take a few minutes</b>')
    obj = netkit.NetkitObject('update',{'type':'generic','mem':32})
    vm = obj.start2({'rw':True, 'path':path})
    #vm = VM('update', 'vstart --append=mconsole=notify:%s.sock --con0=this -W --hostlab=%s --mem=32 %s' % ('update', path, 'update'))
    vm = VM(vm)
    vt = vte.Terminal()
    vt.connect ("child-exited", self.finish_update, msg, path, pkgs)
    vt.connect ("eof", self.finish_update, msg, path, pkgs)
    msg.vbox.pack_end(vt)
    vt.set_visible(True)
    msg.show()
    vm.setVte(vt)
    vm.start()
    return True
    
  def finish_update(self, w, msg, path, pkgs):
    for pkg in pkgs:
      #print '... removendo', pkg
      os.remove('%s/%s' % (path,pkg))
    msg.destroy()
    if self.cont: self.check(True)

class UpdateDialog(gtk.MessageDialog):
    
    def __init__(self):
        super(UpdateDialog, self).__init__(None,
                gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                gtk.MESSAGE_INFO,
                gtk.BUTTONS_OK_CANCEL,
                None)
        self.set_markup('<b>Checking for update ...</b>')
        try:
          self.anim = gtk.Spinner()
          self.anim.set_size_request(40,40)
          self.anim.show()
        except:
          self.anim = gtk.Label('Wait ...')
        self.vbox.pack_end(self.anim)
        hbox = self.get_action_area()
        self.cancel = None
        for w in hbox.get_children():
            if w.get_label() == 'gtk-cancel':
                self.cancel = w
                break
        
    def do_update(self):
        u = Update()
        if u.check():
            self.set_markup('<b>Updated to version %s\n\nRestart netkit2 to activate the new version</b>' % u.get_version())
        else:
            self.set_markup('<b>Already updated</b>')
        try:
          self.anim.stop()
        except:
          pass
        self.anim.hide()
        if self.cancel: self.cancel.hide()
        return False
    
    def run(self):
        self.show_all()
        try:
          self.anim.start()
        except:
          self.anim.show()
        gobject.idle_add(self.do_update)
        r = super(UpdateDialog,self).run()
        try:
          self.anim.stop()
        except:
          self.anim.hide()
        if r == gtk.RESPONSE_OK:
            return True
        return False
       
class AlertDialog(gtk.MessageDialog):

    def __init__(self,  text,  parent=None):
        super(AlertDialog, self).__init__(parent,
                gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                gtk.MESSAGE_ERROR,
                gtk.BUTTONS_CLOSE,
                None)
        self.set_markup('<b>%s</b>' % text)
        self.show()
        w, h = self.get_size()
        self.set_size_request(int(1.5*w), h)
        w, h = self.label.size_request()
        self.label.set_size_request(int(1.5*w), h)
        self.label.queue_resize()
        self.hide_all()

    def run(self):
        self.show_all()
        r = super(AlertDialog,self).run()
        self.hide_all()
        if r == gtk.RESPONSE_CLOSE:
            return True
        return False    
    
# Classe para representar uma VM do Netkit: embute um terminal
class VM:

  Colors = ['green', 'black', 'yellow', 'red']
  Starting = 0
  Started = 1
  Halting = 2
  Halted = 3
  Capturing = 4
  Timeout = 10
  
  def __init__(self, vm):
	self.name = vm.get_name()
        self.vm = vm
	self.vt = None
	self.status = self.Starting
	self.widget = None
	self.task = None

  # deefine o terminal a ser usado
  def setVte(self, v):
    self.vt = v

  # define um widget que ativa/desativa a VM (usualmente um botao)
  def setWidget(self, widget):
    self.widget = widget
    self.set_color()

  def __genEvent__(self, kind=gtk.gdk.BUTTON_PRESS):
    event = gtk.gdk.Event(kind)
    event.window = self.vt.get_window()  # the gtk.gdk.Window of the widget
    event.send_event = True  # this means you sent the event explicitly
    #event.in_ = False
    event.x = .0
    event.y = .0
    event.x_root = .0
    event.y_root = .0
    event.button = 1
    return event

  def grab_focus(self):
    self.vt.grab_focus()
    self.widget.set_active(True)
    #self.vt.emit('button-press-event', self.__genEvent__())
    #self.vt.emit('button-release-event', self.__genEvent__(gtk.gdk.BUTTON_RELEASE))

  def capture_text(self, t1, t2, t3, t4):
    return True

  def last_line(self):
    text = self.vt.get_text(self.capture_text)
    text = text.split('\n')
    try:
      i = -1
      while text[i] == '':
        i -= 1
      return text[i]
    except:
      return ''

  def start_timeout(self):
	self.task = threading.Timer(self.Timeout, self.halt_vm)
	self.task.start()

  def cancel_timeout(self):
	if self.task:
		self.task.cancel()
		self.task = None
	
  def halt_vm(self):
    print 'Matando', self.vm.get_name()
    self.vm.kill()
	  
  # executa a VM
  def start(self):
    if self.vm.started():
      self.vm.stop()   
      self.vm.wait()
    pts = self.vm.start()
    self.vt.set_pty(pts)
    self.status = self.Started
    self.set_color()

  # Inicia a captura em uma interface. Roda o tcpdump com opcao de salvar
  # em um arquivo de captura.
  def start_capture(self, obj, ifname):
    if self.status == self.Started:
      if ifname != 'any':
        try:
          iface = obj.getIface(ifname)
        except:
          return False
      else:
        iface = obj.getIfaces()[0]
        ifname  = iface.ifName()
      self.status = self.Capturing
      if ifname[:3] == 'ppp':
        try:
          dest = iface.getBroadcast()
          self.vt.feed_child('tcpdump -i %s -s 1024 -U -w /hostlab/shared/%s.cap &\nsleep 1\nping -c 1 -I %s -r %s  > /dev/null 2>&1\n' % (ifname, self.name, ifname, dest))
        except:
          return False
      else:
        cmd = 'tcpdump -i %s -s 1024 -U -w /hostlab/shared/%s.cap &\nsleep 1\narping -c 1 -i %s -S 10.1.1.1 10.1.1.2  > /dev/null 2>&1\n' % (ifname, self.name, ifname)
        if obj.Kind == 'router':
          cmd = 'start-shell\n%s' % cmd
        self.vt.feed_child(cmd)
      return True
    return False
 
  # Para a captura: termina o tcpdump
  def stop_capture(self):
    if self.status == self.Capturing:
      self.status = self.Started
      self.vt.feed_child('%c\nkillall -9 tcpdump\n' % chr(3))
      return True
    return False

  # muda a cor do rotulo do widget da VM
  def set_color(self):
    if not self.widget: return
    map = self.widget.get_colormap()
    cor = map.alloc_color(self.Colors[self.status])
    label =  self.widget.get_children()[0]
    label.modify_fg(gtk.STATE_NORMAL, cor)
    label.modify_fg(gtk.STATE_ACTIVE, cor)
    cor = map.alloc_color('white')
    self.widget.modify_bg(gtk.STATE_ACTIVE, cor)

  # para a VM
  def halt(self):
    #self.vt.feed_child('halt\n')
	self.vm.stop()
	self.status = self.Halting
	self.start_timeout()
	self.set_color()

  # define status de terminada ...
  def finish(self):
	self.cancel_timeout()
	self.status = self.Halted
        self.vm.wait()
	self.set_color()

# Classe para a janela que mostra o diagrama de rede
class NetWindow(gtk.Window):

  Img_Width = 800
  Img_Height = 600

  def __init__(self, parent, network):
    gtk.Window.__init__(self)
    self.main = parent
    self.network = network
  
  def show(self):
    grapher = netkit.NetkitGrapher(self.network)
    grapher.gen_image('/tmp/network-netkit.png')
    grapher.get_coords()
    self.connect('delete-event', self.destroyWinTip)
    self.connect('configure-event', self.movedWinGraph)
    self.wpos = self.get_position()
    self.scale_factor = 1

    scroll = gtk.ScrolledWindow()
    scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
    orig = gtk.Image()
    orig.set_from_file('/tmp/network-netkit.png')
    pb = orig.get_pixbuf()
    scroll.connect('button-press-event', self.save_img)
    scroll.set_tooltip_text('Double-click to save this diagram')
    img = gtk.Image()
    img.set_from_pixbuf(pb)
    width = min(pb.get_width(), self.Img_Width)
    height = min(pb.get_height()+50, self.Img_Height)
    scroll.add_with_viewport(img)
    eb = gtk.EventBox()
    eb.add_events(gtk.gdk.POINTER_MOTION_MASK)
    eb.add_events(gtk.gdk.BUTTON_PRESS)
    #eb.connect('motion-notify-event', self.mouseOver, pb)
    eb.connect('button_press_event', self.mouseOver, pb)
    vbox = gtk.VBox(False)
    #vbox.pack_start(scroll)
    vbox.pack_start(eb)
    eb.add(scroll)
    frame = gtk.Frame()
    frame.set_size_request(width, height)
    frame.add(vbox)
    b1 = gtk.Button('+')
    b2 = gtk.Button('-')
    b1.connect('clicked', self.__scale__, True, img, orig)
    b2.connect('clicked', self.__scale__, False, img, orig)
    hbox = gtk.HBox()
    hbox.pack_start(b1)
    hbox.pack_end(b2)
    vbox.pack_end(hbox,False)
    hbox.show()
    frame.show()
    vbox.show()
    scroll.show()
    img.show()
    self.scroll = scroll
    self.add(frame)
    self.show_all()
    self.set_title('%s Diagram' % self.main.conf)

  def __scale__(self, b, plus, img, orig):
    pb = orig.get_pixbuf()
    w = pb.get_width()
    h = pb.get_height()
    if plus:
      self.scale_factor *= 1.1
    else:
      self.scale_factor *= .9
    npb = pb.scale_simple(int(w*self.scale_factor), int(h*self.scale_factor), gtk.gdk.INTERP_HYPER)
    if npb:
      img.set_from_pixbuf(npb)

  def mouseOver(self, widget, e, pb):
    #print 'Moveu:', w, e.x, e.y 
    ch = self.scroll.get_child()
    vx = ch.get_hadjustment().get_value()
    vy = ch.get_vadjustment().get_value()
    #print 'Click: (%d,%d) ---> (%d,%d)' % (e.x, e.y, vx, vy)
    dpi = gtk.gdk.Screen().get_resolution()/72.
    ex = (vx+e.x)/dpi
    ey = (vy+e.y)/dpi
    alt = pb.get_height()/dpi
    for vm in self.network.objs:
      try:
        o = self.network.objs[vm]
        #print o.name, o.data
        x = o.data['x']
        y = alt - o.data['y']
        w = o.data['width']
        h = o.data['height']
        #print ex, ey, x, y, w, h, vm
        dx = ex - x
        dy = ey - y
        #if dx <= w  and dx >= 0 and dy <= h and dy >= 0:
        if abs(dx) <= 20*w and abs(dy) <= 20*h:
          try:
            self.win.destroy()
          except:
            pass
          text = '<b>%s</b>\n' % vm
          for iface in o.getIfaces():
            try:
              ip = iface.getIp()
              if ip:
                text += '%s: %s\n' % (iface.ifName(), iface.ip)
            except:
              pass
            try:
              ipv6 = iface.getIPv6()
              if ipv6:
                text += '%s: %s\n' % (iface.ifName(), ipv6.getCompact())
            except:
              pass
          self.win = gtk.Window(gtk.WINDOW_POPUP)
          self.win.set_type_hint(gtk.gdk.WINDOW_TYPE_HINT_TOOLTIP)
          label = gtk.Label()
          label.set_markup(text)
          self.win.add(label)
          label.show()            
          self.win.show()
          self.win.move(int(self.wpos[0]+e.x),int(self.wpos[1]+e.y))
          self.main.switchVM(vm)
          return True 
      except Exception, e:
        #pass        
        print 'Ops:', vm, e
        traceback.print_exc()
    try:
      self.win.destroy()
    except:
      pass
    return True

  def destroyWinTip(self, *args):
    try:
      self.win.destroy()
    except:
      pass
 
  def movedWinGraph(self, w, e, *args):
    try:
      wpos = self.win.get_position()
      dx = e.x - self.wpos[0]
      dy = e.y - self.wpos[1]
      self.win.move(wpos[0]+dx, wpos[1]+dy)
    except:
      pass
    self.wpos = (e.x, e.y) 

  def save_img(self, widget, event):
     if event.type.value_name == 'GDK_2BUTTON_PRESS':
        saver = gtk.FileChooserDialog('Save Network Diagram', self, gtk.FILE_CHOOSER_ACTION_SAVE,(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
        saver.set_default_response(gtk.RESPONSE_OK)
	saver.set_current_name('network.png')
        r = saver.run()
        if r == gtk.RESPONSE_OK:
          arq = saver.get_filename()
          if arq:
            try:
              f = open('/tmp/network-netkit.png')
              open(arq,'w').write(f.read())
              f.close()
            except Exception, e:
              print 'Ops ... failed to save network diagram:', e
        saver.destroy()

# Classe para representar a tela principal do Gnome-Netkit
class NetkitTerminal:

  MaxColunas = 10
  UpdateTimeout = 5
  ConfCheckInterval = 1

  def __init__(self):
    self.lv = []
    self.finished = 0
    self.started = 0
    self.must_restart = 0
    self.must_reload = 0
    self.must_quit = 0
    self.must_clear = 0
    self.must_reboot = 0
    self.must_restart = 0
    self.table = None
    self.conf = None
    self.network = None
    self.parser = None
    self.size = None
    self.clear_size = None
    self.graphed = False
    self.mtime = 0
    self.processes = {}
    self.version = Update()
    self.prefs = Preferences()
    self.check_idle = True
    self.base_path = os.getcwd()

  def addVM(self, vm):
    self.lv.append(VM(vm))

  def switchVM(self, vm):
    for x in self.lv:
      if x.name == vm:
        try:
          x.grab_focus()
        except Exception, e:
          print 'grab_focus:', e
          traceback.print_exc()
        self.__switchVM__(x.vt)

  # Muda a VM ativa (expoe seu terminal)
  def __switchVM__(self,  vm):
    if self.started:
      vm.set_visible(True)
      vm.grab_focus()
      #widget.set_active(True)
      for x in self.lv:
        if x.vt != vm: 
	  x.vt.set_visible(False)
  	  x.widget.set_active(False)
          #print 'clicou %s e desativou %s' % (widget.get_label(), x.name)

  def __switch__(self, widget, vm):
    if widget.get_active():
      self.__switchVM__(vm)

  # calcula as dimensoes do gtk.Table a ser usado ... meio gambiarra !
  def __aspect__(self):
    n = len(self.lv)
    if (n % self.MaxColunas) == 0:
      linhas = 1 + n / self.MaxColunas
    else:
      linhas = 2 + n / self.MaxColunas
    #colunas = 1 + max(self.MaxColunas, n)
    colunas = self.MaxColunas
    return linhas, colunas

  # testa se todas VMs terminaram
  def isFinished(self):
    return self.finished == len(self.lv)

  # para o experimento (termina todas as VM)
  def halt(self, widget):
    if self.started:
      self.started = 0
      for vm in self.lv:
        vm.halt()
    os.system('killall serialemu > /dev/null 2>&1')

  def reload(self, widget):
    if self.conf_has_changed():
      self.must_reload = 1
      self.load_and_run()
    
  # Inicia as VM de um experimento
  def __start__(self):
    starter = Starter()
    starter.start(self.lv, self.prefs['vm'])
    #for vm in self.lv:
    #  vm.start()
    self.must_restart = 0
    self.started = 1 
    self.finished = 0

  # Reinicia as VM .. halt e depois start
  def restart(self, widget):
    self.must_restart = 1
    self.load_and_run()

  # handler para contabilizar VM que terminaram. serah chamado para cada VM que encerrar.
  def finish_count(self,widget, vm):
    self.finished += 1
    vm.finish()
    if self.isFinished():
      username = pwd.getpwuid(os.getuid())[0]
      self.network.stop()
      #os.system('vclean --clean-all -u %s > /dev/null 2>&1' % username)
      #print 'FINISHED !!!'
      if self.must_quit:
        gtk.main_quit()
        return
      if self.must_restart: self.__start__()
      if self.must_restart: self.restart(widget)
      if self.must_reload: self.load_and_run()
      if self.must_clear:
        self.do_clear()
      if self.must_reboot:
          self.__reboot__()

  # Limpa a tela.
  def do_clear(self):
    self.must_clear = 0
    self.finished = 0
    self.conf = 'Lab.conf'
    self.network = None
    self.parser = None
    self.lv = []
    self.__do_clear__()

  def __do_clear__(self):
    self.window.resize(self.clear_size[0], self.clear_size[1])
    self.__render__()

  def conf_has_changed(self):
    try:
      mtime = os.stat(self.conf)[stat.ST_MTIME]
    except OSError:
      open(self.conf,'w')
      mtime = os.stat(self.conf)[stat.ST_MTIME]
    #print mtime,  self.mtime
    if mtime > self.mtime: return mtime
    return 0

  def __parse_alert__(self, line,  n,  value,  column):
    msg = '<tt><small>Erro na linha %d, coluna %d: %s' % (n, column, str(value))
    if type(value) != types.StringType: value = repr(value)
    msg += '\n\n%s<span foreground="red">%s</span>%s</small></tt>' % (line[:column], value, line[column+len(value):])
    print 'Erro na linha %d, coluna %d: %s' % (n, column, value)
    print '%sv' % (column*' ')
    print line
    self.__show_alert__(msg,  n,  column+1)

  def __semantic_alert__(self, line,  n,  value):
    text = 'Erro na linha %d: %s' % (n, value)
    print text
    print line
    self.__show_alert__(text,  n)

  def __show_alert__(self,  text,  n,  column=1):
    self.window.set_keep_above(True)
    msg = AlertDialog(text)
    msg.set_keep_above(True)
    msg.run()
    msg.destroy()
    self.window.set_keep_above(False)
    os.system('gedit -b +%d:%d %s' % (n,  column,  self.conf))
    return False
    
  # Gera um objeto NetworkParser a partir do arquivo de configuracao escolhido,
  # e o executa.
  def load_and_run(self):
    if self.isFinished():
      os.chdir(self.base_path)
      mtime = self.conf_has_changed()
      if self.conf:
         gobject.timeout_add_seconds(self.ConfCheckInterval, self.check_reload)
      if mtime:
        self.mtime = mtime
        self.network = None
        self.parser = nkparser.NetkitParser(self.conf)
        try:
          self.parser.parse()
        except nkparser.ParseError, e:
          self.__parse_alert__(e.line,  e.n,  e.value,  e.column)
          return False
        except nkparser.SemanticError, e:
          self.__semantic_alert__(e.line,  e.n,  e.value)
          return False 
          
        self.network = self.parser.get_network()
        
        prefs = self.network.get_prefs()
        if prefs:
          self.prefs = Preferences(prefs)
        
        self.window.set_title(self.conf)
        
        print '\nPreparing to run experiment described in "%s"' % self.conf
        print 'Working directory is "%s"' % self.prefs['path']
        try:
          open('/tmp/labstart.path','w').write(self.prefs['path'])
        except:
          pass
 
      self.lv = []
      self.started = 0
      self.finished = 0

      if self.prefs['clean']:
        # remover todos os arquivos contidos em prefs['path']
        print 'removendo',  self.prefs['path']
        shutil.rmtree(self.prefs['path'], True)
      try:
        os.mkdir(self.prefs['path'])
      except:
          pass

      if self.must_restart: 
        self.cleanup()
        self.network.start(self)
      self.must_reload = 0   
      self.must_restart = 0   

      self.__do_clear__()
      #self.__render__()
    else:
      self.halt(None)

  def run_rw(self,  w):
      if self.isFinished():
          self.conf = '/tmp/pkg-install-mode.conf'
          f = open(self.conf,  'w')
          f.write('global[rw]=True\npc[type]=generic\npc[mem]=64\npc[eth0]=uplink:ip=10.0.0.1/30\n')
          f.close()
          self.mtime = 0
          self.must_restart = 0      
          self.load_and_run()
          self.must_restart = 1
          self.load_and_run()
      
  def __get_project_name__(self):
      fname = os.path.split(self.conf)[-1]
      fname = '%s.netkit' % os.path.splitext(fname)[0]
      return fname
      
  def __do_export__(self,  fname,  path):
      msg = gtk.MessageDialog(None,  0,  gtk.MESSAGE_INFO,  gtk.BUTTONS_OK)
      if self.prefs['compact']:
        msg.set_markup('<b>Exporting project to file %s/%s</b>' % (path,  fname))
      else:
        msg.set_markup('<b>Exporting project to folder %s/%s</b>' % (path,  fname))          
      msg.run()
      conf_fname = os.path.split(self.conf)[1]
      conf = '%s/%s' % (self.prefs['path'],  conf_fname)
      if conf != self.conf:
          open(conf, 'w').write(open(self.conf).read())
      arqs = [conf_fname]
      for vm in self.network.objs:
          arqs.append(vm)
      if self.prefs['compact']:
          arqs = string.join(arqs,  '\n')
          p = os.popen('tar cCzfT "%s" "%s/%s" -' % (self.prefs['path'],  path,  fname),  'w')
          p.write(arqs)
          p.close()
      else:
          fname = '%s/%s' % (path,  fname)
          print fname
          os.system('rm -rf "%s"; mkdir -p "%s"' % (fname,  fname))
          arqs = string.join(map(lambda x: "%s" %x,  arqs))
          os.system('cd "%s"; cp -r %s "%s"' % (self.prefs['path'],  arqs,  fname))
      msg.destroy()
      
      
  def do_export(self,  w):
    chooser = gtk.FileChooserDialog('Export Netkit Project', None, gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                  (gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
    chooser.set_default_response(gtk.RESPONSE_OK)
 
    r = chooser.run()
    if r == gtk.RESPONSE_OK:
      self.__do_export__(self.__get_project_name__(),  chooser.get_filename())
    chooser.destroy()
    
  def __do_import__(self,  fname,  file):
      os.system('rm -rf "%s"/*; mkdir -p "%s"' % (self.prefs['path'],  self.prefs['path']))
      conf = os.path.split(fname)[-1]
      self.conf = '%s/%s.conf' % (self.prefs['path'],  os.path.splitext(conf)[0])
      if file:
          os.system('tar xCzf "%s" "%s"' % (self.prefs['path'],  fname))
      else:
          os.system('cp -r "%s"/* "%s/"' % (fname,  self.prefs['path']))
      self.mtime = 0
      self.reload(None)
              
  def do_import(self,  file=False):
    if file:
        action = gtk.FILE_CHOOSER_ACTION_OPEN
    else:
        action = gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER        
    chooser = gtk.FileChooserDialog('Import Netkit Project', None, action, 
                                  (gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
    chooser.set_default_response(gtk.RESPONSE_OK)

    if file:
        filtro = gtk.FileFilter()
        filtro.set_name('Netkit Project File')
        filtro.add_pattern('*.netkit')
        chooser.add_filter(filtro)
          
        filtro = gtk.FileFilter()
        filtro.set_name('All files')
        filtro.add_pattern('*')
        chooser.add_filter(filtro)

    r = chooser.run()
    if r == gtk.RESPONSE_OK:
      fname = chooser.get_filename()
      if fname:
        self.__do_import__(fname,  file)
        
    chooser.destroy()
      
  # Escolhe um arquivo de configuracao. Executa-o se for valido.
  def load(self, run):
    chooser = gtk.FileChooserDialog('Load Netkit Configuration', None, gtk.FILE_CHOOSER_ACTION_OPEN,
                                  (gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
    chooser.set_default_response(gtk.RESPONSE_OK)

    filtro = gtk.FileFilter()
    filtro.set_name('Netkit Configuration File')
    filtro.add_pattern('*.conf')
    chooser.add_filter(filtro)
      
    filtro = gtk.FileFilter()
    filtro.set_name('All files')
    filtro.add_pattern('*')
    chooser.add_filter(filtro)

    r = chooser.run()
    if r == gtk.RESPONSE_OK:
      self.conf = chooser.get_filename()
      if self.conf:
        #print 'escolheu', self.conf
        self.mtime = 0
        self.prefs['rw'] = False
        self.must_reload = 1
        self.must_restart = run
        self.load_and_run()

    chooser.destroy()

  # Chama um editor para visualizar/editar o arquivo de configuracao
  def edit(self, widget):
    if not self.network: self.load_and_run()
    if not os.fork():
      os.execlp('gedit', 'gedit', self.conf)

  # Termina o experimento e encerra o gnome-netkit
  def quit(self, widget):
    if self.isFinished():
      gtk.main_quit()
    else:
      self.halt(None)
      self.must_quit = 1

  # handler para parar a captura de pacotes em uma VM, se o wireshark correspondente
  # tiver terminado.
  def sigchld_handler(self, pid, cond, vm):
    vm.stop_capture()

  # retorna a VM que estiver ativa (com terminal selecionado)
  def get_active_vm(self):
    for x in self.lv:
      if x.vt.get_visible():
        return x

  # inicia a captura de pacotes em um VM, e abre um wireshark.
  def __capture__(self, args):
    vm, ifname = args
    if vm:
        obj = self.network.objs[vm.name]
        if ifname[:3] == 'ppp':
            path = '%s/%s/%s.log' % (self.network.get_path(),  vm.name,  ifname)
            try:
                os.stat(path)
                ok = True
            except:
                ok = False
                msg = gtk.MessageDialog(None,  0,  gtk.MESSAGE_INFO,  gtk.BUTTONS_OK,  None)
                msg.set_markup('<b>Cannot start wireshark: first enable <i>debug</i> on interface <i>%s</i> (add "debug=True" to its config line)</b>' % ifname)
                msg.run()
                msg.destroy()
        else:
           ok = vm.start_capture(obj, ifname)
           path = '%s/shared/%s.cap' % (self.network.get_path(), vm.name)
        if ok:
          pid = os.fork()          
          if pid == 0:
            time.sleep(1)
            os.execlp('wireshark','wireshark', path)
            sys.exit(0)
          if pid > 0: 
            gobject.child_watch_add(pid, self.sigchld_handler, vm)
          else: vm.stop_capture()
        else:
          if ifname[:3] != 'ppp': vm.stop_capture()

  # handler para gerar o menu do wireshark: esse menu precisa ter uma entrada para cada
  # interface da VM selecionada, mais a opcao "any" para coletar em todas as interfaces.
  def wireshark_menu(self, widget, data):
    menu = widget.get_submenu()
    if menu:
      for c in menu.get_children():
        menu.remove(c)
    vm = self.get_active_vm()
    if vm:
      obj = self.network.objs[vm.name]
      ifaces = obj.getIfaces()
      if not ifaces: return
      if not menu:
        menu = gtk.Menu()
      for iface in ifaces:
        ifname = iface.ifName()
        self.__add_menu_item__(ifname, self.__capture__, (vm, ifname), menu)
      self.__add_menu_item__('any', self.__capture__, (vm, 'any'), menu)
      widget.set_submenu(menu)
      menu.show()
      widget.show()

  # handler para ativar/desativar algumas opcoes do menu File
  def file_menu(self, widget, data):
    menu = widget.get_submenu()
    for c in menu.get_children():
      if c.get_label() == 'Graph':
        c.set_sensitive(self.network != None)
      if c.get_label() == 'Export':
        c.set_sensitive(self.network != None and self.finished != 0)    

  # handler para ativar/desativar algumas opcoes do menu Run
  def run_menu(self, widget, data):
    menu = widget.get_submenu()
    for c in menu.get_children():
      if c.get_label() == 'Stop':
        c.set_sensitive(self.started)
      elif c.get_label() in ['Start', 'Restart']:
        if self.started:
          c.set_label('Restart')
        else:
          c.set_label('Start')
      elif c.get_label() == 'Reload':
        c.set_sensitive(self.conf_has_changed() != 0)
  

  # handler para limpar a tela ... faz halt e depois limpa a tela
  def clear(self, widget):
    if not self.isFinished():
      self.halt(None)
      self.must_clear = 1
    else:
      self.do_clear()
      
  # handler para mostrar a tela About
  def help(self, args):
    if os.fork():
      return
    os.execlp('firefox','firefox','http://wiki.sj.ifsc.edu.br/wiki/index.php/Netkit2')
    print 'Erro !!!'
    sys.exit(0)

  def about(self, args):
    about = gtk.AboutDialog()
    about.set_program_name("Netkit2")
    about.set_version(self.version.get_version())
    about.set_copyright("(c) Marcelo Maia Sobral - msobral@ifsc.edu.br")
    about.set_comments("Netkit2 is a tool to run virtual networks for teaching purposes")
    about.set_website("http://wiki.sj.ifsc.edu.br/wiki/index.php/Netkit2")
    about.set_logo(gtk.gdk.pixbuf_new_from_file("%s/contrib/netkit.png" % os.environ['NETKIT2_HOME']))
    about.run()
    about.destroy()

  def update(self, args):
      u = UpdateDialog()
      ok = u.run()
      u.destroy()
      if ok and self.isFinished(): self.reboot(None)
          
      
  def reboot(self, args):
    self.halt(None)
    if self.isFinished():
      self.__reboot__()
    else:
      self.must_reboot = 1
      
  def __reboot__(self):
     pid = os.fork()
     if pid == 0:
         os.execlp(sys.argv[0], sys.argv[0])
         sys.exit(1)
     gtk.main_quit()
      
  def get_prefs(self, args):
    d = PrefsDialog(self.window, self.prefs)
    d.set_resizable(False)
    r = d.run()
    if r:
      self.prefs = r
      if self.parser and self.prefs.has_changed():
        self.parser.update_prefs(self.prefs)
      self.prefs.clear()
    d.destroy()
    
  def cleanup(self):
      print '.. cleaning up any hung VMs'
      #os.system('vclean --clean-all > /dev/null 2>&1')
      username = pwd.getpwuid(os.getuid())[0]
      os.system('killall -u %s netkit-kernel > /dev/null 2>&1' % username)
      os.system('killall -u %s uml_switch > /dev/null 2>&1'% username)
      os.system('killall -u %s serialemu > /dev/null 2>&1'% username)
      #path = '%s/bin' % os.environ['NETKIT2_HOME']
      #os.system('sudo -E -A %s/vclean --clean-all -u %s > /dev/null' % (path,  username))
      print '...done ???'
  ######################################################################
  # Metodos para exposicao da tela

  def __add_menu_item__(self, label, callback, arg=None, menu=None):
    if  not menu: menu = gtk.Menu()
    m = gtk.MenuItem(label)
    m.show()
    m.connect_object("activate", callback, arg)
    menu.append(m)
    return menu
 
  def __set_menu__(self):
    self.menu_bar = gtk.MenuBar()
    self.menu_bar.show()
    if self.network != None:
      self.table.attach(self.menu_bar, 0, self.MaxColunas+1, 0, 1)
    else:
      self.table.attach(self.menu_bar, 0, self.MaxColunas, 0, 1)

    menu = self.__add_menu_item__('Load Only', self.load, False)
    self.__add_menu_item__('Load and Run', self.load, True, menu)
    self.__add_menu_item__('Edit', self.edit, None, menu)
    self.__add_menu_item__('Graph', self.__graph__, None, menu)
    self.__add_menu_item__('Clear', self.clear, None, menu)
    menu.append(gtk.SeparatorMenuItem())
    self.__add_menu_item__('Export',  self.do_export,  None,  menu)
    self.__add_menu_item__('Import from folder',  self.do_import,  False,  menu)
    self.__add_menu_item__('Import from file',  self.do_import,  True,  menu)
    menu.append(gtk.SeparatorMenuItem())
    self.__add_menu_item__('Quit', self.quit, None, menu)
    m = gtk.MenuItem('File')
    m.connect("select", self.file_menu, None)
    m.set_submenu(menu)
    m.show()
    self.menu_bar.append(m)

    menu = self.__add_menu_item__('Start', self.restart, None)
    self.__add_menu_item__('Stop', self.halt, None, menu)
    self.__add_menu_item__('Reload', self.reload, None, menu)
    m = gtk.MenuItem('Network')
    m.connect("select", self.run_menu, None)
    m.set_submenu(menu)
    m.set_sensitive(self.network != None)
    m.show()
    menu.set_sensitive(self.network != None)
    self.menu_bar.append(m)

    m = gtk.MenuItem('Wireshark')
    m.connect("select", self.wireshark_menu, None)
    m.set_sensitive(self.started)
    m.show()
    menu = gtk.Menu()
    m.set_submenu(menu)
    self.menu_bar.append(m)

    menu = self.__add_menu_item__('Help', self.help, None)
    self.__add_menu_item__('About', self.about, None, menu)
    menu.append(gtk.SeparatorMenuItem())
    self.__add_menu_item__('Update', self.update, None, menu)
    self.__add_menu_item__('Restart netkit2', self.reboot, None, menu)
    self.__add_menu_item__('Package Install Mode', self.run_rw, None, menu)
    menu.append(gtk.SeparatorMenuItem())
    self.__add_menu_item__('Preferences', self.get_prefs, None, menu)
    m = gtk.MenuItem('General')
    m.set_submenu(menu)
    m.show()
    self.menu_bar.append(m)

  def debug_vt(self, vt, w, h):
    print 'resize:', vt, w, h

  def __render__(self):
      linhas,colunas = self.__aspect__()
      #print 'aspecto:', linhas, colunas
      self.graphed = False
      if self.table:
        self.table.destroy()
        #self.window.remove(self.table)
      if self.size and self.network and self.started:
        #self.window.set_size_request(self.size[0], self.size[1])
        self.window.resize(self.size[0], self.size[1])
      if self.network != None:
        self.table = gtk.Table(linhas,colunas+1,False)
      else:
        self.table = gtk.Table(linhas,colunas,False)

      #self.__set_menu__()

      j = 0
      while j < len(self.lv):
        v = vte.Terminal ()
        self.lv[j].setVte(v)
        v.connect ("child-exited", self.finish_count, self.lv[j])
	v.connect("eof", self.finish_count, self.lv[j])
        if j == 0: v.grab_focus()
        #self.lv[j].start()
        b = gtk.ToggleButton(self.lv[j].name)
        self.lv[j].setWidget(b)
        b.connect("clicked", self.__switch__, v)
        linha = 2 + j / self.MaxColunas
        col = j % self.MaxColunas
        self.table.attach(b,col,col+1,linha,linha + 1)
        self.table.attach(v, 0,colunas,1,2, gtk.FILL | gtk.SHRINK)
        v.set_visible(False)
        j += 1
      if len(self.lv) > 0: 
        self.lv[0].vt.set_visible(True)
        self.lv[0].widget.set_active(True)
        #print filter(lambda x: x.find('pare') >= 0, dir(self.lv[0].vt))
        #print self.lv[0].vt.get_parent_window(), self.lv[0].vt.get_parent()
        self.__start__()
        self.started = 1
      else:
        frame = gtk.Frame()
        #frame.set_size_request(640,480)
        frame.show()
        img = gtk.Image()
        img.set_from_file("%s/contrib/netkit-demo.png" % os.environ['NETKIT2_HOME'])
        img.show()
        frame.add(img)
        self.table.attach(frame, 0,self.MaxColunas, 1, 2)
      self.__set_menu__()
      self.window.add(self.table)
      self.window.connect('configure-event', self.get_size)
      #self.window.reset_shapes()
      self.window.show_all()
      if self.up_handler and self.check_idle:
        self.check_idle = False
        task = threading.Thread(None, self.check_update)
        task.start()

  def __graph__(self, widget, linhas=2):
    if not self.network: return
    if self.graphed:
      self.wpic.destroy()
    try:
      self.wpic = NetWindow(self, self.network)
      self.wpic.show()
      self.graphed = True
    except Exception, e:
      print 'Nao conseguiu gerar o grafico da rede (graphviz estah instalado ?):', e

  # Obtem o tamanho da janela principal. Necessario para saber como redimensiona-la
  # quando encerra um experimento, remove um desenho da rede ou limpa a janela.
  # Talvez tenha um jeito mais simples, explorando o proprio gtk.Table ...
  def get_size(self, window, event):
    if self.network:
      if not self.size:
        self.size = (event.width, event.height)
    elif not self.clear_size:
      self.clear_size = (event.width, event.height)
    return False
    
  def check_update(self):
    u = Update()
    if u.check(False):
      gtk.gdk.threads_enter()
      self.up_handler = False
      msg = gtk.MessageDialog(None, 0,
                gtk.MESSAGE_INFO,
                gtk.BUTTONS_OK,
                None)
      msg.set_markup('There is a new version in the server ...\nConsider updating <b>netkit2 </b>!')
      msg.run()
      msg.destroy()
      gtk.gdk.threads_leave()
    self.check_idle = True

  def update_timer(self):
    if self.up_handler and self.check_idle:
      task = threading.Thread(None, self.check_update)
      task.start()
    return self.up_handler

  def check_reload(self):
      gtk.gdk.threads_enter()
      if not self.started and self.conf:
        try:
            os.stat(self.conf)
            self.reload(None)            
        except OSError:
            gtk.gdk.threads_leave()
            return False
      gtk.gdk.threads_leave()
      return True
        
         
  # Metodo usado para abrir a janela ... na pratica o unico metodo publico
  def show(self):
      self.window = gtk.Window()
      self.window.connect('delete-event', lambda window, event: gtk.main_quit())
      self.up_handler = True
      self.__render__()
      gobject.threads_init()
      gtk.gdk.threads_init()
      gobject.timeout_add_seconds(self.UpdateTimeout, self.update_timer)
      #gobject.timeout_add_seconds(self.ConfCheckInterval, self.check_reload)
      #gtk.main()
      #if self.started: self.halt(None)

MenuEntry='''[Desktop Entry]
Encoding=UTF-8
Name=Netkit2
Comment=Gnome interface to Netkit2
Type=Application
Exec=%s/bin/netkit2
Icon=%s/contrib/ifsc.png
Terminal=false
Categories=GNOME;Application;Education;
StartupNotify=true'''

def set_environ():
  try:
    nkhome = os.environ['NETKIT2_HOME']
  except:
    nkhome = os.path.abspath('%s/../..' % sys.argv[0])
    os.environ['NETKIT2_HOME'] = nkhome
  os.environ['PATH'] = '%s:%s/bin' % (os.environ['PATH'], nkhome)
  os.environ['SUDO_ASKPASS']='%s/bin/netkit_askpass.py' % nkhome
  try:
    os.environ['MANPATH'] = '%s:%s/man' % (os.environ['MANPATH'], nkhome)
  except:
    os.environ['MANPATH'] = '%s/man' % nkhome
  #print nkhome, os.environ['PATH']
  return nkhome

def check_menu_entry():
  try:
    pw = pwd.getpwuid(os.getuid())
    home = pw[5]
  except Exception, e:
    print 'WARNING: could not obtain user home directory .. skipping checking menu entry', e
    return True
  nkhome = set_environ()

  #try:
  #  nkhome = os.environ['NETKIT2_HOME']
  #except:
  #  print 'ERROR: environment variable NETKIT2_HOME is not set ! Check your shell profile ...'
  #  return False
  desktop_dir = '%s/.local/share/applications' % home
  try:
    os.mkdir(desktop_dir)
  except:
    pass
  desktop = '%s/netkit2.desktop' % desktop_dir
  try:
    os.stat(desktop)
  except:
    entry = MenuEntry % (nkhome, nkhome)
    try:
      f = open(desktop,'w')
      f.write(entry)
      f.close()
      print 'INFO: installed netkit2 in GNOME menu Applications/Education'
    except Exception, e:
      print 'WARNING: Failed to install menu entry for netkit2:', e
  return True

def cleanup():
  from netkit_askpass import get_passfile

  try:
    os.remove(get_passfile())
  except:
    pass

###########################################################################
GeditConf = '''<?xml version="1.0"?>
<gconf>
	<entry name="highlight_current_line" mtime="1401229987" type="bool" value="true"/>
</gconf>
'''
GeditPrefsDir = '.gconf/apps/gedit-2/preferences/editor/current_line'
GeditPrefs = '%gconf.xml'
###########################################################################
if __name__ == '__main__':

  if not check_menu_entry():
    sys.exit(0)

  bg = False
  verbose = False
  opcoes,extra = getopt.getopt(sys.argv[1:], 'dv')
  for op,valor in opcoes:
    if op == '-d': bg = True
    elif op == '-v': verbose = True

  if bg:
    if os.fork():
      sys.exit(0)

  if not verbose:
    print 'Mensagens de alerta e debug serao gravadas em', '%s/netkit.log' % os.getcwd()
    outp = open('netkit.log','w')
    os.dup2(outp.fileno(), 1)
    os.dup2(outp.fileno(), 2)
    outp.close()

  gdir = '%s/%s' % (os.environ['HOME'], GeditPrefsDir)
  try:
    os.makedirs(gdir)
  except:
    pass
  try:
    f = open('%s/%s' % (gdir, GeditPrefs), 'w')
    f.write(GeditConf)
    f.close()
  except:
    pass

  os.environ['UBUNTU_MENUPROXY']='0'
  term = NetkitTerminal()

  term.cleanup()
  term.show()
  gtk.main()
  term.halt(None)
  cleanup()

